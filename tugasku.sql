/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50529
Source Host           : localhost:3306
Source Database       : tugasku

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2013-03-31 11:54:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `jabatan`
-- ----------------------------
DROP TABLE IF EXISTS `jabatan`;
CREATE TABLE `jabatan` (
  `jabatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(10) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jabatan
-- ----------------------------
INSERT INTO `jabatan` VALUES ('1', 'Admin', '1');
INSERT INTO `jabatan` VALUES ('2', 'Guru', '2');
INSERT INTO `jabatan` VALUES ('3', 'Siswa', '3');

-- ----------------------------
-- Table structure for `jenis_tugas`
-- ----------------------------
DROP TABLE IF EXISTS `jenis_tugas`;
CREATE TABLE `jenis_tugas` (
  `jenis_tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  PRIMARY KEY (`jenis_tugas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of jenis_tugas
-- ----------------------------
INSERT INTO `jenis_tugas` VALUES ('1', 'Tugas Produktif', '');
INSERT INTO `jenis_tugas` VALUES ('4', 'Tugas Akhir', '');

-- ----------------------------
-- Table structure for `kompetensi_keahlian`
-- ----------------------------
DROP TABLE IF EXISTS `kompetensi_keahlian`;
CREATE TABLE `kompetensi_keahlian` (
  `kompetensi_keahlian_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(10) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  PRIMARY KEY (`kompetensi_keahlian_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kompetensi_keahlian
-- ----------------------------
INSERT INTO `kompetensi_keahlian` VALUES ('1', 'RPL', 'Rekayasa Perangkat Lunak');
INSERT INTO `kompetensi_keahlian` VALUES ('2', 'TKJ', 'Teknik Komputer Jaringan');
INSERT INTO `kompetensi_keahlian` VALUES ('4', 'MM', 'Multimedia');

-- ----------------------------
-- Table structure for `mata_pelajaran`
-- ----------------------------
DROP TABLE IF EXISTS `mata_pelajaran`;
CREATE TABLE `mata_pelajaran` (
  `mata_pelajaran_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kompetensi_keahlian_id` int(11) NOT NULL,
  PRIMARY KEY (`mata_pelajaran_id`),
  KEY `mata_pelajaran_FI_1` (`kompetensi_keahlian_id`),
  CONSTRAINT `mata_pelajaran_FK_1` FOREIGN KEY (`kompetensi_keahlian_id`) REFERENCES `kompetensi_keahlian` (`kompetensi_keahlian_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mata_pelajaran
-- ----------------------------
INSERT INTO `mata_pelajaran` VALUES ('1', 'Algoritma & Pemrograman', '1');
INSERT INTO `mata_pelajaran` VALUES ('2', 'Pemrograman Visual Basic', '1');
INSERT INTO `mata_pelajaran` VALUES ('3', 'Basis Data', '1');
INSERT INTO `mata_pelajaran` VALUES ('4', 'Perancangan Pembuatan Aplikasi', '1');
INSERT INTO `mata_pelajaran` VALUES ('5', 'Pemrograman Java', '1');
INSERT INTO `mata_pelajaran` VALUES ('6', 'Pemrograman Dot Net', '1');
INSERT INTO `mata_pelajaran` VALUES ('7', 'Praktek Kerja Industri', '1');

-- ----------------------------
-- Table structure for `pengguna`
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna` (
  `pengguna_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(15) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  PRIMARY KEY (`pengguna_id`),
  KEY `pengguna_FI_1` (`jabatan_id`),
  CONSTRAINT `pengguna_FK_1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`jabatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pengguna
-- ----------------------------
INSERT INTO `pengguna` VALUES ('1', 'amster', 'amster', 'Muhammad Faisal Akbar', '085720003549', 'amster.jpg', '1');
INSERT INTO `pengguna` VALUES ('17', 'faisal', 'faisal', 'faisal', '123454', 'faisal.png', '2');
INSERT INTO `pengguna` VALUES ('57', '0809112758', '12758', 'Adam Fadhillah', '085722541119', '', '3');
INSERT INTO `pengguna` VALUES ('58', '0809112759', '12759', 'Adi Muhammad Yusuf', '', '', '3');
INSERT INTO `pengguna` VALUES ('59', '0809112760', '12760', 'Afrizal Septian Fauzi', '081809331573', '', '3');
INSERT INTO `pengguna` VALUES ('60', '0809112761', '12761', 'Alam Ramadhan', '85860186915', '', '3');
INSERT INTO `pengguna` VALUES ('61', '0809112762', '12762', 'Ammalida Adam Muhammad', '85721144425', '', '3');
INSERT INTO `pengguna` VALUES ('62', '0809112763', '12763', 'Anissa Istiqomah', '85659981181', '', '3');
INSERT INTO `pengguna` VALUES ('63', '0809112764', '12764', 'Arif Ramadan', '89655187510', '', '3');
INSERT INTO `pengguna` VALUES ('64', '0809112765', '12765', 'Bettari Laras Fitrany', '85720046012', '', '3');
INSERT INTO `pengguna` VALUES ('65', '0809112766', '12766', 'Dewi Lestari', '85721788710', '', '3');
INSERT INTO `pengguna` VALUES ('66', '0809112767', '12767', 'Dhifan Ramdhani', '85624194994', '0809112767.png', '3');
INSERT INTO `pengguna` VALUES ('67', '0809112768', '12768', 'Dikki Juhadi', '85721896300', '0809112768.png', '3');
INSERT INTO `pengguna` VALUES ('68', '0809112769', '12769', 'Faizal Fitrikorama', '83820863250', '', '3');
INSERT INTO `pengguna` VALUES ('69', '0809112770', '12770', 'Fajar Dwi Putra', '1234', '0809112770.png', '3');
INSERT INTO `pengguna` VALUES ('70', '0809112772', '12772', 'Ferdy Octovian', '85721979794', '', '3');
INSERT INTO `pengguna` VALUES ('71', '0809112773', '12773', 'Gusti Ramanda Terupali', '022 76647734', '', '3');
INSERT INTO `pengguna` VALUES ('72', '0809112774', '12774', 'Hannie Priliati Sekarni', '8812232507', '', '3');
INSERT INTO `pengguna` VALUES ('73', '0809112775', '12775', 'Ian Supriatna', '022 92673608', '', '3');
INSERT INTO `pengguna` VALUES ('74', '0809112776', '12776', 'Ilham Sara Dewa', '022 7809684', '', '3');
INSERT INTO `pengguna` VALUES ('75', '0809112777', '12777', 'Ivan Hendriono', '85624892915', '', '3');
INSERT INTO `pengguna` VALUES ('76', '0809112778', '12778', 'Januarsyah Dwimudya S', '0', '0809112778.png', '3');
INSERT INTO `pengguna` VALUES ('77', '0809112779', '12779', 'Krisna Wijaya', '8996054610', '', '3');
INSERT INTO `pengguna` VALUES ('78', '0809112780', '12780', 'M. Reksa Surya Pandita', '8562112222', '', '3');
INSERT INTO `pengguna` VALUES ('79', '0809112781', '12781', 'Mochamad Padli Putrapratama', '87821919828', '', '3');
INSERT INTO `pengguna` VALUES ('80', '0809112782', '12782', 'Mochamad Vega Rachman', '85720141095', '', '3');
INSERT INTO `pengguna` VALUES ('81', '0809112783', '12783', 'Mohammad Briandi', '85722394229', '', '3');
INSERT INTO `pengguna` VALUES ('82', '0809112784', '12784', 'Muhamad Zaelani', '022 5398859', '', '3');
INSERT INTO `pengguna` VALUES ('83', '0809112785', '12785', 'Muhammad Faisal Akbar', '85720003549', '', '3');
INSERT INTO `pengguna` VALUES ('84', '08091123445', '12344', 'Faisal Uje', '085720003549', '08091123445.png', '3');

-- ----------------------------
-- Table structure for `siswa`
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `nis` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `foto` varchar(255) NOT NULL,
  PRIMARY KEY (`siswa_id`),
  KEY `I_referenced_tugas_FK_3_1` (`nis`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa
-- ----------------------------
INSERT INTO `siswa` VALUES ('1', '0809112758', 'Adam Fadhillah', 'Laki-laki', '', '', '085722541119', 'XIII D', '');
INSERT INTO `siswa` VALUES ('2', '0809112759', 'Adi Muhammad Yusuf', 'Laki-laki', '', '', '', 'XIII D', '');
INSERT INTO `siswa` VALUES ('3', '0809112760', 'Afrizal Septian Fauzi', 'Laki-laki', '', '', '081809331573', 'XIII D', '');
INSERT INTO `siswa` VALUES ('4', '0809112761', 'Alam Ramadhan', 'Laki-laki', '', '', '85860186915', 'XIII D', '');
INSERT INTO `siswa` VALUES ('5', '0809112762', 'Ammalida Adam Muhammad', 'Laki-laki', '', '', '85721144425', 'XIII D', '');
INSERT INTO `siswa` VALUES ('6', '0809112763', 'Anissa Istiqomah', 'Perempuan', '', '', '85659981181', 'XIII D', '');
INSERT INTO `siswa` VALUES ('7', '0809112764', 'Arif Ramadan', 'Laki-laki', '', '', '89655187510', 'XIII D', '');
INSERT INTO `siswa` VALUES ('8', '0809112765', 'Bettari Laras Fitrany', 'Perempuan', '', '', '85720046012', 'XIII D', '');
INSERT INTO `siswa` VALUES ('9', '0809112766', 'Dewi Lestari', 'Perempuan', '', '', '85721788710', 'XIII D', '');
INSERT INTO `siswa` VALUES ('10', '0809112767', 'Dhifan Ramdhani', 'Laki-laki', '', '', '85624194994', 'XIII D', '0809112767.png');
INSERT INTO `siswa` VALUES ('11', '0809112768', 'Dikki Juhadi', 'Laki-laki', '', '', '85721896300', 'XIII D', '0809112768.png');
INSERT INTO `siswa` VALUES ('12', '0809112769', 'Faizal Fitrikorama', 'Laki-laki', '', '', '83820863250', 'XIII D', '');
INSERT INTO `siswa` VALUES ('13', '0809112770', 'Fajar Dwi Putra', 'Laki-laki', '', '', '', 'XIII D', '0809112770.png');
INSERT INTO `siswa` VALUES ('14', '0809112772', 'Ferdy Octovian', 'Laki-laki', '', '', '85721979794', 'XIII D', '');
INSERT INTO `siswa` VALUES ('15', '0809112773', 'Gusti Ramanda Terupali', 'Laki-laki', '', '', '022 76647734', 'XIII D', '');
INSERT INTO `siswa` VALUES ('16', '0809112774', 'Hannie Priliati Sekarni', 'Perempuan', '', '', '8812232507', 'XIII D', '');
INSERT INTO `siswa` VALUES ('17', '0809112775', 'Ian Supriatna', 'Laki-laki', '', '', '022 92673608', 'XIII D', '');
INSERT INTO `siswa` VALUES ('18', '0809112776', 'Ilham Sara Dewa', 'Laki-laki', '', '', '022 7809684', 'XIII D', '');
INSERT INTO `siswa` VALUES ('19', '0809112777', 'Ivan Hendriono', 'Laki-laki', '', '', '85624892915', 'XIII D', '');
INSERT INTO `siswa` VALUES ('20', '0809112778', 'Januarsyah Dwimudya S', 'Laki-laki', '', '', '', 'XIII D', '0809112778.png');
INSERT INTO `siswa` VALUES ('21', '0809112779', 'Krisna Wijaya', 'Laki-laki', '', '', '8996054610', 'XIII D', '');
INSERT INTO `siswa` VALUES ('22', '0809112780', 'M. Reksa Surya Pandita', 'Laki-laki', '', '', '8562112222', 'XIII D', '');
INSERT INTO `siswa` VALUES ('23', '0809112781', 'Mochamad Padli Putrapratama', 'Laki-laki', '', '', '87821919828', 'XIII D', '');
INSERT INTO `siswa` VALUES ('24', '0809112782', 'Mochamad Vega Rachman', 'Laki-laki', '', '', '85720141095', 'XIII D', '');
INSERT INTO `siswa` VALUES ('25', '0809112783', 'Mohammad Briandi', 'Laki-laki', '', '', '85722394229', 'XIII D', '');
INSERT INTO `siswa` VALUES ('26', '0809112784', 'Muhamad Zaelani', 'Laki-laki', '', '', '022 5398859', 'XIII D', '');
INSERT INTO `siswa` VALUES ('27', '0809112785', 'Muhammad Faisal Akbar', 'Laki-laki', '', '', '85720003549', 'XIII D', '');
INSERT INTO `siswa` VALUES ('28', '0809112786', 'Muhammad Keival Apriandika', 'Laki-laki', '', '', '83820845745', 'XIII D', '');
INSERT INTO `siswa` VALUES ('29', '0809112787', 'Muhammad Rinaldy Basuki', 'Laki-laki', '', '', '8986487134', 'XIII D', '');
INSERT INTO `siswa` VALUES ('30', '0809112788', 'Naufal Eka', 'Laki-laki', '', '', '87722241492', 'XIII D', '');
INSERT INTO `siswa` VALUES ('31', '0809112789', 'Prasetyo Nandha Diputra', 'Laki-laki', 'Bandung', '24/02/2012', '85722165905', 'XIII D', '0809112789.png');
INSERT INTO `siswa` VALUES ('32', '0809112790', 'Razan Fachdari Ahmad', 'Laki-laki', '', '', '85722623956', 'XIII D', '');
INSERT INTO `siswa` VALUES ('33', '0809112791', 'Rheza Safarudin Maulana', 'Laki-laki', '', '', '85721640088', 'XIII D', '');
INSERT INTO `siswa` VALUES ('34', '0809112792', 'Ryan A. Fadillah', 'Laki-laki', '', '', '89656142499', 'XIII D', '');
INSERT INTO `siswa` VALUES ('35', '0809112793', 'Shilva Maylinda Irawan', 'Perempuan', '', '', '85624331105', 'XIII D', '');
INSERT INTO `siswa` VALUES ('36', '0809112794', 'Sofi Sofiah', 'Perempuan', '', '', '818637463', 'XIII D', '');
INSERT INTO `siswa` VALUES ('37', '0809112796', 'Wahyu Indra Ramdani', 'Laki-laki', '', '', '85724715815', 'XIII D', '');
INSERT INTO `siswa` VALUES ('38', '0809112797', 'Yuda Permana', 'Laki-laki', '', '', '085722235833', 'XIII D', '');
INSERT INTO `siswa` VALUES ('40', '0809112715', 'Bahrun', 'Laki-laki', 'Bandung', '22/02/2012', '1234567890', 'XI D', '.png');
INSERT INTO `siswa` VALUES ('41', '08091123445', 'Faisal Uje', 'Laki-laki', 'Bandung', '31/03/2013', '085720003549', 'XI A', '08091123445.png');

-- ----------------------------
-- Table structure for `tugas`
-- ----------------------------
DROP TABLE IF EXISTS `tugas`;
CREATE TABLE `tugas` (
  `tugas_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_tugas_id` int(11) NOT NULL,
  `mata_pelajaran_id` int(11) NOT NULL,
  `nis` varchar(20) NOT NULL,
  `tanggal_pengumpulan` varchar(50) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `abstrak` varchar(255) DEFAULT NULL,
  `pengguna_id` int(11) NOT NULL,
  PRIMARY KEY (`tugas_id`),
  KEY `tugas_FI_1` (`jenis_tugas_id`),
  KEY `tugas_FI_2` (`mata_pelajaran_id`),
  KEY `tugas_FI_3` (`nis`),
  KEY `tugas_FI_4` (`pengguna_id`),
  CONSTRAINT `tugas_FK_1` FOREIGN KEY (`jenis_tugas_id`) REFERENCES `jenis_tugas` (`jenis_tugas_id`),
  CONSTRAINT `tugas_FK_2` FOREIGN KEY (`mata_pelajaran_id`) REFERENCES `mata_pelajaran` (`mata_pelajaran_id`),
  CONSTRAINT `tugas_FK_3` FOREIGN KEY (`nis`) REFERENCES `siswa` (`nis`),
  CONSTRAINT `tugas_FK_4` FOREIGN KEY (`pengguna_id`) REFERENCES `pengguna` (`pengguna_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tugas
-- ----------------------------
INSERT INTO `tugas` VALUES ('32', '1', '2', '0809112762', '03/30/2013', 'Looping', '0809112762_Looping.zip', null, '1');
INSERT INTO `tugas` VALUES ('33', '1', '3', '08091123445', '03/31/2013', 'Membuat Database', '08091123445_Membuat Database.zip', null, '1');
