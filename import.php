<?php

require_once('startup.php');

//error_reporting(E_ALL);
$thang = '2012';
$kddept = '023';
$kdunit = '03';
//$kddekon = '1';   //1=pusat, 3=dekon
$kddekon = '1';   //1=pusat, 3=dekon, 4=gabung
//$kdsatker = '414726';
//$kdsatker = '040007'; diy
//$kdsatker = '310030'; g'talo
//$kdsatker = '682118'; 

//Timer//
$mtime = microtime(); 
$mtime = explode(' ', $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 
$where = "where kdsatker = '$kdsatker' and kddept='$kddept' and kdunit= '$kdunit'";

function countTableRows($tablename) {
	return getValueBySql("select count(*) from $tablename");
}

function truncateTable($tablename) {
	try {
		executeSql("set foreign_key_checks = 0; truncate table $tablename");
		echo "Table emptied.";
	} catch (Exception $e) {
		echo "Error : ".$e->getMessage();
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Importing Data RKAKL</title>
</head>
<body>
<?php

//error_reporting(E_ALL);
//error_reporting(E_ALL);
//session_start();

try {
	
	$conn = odbc_connect('RKAKL12','','');
	
	if ($_REQUEST["action"] == "empty") {
		truncateTable($_REQUEST["data"]);
		die;
	}
	
	switch ($_REQUEST["data"]) {
		case "emptyrealisasi" :
			$sql = "
				set foreign_key_checks = 0;
				update detil set jumlah_spp = 0;
				truncate table detil_spp;				
				truncate table detil_tup;
				truncate table permohonan_tup;
				truncate table detil_revisi;				
				truncate table sub_output_revisi;
				truncate table komponen_revisi;
				truncate table sub_komponen_revisi;				
				truncate table permohonan_revisi;				
				truncate table data_transaksi;
				truncate table acara_kegiatan;
				truncate table rincian_spp;
				truncate table spp;			";			
			break;

		case "emptyrkakl" :
			$sql = "
				set foreign_key_checks = 0;
				truncate table pengguna;
				truncate table kegiatan;
				truncate table output;
				truncate table sub_output;
				truncate table komponen;
				truncate table sub_komponen;
				truncate table detil;
			
			";
			break;
			
		case "lokasi" :
			$sql = "select * from t_lokasi";
			$rs = odbc_exec($conn,$sql);		
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);
				$lokasi = new Lokasi();
				$lokasi->setKode($row["kdlokasi"]);
				$lokasi->setNama(trim(str_replace(".","",$row["nmlokasi"])));
				$lokasi->save($con);
				echo $i++.") ".$row["nmlokasi"]."<br>";
			}
			$con->commit();			
			break;
			
		case "kabkota" :
			$sql = "select * from t_kabkota";
			$rs = odbc_exec($conn,$sql);		
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);				
				$kabkota = new Kabkota();
				$kabkota->setKode($row["kdkabkota"]);
				$kabkota->setNama(trim(str_replace(".","",$row["nmkabkota"])));
				
				$c = new Criteria();
				$c->add(LokasiPeer::KODE, $row["kdlokasi"]);
				$lokasi = LokasiPeer::doSelectOne($c);
				
				$kabkota->setLokasi($lokasi);
				$kabkota->save($con);
				echo $i++.") ".$row["nmkabkota"]."<br>";				
			}
			$con->commit();
			
			break;
		
		case "kppn" :
			$sql = "select * from t_kppn";
			$rs = odbc_exec($conn,$sql);
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			while ($row = odbc_fetch_array($rs)) {				
				$kppn = new Kppn();
				$kppn->setKode($row["kdkppn"]);
				$kppn->setNama(trim(str_replace(".","",$row["nmkppn"])));
				$kppn->setAlamat(trim(str_replace(".","",$row["almkppn"])));
				$kppn->setTipeKppn($row["tipekppn"]);
				$kppn->save($con);
				echo $i++.") ".$row["nmkppn"]."<br>";				
			}
			$con->commit();
			break;
			
		case "unit" :
			$sql = "select * from t_unit where kddept = '$kddept'";
			//echo $sql; die;
			$rs = odbc_exec($conn,$sql);
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			while ($row = odbc_fetch_array($rs)) {				
				$unit = new Unit();
				$unit->setKode($row["kdunit"]);
				$unit->setNama(trim(str_replace(".","",$row["nmunit"])));
				$unit->save($con);
				echo $i++.") ".$row["nmunit"]."<br>";				
			}
			$con->commit();
			break;
			
		case "satker" :
			$sql = "select * from t_satker where kddept = '$kddept' AND kdkppn != '   '";
			//echo $sql; die;
			$rs = odbc_exec($conn,$sql);
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);
				$satker = new Satker();
				$satker->setKode($row["kdsatker"]);
				$satker->setNama(trim(str_replace(".","",$row["nmsatker"])));
				
				$c = new Criteria();
				$c->add(UnitPeer::KODE, $row["kdunit"]);
				$unit = UnitPeer::doSelectOne($c);
				if (is_object($unit))				
					$satker->setUnit($unit);
				else
					$satker->setUnit(NULL);
					
				$c = new Criteria();
				$c->add(KabkotaPeer::KODE, $row["kdkabkota"]);
				$kabkota = KabkotaPeer::doSelectOne($c);
				if (is_object($kabkota))				
					$satker->setKabkota($kabkota);
				else
					$satker->setKabkota(NULL);

				$c = new Criteria();
				$c->add(KppnPeer::KODE, $row["kdkppn"]);
				$kppn = KppnPeer::doSelectOne($c);
				if (is_object($kppn))				
					$satker->setKppn($kppn);
				else
					$satker->setKppn(NULL);					
				$satker->save($con);
				echo $i++.") ".$row["nmsatker"]."<br>";				
			}
			$con->commit();
			break;

		case "mak":
			$sql = "select * from t_akun";
			$rs = odbc_exec($conn,$sql);
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			while ($row = odbc_fetch_array($rs)) {				
				$mak = new Mak();
				$mak->setKode($row["kdakun"]);
				$mak->setNama(trim(str_replace(".","",$row["nmakun"])));
				$mak->save($con);
				echo $i++.") ".$row["nmakun"]."<br>";				
			}
			$con->commit();
			break;
			
		case "program" :
			//$sql = "select distinct a.*, b.kdfungsi, b.kdsfung from t_program a left join t_giat b on a.kddept = b.kddept and a.kdunit = b.kdunit and a.kdprogram = b.kdprogram where a.kddept = '$kddept' and a.thang = '$thang'";
			//$sql = "select distinct a.*, b.kdfungsi, b.kdsfung from t_program a left join t_giat b on a.kddept = b.kddept and a.kdunit = b.kdunit and a.kdprogram = b.kdprogram where a.kddept = '$kddept'";
			//$sql = "select distinct a.* from t_program a where a.kddept = '$kddept'";
			$sql = "select * from t_program where kddept = '$kddept'";
			//echo $sql."<br>";
			
			$rs = odbc_exec($conn,$sql);
			//print_r($rs);
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				print_r($row);
				$prog = new Program();
				$prog->setKode($row["kddept"].".".$row["kdunit"].".".$row["kdprogram"]);
				$prog->setKodeFungsi($row["kdfungsi"].".".$row["kdsfung"]);
				$prog->setNama(trim(str_replace(".","",$row["nmprogram"])));				
				$prog->save($con);
				echo $i++.") ".$row["nmprogram"]."<br>";
			}
			$con->commit();
			break;
		
		case "kegiatan" :
			$sql = "select * from t_giat where kddept = '$kddept' and kdunit= '$kdunit'";
			$rs = odbc_exec($conn,$sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);
				//$prog = new Program();
				//$prog->setKode($row["kddept"].".".$row["kdunit"].".".$row["kdprogram"]);
				//$prog->save();
				$keg = new Kegiatan();
				$keg->setKode($row["kdgiat"]);
				$keg->setNama(trim(str_replace(".","",$row["nmgiat"])));
				
				$c = new Criteria();
				$c->add(ProgramPeer::KODE, $row["kddept"].".".$row["kdunit"].".".$row["kdprogram"]);
				$prog = ProgramPeer::doSelectOne($c);
				if (!is_object($prog)) {
					throw new Exception("The parent program (".$row["kddept"].".".$row["kdunit"].".".$row["kdprogram"].") not found");					
				}
				$keg->setProgram($prog);
				echo $i++.") ".$row["nmgiat"]."<br>";
				$keg->save($con);
			}
			$con->commit();
			break;	
			
		case "output" :
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : ""; 
			$sql = "select a.*, b.nmoutput, b.sat from d_output a
					left join t_output b on a.kdgiat = b.kdgiat and a.kdoutput = b.kdoutput
					where a.kddept = '$kddept' and kdunit= '$kdunit' and a.thang = '$thang' and a.kddekon = '$kddekon'
					$andkdsatker
					order by a.kdsatker, a.kdunit, a.kdprogram, a.kdgiat, a.kdoutput";
			//echo $sql; die;
			
			if ($kddekon == '4') {
				$sql = "select a.*, b.nmoutput, b.sat from d_output a 
						left join t_output b on a.kdgiat = b.kdgiat and a.kdoutput = b.kdoutput					
						where a.kddept = '$kddept' and kdunit= '$kdunit' and a.thang = '$thang' 
						order by a.kdsatker, a.kdunit, a.kdprogram, a.kdgiat, a.kdoutput";
			}
			
			$rs = odbc_exec($conn,$sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row); continue;
				
				$output = new Output();
				$output->setKode($row["kdoutput"]);
				$output->setKodeKp($row["kddekon"]);
				$output->setNama(trim(str_replace(".","",$row["nmoutput"])));
				$output->setVol($row["vol"]);
				$output->setVolmin1($row["volmin1"]);
				$output->setVolpls1($row["volpls1"]);
				$output->setVolpls2($row["volpls2"]);
				$output->setRphmin1($row["rphmin1"]);
				$output->setRphpls1($row["rphpls1"]);
				$output->setRphpls2($row["rphpls2"]);
				$output->setSatuan(trim(str_replace(".","", $row["sat"])));
				
				$c = new Criteria();
				$c->add(KabkotaPeer::KODE, $row["kdkabkota"]);
				$kabkota = KabkotaPeer::doSelectOne($c);				
				$output->setKabkota($kabkota);
				
				$c = new Criteria();
				$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
				$kegiatan = KegiatanPeer::doSelectOne($c);
				$output->setKegiatan($kegiatan);
				$output->setKodeOutput($kegiatan->getKode().".".$output->getKode());
				
				$c = new Criteria();
				$c->add(TahunPeer::NAMA, $row["thang"]);
				$tahun = TahunPeer::doSelectOne($c);
				$output->setTahun($tahun);

				$c = new Criteria();
				$c->add(SatkerPeer::KODE, $row["kdsatker"]);
				$satker = SatkerPeer::doSelectOne($c);
				$output->setSatker($satker);
				echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"]." - ".$row["nmoutput"]." | ". $row["kdsatker"] ."<br>";
				
				$output->save($con);
			}
			$con->commit();
			break;
			
		case "sub_output":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			$sql = "select * from d_soutput where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
					$andkdsatker
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput";
			//echo $sql; die;
			if ($kddekon == '4') {
				$sql = "select * from d_soutput where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput";
			}
			
			$rs = odbc_exec($conn,$sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);				
				$sub_output = new SubOutput();
				$sub_output->setKode($row["kdsoutput"]);				
				$sub_output->setNama(trim(str_replace(".","",$row["ursoutput"])));
				$sub_output->setVol($row["volsout"]);
				$sub_output->setSatuan($row["sbmksat"]);
				
				$c = new Criteria();
				$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
				$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
				$c->add(OutputPeer::KODE, $row["kdoutput"]);
				$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
				$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
				$output = OutputPeer::doSelectOne($c);								
				if (!is_object($output)) {
					throw new Exception("The parent (output) not found");					
				} else {
					$sub_output->setOutput($output);
					$sub_output->setKodeSubOutput($output->getKodeOutput().".".$sub_output->getKode());
					if (trim(strtolower($row["ursoutput"])) == "tanpa sub output") {
						$sub_output->setNama($output->getNama());
						$tanpa_sub_output_string = "( digantikan ".$output->getNama()." )";
					}					
				}
				echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"]." - ".$row["ursoutput"]." $tanpa_sub_output_string<br>";				
				$sub_output->save($con);
				$tanpa_sub_output_string = "";
			}
			$con->commit();
			break;			

		case "komponen":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			
			$sql = "select * from d_kmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen";
			//echo $sql; die;
			if ($kddekon == '4') {
				$sql = "select * from d_kmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen";			
			}
			
			$rs = odbc_exec($conn,$sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				try {
					//print_r($row);				
					$komponen = new Komponen();
					$komponen->setKode($row["kdkmpnen"]);
					$komponen->setNama(trim(str_replace(".","",$row["urkmpnen"])));
					
					$c = new Criteria();
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
					$sub_output = SubOutputPeer::doSelectOne($c);
									
					if (!is_object($sub_output)) {
						throw new Exception("<font color='red'>The parent (suboutput) not found. Komponen : <b>".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"]."</b></font><br>");
					} else {
						$komponen->setSubOutput($sub_output);
						$komponen->setKodeKomponen($sub_output->getKodeSubOutput().".".$komponen->getKode());	
					}
					echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"]." - ".$row["urkmpnen"]."<br>";
					$komponen->save($con);
				
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			$con->commit();
			break;

		case "sub_komponen":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			
			$sql = "select * from d_skmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen";
			//echo $sql; die;			
			if ($kddekon == '4') {
				$sql = "select * from d_skmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen";
			}
			$rs = odbc_exec($conn,$sql);

			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);			
				try {	
					$sub_komponen = new SubKomponen();
					$sub_komponen->setKode($row["kdskmpnen"]);
					$sub_komponen->setNama(trim(str_replace(".","",$row["urskmpnen"])));
					
					$c = new Criteria();
					$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
					$komponen = KomponenPeer::doSelectOne($c);
									
					if (!is_object($komponen)) {
						//throw new Exception("The parent (komponen) not found at row #$i<br>");					
						throw new Exception("<font color='red'>The parent (komponen) not found. Komponen : <b>".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"]."</b></font><br>");
					} else {
						$sub_komponen->setKomponen($komponen);	
					}
					//echo $i++.") ".$row["urskmpnen"]."<br>";								
					echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"]." - ".$row["urskmpnen"]."<br>";
					
					$komponen->save($con);
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			$con->commit();
			break;

		case "item" :
			
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			$sql = "select distinct * from d_item where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker	
				order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem";
				//order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem  where kddekon = '1'";
				//echo $sql;die;
			
			if ($kddekon == '4') {
				$sql = "select distinct * from d_item where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem";
			}
			
			$rs = odbc_exec($conn,$sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			while ($row = odbc_fetch_array($rs)) {
				//print_r($row);
				try {			
					$d = new Detil();
					//$d->setKodeDetil($row["noitem"]);
					$d->setNoHeader($row["kdheader"]);
					$d->setNoHeader1($row["header1"]);
					$d->setNoHeader2($row["header2"]);
					$d->setNoItem($row["noitem"]);
					$d->setNama(trim(str_replace(".","",$row["nmitem"])));
					$d->setKodeKppn($row["kdkppn"]);
					$d->setKodeBeban($row["kdbeban"]);
					$d->setKodeJenisbantuan($row["kdjnsban"]);
					$d->setKodeCaratarik($row["kdctarik"]);
					$d->setRegister($row["register"]);
					$d->setVolumeRka($row["volkeg"]);
					$d->setVol1($row["vol1"]);
					$d->setVol2($row["vol2"]);
					$d->setVol3($row["vol3"]);					
					$d->setVol4($row["vol4"]);
					$d->setSat1($row["sat1"]);
					$d->setSat2($row["sat2"]);
					$d->setSat3($row["sat3"]);
					$d->setSat4($row["sat4"]);
					$d->setVolumeSpp(0);
					$d->setSatuan($row["satkeg"]);
					$d->setHargaSatuan($row["hargasat"]);
					$d->setJumlahRka($row["jumlah"]);
					$d->setJumlahSpp(0);
					
					$c = new Criteria();
					$c->addJoin(SubKomponenPeer::KOMPONEN_ID, KomponenPeer::KOMPONEN_ID);
					$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(SubKomponenPeer::KODE, $row["kdskmpnen"]);
					$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
	
					$kodeLengkap = $row["kdskmpnen"].".".$row["kdkmpnen"] .".". $row["kdsoutput"] .".". $row["kdoutput"] .".". $row["kdgiat"];
					$kodeLengkap = $row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"];
					
					$sub_komponen = SubKomponenPeer::doSelectOne($c);
					if (!is_object($sub_komponen)) {
						//throw new Exception("The parent (sub komponen) not found at row #$i ( $kodeLengkap )");
						echo "&nbsp;&nbsp;&nbsp;<font color='red'>Notice: The parent (sub komponen) not found at row #$i ( $kodeLengkap ). Trying to catch the komponen..</font><br>";
						
						$c = new Criteria();
						$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
						$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
						$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
						$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
						$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
						$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
						$c->add(OutputPeer::KODE, $row["kdoutput"]);
						$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
						$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
						$komponen = KomponenPeer::doSelectOne($c);						
						$d->setKomponen($komponen);
						
					} else {
						$d->setSubKomponen($sub_komponen);
						$d->setKomponen($sub_komponen->getKomponen());											
					}
					
					$c = new Criteria();
					$c->add(MakPeer::KODE, $row["kdakun"]);
					$mak = MakPeer::doSelectOne($c);
					if (!is_object($mak)) {
						throw new Exception("Kode akun not found<br>");					
					} else {
						$d->setMak($mak);
					}
					echo $i++.") ".$row["nmitem"]."<br>";								
					
					$d->save($con);
				} catch (Exception $e) {
					$e->getMessage();	
				}
			}
			$con->commit();			
			break;	
		
		default :
			?>
			Importing:<br>
			Tahun : <?=$thang?><br>
			Dept: <?=$kddept?><br>
			Unit: <?=$kdunit?><br>
			Satker: <?=$kdsatker?><br> 
			Dekon: <?=$kddekon == '3' ? "Ya": "Bukan"?><br><br>
			
			Pilih mau import apa : <br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=emptyrealisasi>Kosongkan data Realisasi</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=emptyrkakl>Kosongkan data RKAKL</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=lokasi>lokasi</a> (<?=countTableRows("lokasi")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=kabkota>kabkota</a> (<?=countTableRows("kabkota")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=kppn>kppn</a> (<?=countTableRows("kppn")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=unit>unit</a> (<?=countTableRows("unit")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=satker>satker</a> (<?=countTableRows("satker")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=mak>mak</a> (<?=countTableRows("mak")?>)<br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=program>program</a> (<?=countTableRows("program")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=program&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=kegiatan>kegiatan</a> (<?=countTableRows("kegiatan")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=kegiatan&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=output>output</a> (<?=countTableRows("output")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=output&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output>sub_output</a> (<?=countTableRows("sub_output")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen>komponen</a> (<?=countTableRows("komponen")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen&action=empty>empty</a><br/>									
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen>sub_komponen</a> (<?=countTableRows("sub_komponen")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=item>item</a> (<?=countTableRows("detil")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=detil&action=empty>empty</a><br/><br/>
			Referensi: <br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=pengguna>pengguna</a> (<?=countTableRows("pengguna")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=pengguna&action=empty>empty</a><br/>			
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=penerima>penerima</a> (<?=countTableRows("penerima")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=penerima&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=kontrak>kontrak</a> (<?=countTableRows("kontrak")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=kontrak&action=empty>empty</a><br/><br/>
			Realisasi : <br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=spp>spp</a> (<?=countTableRows("spp")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=spp&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=permohonan_tup>permohonan_tup</a> (<?=countTableRows("permohonan_tup")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=permohonan_tup&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=permohonan_revisi>permohonan_revisi</a> (<?=countTableRows("permohonan_revisi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=permohonan_revisi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_spp>detil_spp</a> (<?=countTableRows("detil_spp")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_spp&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_tup>detil_tup</a> (<?=countTableRows("detil_tup")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_tup&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_revisi>detil_revisi</a> (<?=countTableRows("detil_revisi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=detil_revisi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output_revisi>sub_output_revisi</a> (<?=countTableRows("sub_output_revisi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output_revisi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen_revisi>komponen_revisi</a> (<?=countTableRows("komponen_revisi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen_revisi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen_revisi>sub_komponen_revisi</a> (<?=countTableRows("sub_komponen_revisi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen_revisi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=data_transaksi>data_transaksi</a> (<?=countTableRows("data_transaksi")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=data_transaksi&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=acara_kegiatan>acara_kegiatan</a> (<?=countTableRows("acara_kegiatan")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=acara_kegiatan&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=rincian_spp>rincian_spp</a> (<?=countTableRows("rincian_spp")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=rincian_spp&action=empty>empty</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=feed_back>feed_back</a> (<?=countTableRows("feed_back")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=feed_back&action=empty>empty</a><br/>
			
				
			<!-- >a href=<?=$_SERVER["PHP_SELF"]?>?data=giat>giat</a><br/>			
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=sgiat>sgiat</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=groupmak>groupmak</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=item>item</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=dipa>pembukuan-dipa</a><br/>
			<a href=<?=$_SERVER["PHP_SELF"]?>?data=rgiat>rgiat</a><br/ -->
			
			<?php		 
			break;	
	}

	odbc_close($conn);

} catch (Exception $e) {
	echo $e->getMessage();
}

$mtime = microtime(); 
$mtime = explode(" ", $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 
println('Diproses dalam ' .$totaltime. ' detik.');

      
?>
</body>
</html>
