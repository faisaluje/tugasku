tugasku.PenggunaModule = Ext.extend(Xond.Module, {
	name: 'pengguna',
	title: 'Pengguna',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
		tugasku.RecordSetPengguna = [
			{name: 'PenggunaId'},
			{name: 'Username', type: 'string'},
			{name: 'Password', type: 'string'},
			{name: 'Nama', type: 'string'},
			{name: 'Foto', type: 'string'},
			{name: 'Telepon', type: 'string'},
			{name: 'JabatanId', type: 'integer'}
		];
		
		tugasku.RecordSetJabatan = [
			{name: 'JabatanId', type: 'integer'},
			{name: 'Nama', type: 'string'},
			{name: 'Deskripsi', type: 'string'}
		];
		
		tugasku.RecordObjPengguna = Ext.data.Record.create(tugasku.RecordSetPengguna);
	
		tugasku.JabatanStore = new Ext.data.Store({
			url: './Pengguna/fetchJabatan.json',
			autoLoad: true,
			reader: new Ext.data.JsonReader({
				id: 'JabatanId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetJabatan
			})
		});
		
		tugasku.PenggunaStore = new Ext.data.Store({
			url: './Pengguna/pengguna.json',
			autoLoad: false,
			reader: new Ext.data.JsonReader({
				id: 'penggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetPengguna
			})
		});
		
		var penggunaGrid = new Ext.grid.EditorGridPanel({
			id: 'grid-pengguna',
			store: tugasku.PenggunaStore,
			sm: new Ext.grid.RowSelectionModel(),
			region: 'center',
			viewConfig: {
				forceFit: true
			},
			loadMask: true,
			tbar: [{
				text: 'Tambah Pengguna',
				tooltip: 'Klik untuk menambah pengguna',
				iconCls: 'add',
				handler: function(btn){
					frmTambah.show();
					simple.form.setValues({
						pid: 0,
						username: '',
						password: '',
						nama: '',
						telepon: '',
						foto: null,
						jabatan_id: null
					});
				}
			},'-',{
				text: 'Hapus Pengguna',
				tooltip: 'Klik untuk menghapus pengguna yang dipilih',
				iconCls: 'remove',
				handler: function(btn){
					checkedHapus = penggunaGrid.selModel.getSelected();
					if(!(checkedHapus)){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data yang mau dihapus..');
						return false;
					}else{
						Ext.Ajax.request({
							waitMsg: 'Menghapus...',
							url: '/Pengguna/delete.json',
							params: { id: checkedHapus.data.PenggunaId },
							failure: function(response, options){
								Ext.Msg.alert('Error', 'Something wrong...');
							},
							success: function(response, options){
								Ext.Msg.alert('Ok', 'Data berhasil dihapus');
								tugasku.PenggunaStore.reload();
							}
						});
					}
				}
			},'-',{
				text: 'Ubah Pengguna',
				tooltip: 'Klik untuk mengubah baris terpilih',
				iconCls: 'edit',
				handler: function(btn){
					if(!penggunaGrid.getSelectionModel().getSelected()){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris...');
						return;
					}
					var data = penggunaGrid.getSelectionModel().getSelected().data;
					frmTambah.show();
					
					simple.form.setValues({
						pid: data.PenggunaId,
						username: data.Username,
						password: data.Password,
						nama: data.Nama,
						telepon: data.Telepon,
						foto: null,
						jabatan_id: null
					});
				}
			},'->',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh grid',
				iconCls: 'refresh',
				handler: function(btn){
					tugasku.PenggunaStore.reload();
				}
			}],
			columns: [
				/*{
					header: 'ID',
					width: 15,
					editor: false,
					sortable: true,
					dataIndex: 'PenggunaId'
				},*/{
					header: 'Username',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Username'
				},{
					header: 'Password',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Password'
				},{
					header: 'Nama',
					width: 100,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					sortable: true,
					dataIndex: 'Nama'
				},{
					header: 'Telepon',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Telepon'
				},{
					header: 'Foto',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Foto'
				},{
					name: 'Jabatan',
					header: 'Jabatan',
					width: 100,
					sortable: true,
					renderer: function(v, params, record){
						record = tugasku.JabatanStore.getById(v);
						if(record){
							return record.get('Nama');
						}else{
							return v;
						}
					},
					// editor: new Ext.form.ComboBox({
						// store: tugasku.JabatanStore,
						// displayField: 'Nama',
						// valueField: 'JabatanId',
						// typeAhead: true,
						// mode: 'local',
						// triggerAction: 'all',
						// emptyText: 'Pilih...',
						// selectOnFocus: true
					// }),
					dataIndex: 'JabatanId'
				}],
			bbar: new Ext.PagingToolbar({
				pageSize: 20,
				store: tugasku.PenggunaStore,
				displayInfo: true,
				displayMsg: 'Menampilkan [0] - [1] dari [2]',
				emptyMsg: 'Data tidak ditemukan'
			})
		});
	
		this.panel = new Ext.Panel({
			id: 'pengguna-tab',
			border: 'false',
			layout: 'border',
			title: 'Daftar Pengguna',
			closable: true,
			items: penggunaGrid
		});
		
		var simple = new Ext.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 120, // label settings here cascade unless overridden
        //url:'/Pengguna/save.json',
        bodyStyle:'padding:5px 5px 0',
        defaultType: 'textfield',
		fileUpload: true,
        items: [{
			name: 'pid',
			xtype: 'hidden'
		},{
			fieldLabel: 'Nama Pengguna',
			name: 'nama',
			allowBlank:false,
			anchor: '%100'
		},{
			fieldLabel: 'Username',
			name: 'username',
			allowBlank:false,
			anchor: '%100'
		},{
			fieldLabel: 'Password',
			name: 'password',
			allowBlank:false,
			anchor: '%100'
		},{
			fieldLabel: 'Telepon',
			name: 'telepon',
			allowBlank:false,
			anchor: '%100'
		},{
			xtype: 'fileuploadfield',
			fieldLabel: 'foto',
			name: 'foto',
			emptyText: 'Pilih File...'
		},{
			xtype: 'combo',
			fieldLabel: 'Jabatan',
			hiddenName: 'jabatan_id',
			store: tugasku.JabatanStore,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'JabatanId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		}]
    });

    var frmTambah = new Ext.Window({
		title: 'Tambah Pengguna',
		id: 'frmtambah',
		width: 380,
		height: 280,
		layout: 'fit',
		plain: 'true',
		bodyStyle: 'padding:5px;',
		buttonAlign: 'center',
		items: simple,
		closable: false,
		buttons: [{
			text: 'Simpan',
			iconCls: 'save',
			handler: function(){
				if(simple.getForm().isValid()){
					simple.getForm().submit({
						url: 'Pengguna/upload.php',
						waitMsg: 'Upload foto...',
						success: function(simple, o){
							if(o.result.success == 'true'){
								Ext.Msg.alert('Berhasil '+ o.result.message + '.');
							}else{
								//Ext.Msg.alert('Berhasil cuy' + o.result.message + '.');
							}
						},
						failure: function(response, o){
							//Ext.Msg.alert('Gagal ' + o.result.message + '.');
						}
					});
				}
			
				Ext.Ajax.request({
					waitMsg: 'Menyimpan...',
					url: 'Pengguna/save.json',
					method: 'POST',
					params: simple.getForm().getValues(),
					failure: function(response, options){
						Ext.Msg.alert('Warning', 'Response : ' + response );
					},
					success: function(response, options){
						var json = Ext.util.JSON.decode(response.responseText);
						
						if(json.success){
							Ext.Msg.alert('OK', ' Data Pengguna tersimpan.');
							setTimeout(function(){
								tugasku.PenggunaStore.reload();
							}, 1000);
							frmTambah.hide();
						}else if(json.success == false){
							Ext.Msg.alert('Error', json.message);
							setTimeout(function(){
								tugasku.PenggunaStore.reload();
							}, 1000);
							frmTambah.hide();							
						}	
					}
				});
			}		
		},{
			text: 'Batal',
			iconCls: 'logout',
			handler: function(){
				frmTambah.hide();
			}
		}]
	});
		
		var template = new Ext.Template([
			'<h2>Detail Pengguna</h2><br/>',
			'<table><tr><td rowspan="7">',
			'<img src="images/{Foto}" width="35%" height="35%"/></td></tr><tr><td>',
			'ID</td><td>: {PenggunaId}</td></tr><tr><td>',
			'Nama</td><td>: {Nama}</td></tr><tr><td>',
			'Username</td><td>: {Username}</td></tr><tr><td>',
			'Password</td><td>: {Password}</td></tr><tr><td>',
			'Telepon</td><td>: {Telepon}</td></tr><tr><td>',
			'Jabatan</td><td>: {JabatanId:this.lookupJabatan}</td></tr></table>',
			
		]);
		
		template.lookupJabatan = function(value){
			record = tugasku.JabatanStore.getById(value);
			if(record){
				return record.get('Nama');
			}else{
				return value;
			}
		}
				
		var detailPanel = new Ext.Panel({
			xtype: 'penggunadetail',
			itemId: 'penggunadetailPanel',
			region: 'south',
			tpl: template
		});
		
		Ext.apply(detailPanel, {
			bodyStyle: {
				background: '#ffffff',
				padding: '7px'
			},
			html: 'Silahkan pilih baris untuk menampilkan detil'
		});
		
		penggunaGrid.getSelectionModel().on('rowselect', function(sm, rowIdx, r) {
			var dp = Ext.getCmp('detailPanel');
			template.overwrite(detailPanel.body, r.data);
		});
		
		this.panel = new Xond.layout.UL({
			id: 'pengguna-tab',
			title: 'Pengguna',
			upperPanel: penggunaGrid,
			lowerPanel: detailPanel
		});
		
		setTimeout(function(){
			tugasku.PenggunaStore.load();
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});