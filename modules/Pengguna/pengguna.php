<?php

class PenggunaModule extends XondModule{
	var $component;
	
	function PenggunaModule(){
		parent::__construct();
		
		$this->registerUi('pengguna');
		//$this->setMinifyUi(false);
		$this->setTitle('Pengguna');
	}
	
	function execPengguna(){
		$c = new Criteria();
		$c->add(PenggunaPeer::JABATAN_ID, 3, Criteria::NOT_EQUAL);
		$rowCount = PenggunaPeer::doCount($c);
		$c->setOffset($_REQUEST['start']);
		$c->setLimit($_REQUEST['limit']);
		$penggunas = PenggunaPeer::doSelect($c);
		$fieldName = PenggunaPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($penggunas), $rowCount, $fieldName));
	}
	
	function execFetchJabatan(){
		$jabatanId = JabatanPeer::doSelect(new Criteria());
		$rowCount = JabatanPeer::docount(new Criteria());
		
		$fieldName = JabatanPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($jabatanId), $rowCount, $fieldName));
	}

	function execSave(){
		try{
			if($_POST['pid']){
				$n = PenggunaPeer::retrieveByPK($_POST['pid']);
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			}else{
				$n = new Pengguna();
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			}
			
			$n->setFoto($n->getUsername(). '.png');
			
			if($n->save()){
				$this->write("{ success : true }");
			}else{
				$this->write("{ success : false }");
			}
		}catch(Exception $e){
			$this->write(sprintf("{ success : false }"));
		}
	}
	
	function execUpload(){
		$uploadDir = 'images/';
		$theFileName = '';
					
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){
			$n = new Pengguna();
			$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			$uploadFile = $uploadDir . $n->getUsername() . '.png';
//			$uploadFile = $uploadDir . basename($_FILES['foto']['name']);
			if(move_uploaded_file($_FILES['foto']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'falsed';
			}
		}else{
			$success = 'false';
		}
		$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
		$this->write($result);
	}
	
	function execDelete(){
		$pengguna = PenggunaPeer::retrieveByPK($_REQUEST['id']);
		try{
			$pengguna->delete();
			$this->write("{ 'success' : true }");			
		}catch(Exception $e){
			$this->write("{ 'success' : false }");
		}
	}	
}

?>