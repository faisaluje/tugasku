<?php

class TugasModule extends XondModule{
	var $component;
	
	function TugasModule(){
		parent::__construct();
		
		$this->registerUi('tugas');
		//$this->setMinifyUi(false);
		$this->setTitle('Tugas');
	}
	
	function execTugas(){
		$c = new Criteria();
		if($_REQUEST["jenis_tugas_id"] > 0){
			$c->add(TugasPeer::JENIS_TUGAS_ID, $_REQUEST["jenis_tugas_id"]);
		}
		if($_REQUEST["nama"] != ""){
			$c->add(TugasPeer::NAMA, "%".$_REQUEST[nama]."%", Criteria::LIKE);
		}
		if($_REQUEST["nis"] != ""){
			$c->add(TugasPeer::NIS, "%".$_REQUEST[nis]."%", Criteria::LIKE);
		}
		$rowCount = TugasPeer::doCount($c);
		$c->setOffset($_REQUEST['start']);
		$c->setLimit($_REQUEST['limit']);
		$tugas = TugasPeer::doSelectJoinAll($c);
		foreach ($tugas as $tugasArr){
			$arr = $tugasArr->toArray();
			$arr["JenisTugas"] = $tugasArr->getJenisTugas()->getNama();
			$arr["MataPelajaran"] = $tugasArr->getMataPelajaran()->getNama();
			$arr["Siswa"] = $tugasArr->getSiswa()->getNama();
			$arr["Pengguna"] = $tugasArr->getPengguna()->getNama();
			$outArr[] = $arr;
		}
		$fieldName = TugasPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson($outArr, $rowCount, $fieldName));
	}
	
	function execGuru(){
		$c = new Criteria();
		$c->add(PenggunaPeer::JABATAN_ID, "2");
		$guru = PenggunaPeer::doSelect($c);
		$rowCount = PenggunaPeer::doCount($c);
		$fieldName = PenggunaPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($guru), $rowCount, $fieldName));
	}
	
	function execMataPelajaran(){
		$c = new Criteria();
		$mp = MataPelajaranPeer::doSelectJoinKompetensiKeahlian($c);
		foreach ($mp as $arrMp) {
			$arr = $arrMp->toArray();
			$arr["KompetensiKeahlian"] = $arrMp->getKompetensiKeahlian()->getNama();
			$outArr[] = $arr;
		}
		$fieldName = MataPelajaranPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson($outArr, $rowCount, $fieldName));
	}

	function execSave(){
		try{
			if($_POST['tid']){
				$n = TugasPeer::retrieveByPK($_POST['tid']);
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			}else{
				$n = new Tugas();
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			}
			
			$siswa = str_replace(" ", "_", $n->getSiswa()->getNama());
			$nmFile = str_replace(" ", "_", $n->getNama());
			$n->setFile($siswa . '_' . $nmFile . '.zip');
			if($n->getMataPelajaranId() == 4){
				$n->setAbstrak($siswa . '_' . $nmFile . '_Abstrak' . '.pdf');
			}
			if($n->save()){
				$this->write("{ success : true }");
			}else{
				$this->write("{ success : false }");
			}
		}catch(Exception $e){
			$this->write(sprintf("{ success : false }"));
		}
	}
	
	function execUpload(){
		$uploadDir = 'files/';
		$theFileName = '';
					
		if(is_uploaded_file($_FILES['file']['tmp_name'])){
			$uploadFile = $uploadDir . basename($_FILES['file']['name']);
			$n = new Tugas();
			$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			$siswa = str_replace(" ", "_", $n->getSiswa()->getNama());
			$nmFile = str_replace(" ", "_", $n->getNama());
			$uploadFile = $uploadDir . $siswa . '_' . $nmFile . '.zip';
			if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'false';
			}
		}else{
			$success = 'false';
		}
		$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
		$this->write($result);
	}
	
	function execAbstrak(){
		$uploadDir = 'files/';
		$theFileName = '';
					
		if(is_uploaded_file($_FILES['fileAbstrak']['tmp_name'])){
			$uploadFile = $uploadDir . basename($_FILES['fileAbstrak']['name']);
			$n = new Tugas();
			$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			$siswa = str_replace(" ", "_", $this->getAuth()->getUser()->getNama());
			$nmFile = str_replace(" ", "_", $n->getNama());
			$uploadFile = $uploadDir . $siswa . '_' . $nmFile . '_Abstrak' .'.pdf';
//			$uploadFile = $uploadDir . '_Abstrak' .'.pdf';
			if(move_uploaded_file($_FILES['fileAbstrak']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'false';
			}
		}else{
			$success = 'false';
		}
		$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
		$this->write($result);
	}
	
	function execDelete(){
		$tugas = TugasPeer::retrieveByPK($_REQUEST['id']);
		try{
			$tugas->delete();
			$this->write("{ 'success' : true }");			
		}catch(Exception $e){
			$this->write("{ 'success' : false }");
		}
	}	
}

?>