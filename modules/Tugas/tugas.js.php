tugasku.TugasModule = Ext.extend(Xond.Module, {
	name: 'tugas',
	title: 'Tugas Saya',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
		tugasku.RecordSetTugas = [{
			name: 'TugasId',
			type: 'float'
		}, {
			name: 'JenisTugasId',
			type: 'float'
		}, {
			name: 'MataPelajaranId',
			type: 'float'
		}, {
			name: 'Nis',
			type: 'string'
		}, {
			name: 'TanggalPengumpulan',
			type: 'string',
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'File',
			type: 'string'
		}, {
			name: 'PenggunaId',
			type: 'float'
		}];
		
		tugasku.RecordSetTugasGroup = [{
			name: 'TugasId',
			type: 'float'
		}, {
			name: 'JenisTugas',
			type: 'string'
		}, {
			name: 'MataPelajaran',
			type: 'string'
		}, {
			name: 'Siswa',
			type: 'string'
		}, {
			name: 'TanggalPengumpulan',
			type: 'string',
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'File',
			type: 'string'
		}, {
			name: 'Abstrak',
			type: 'string'
		}, {
			name: 'Pengguna',
			type: 'string'
		}];
		
		tugasku.RecordSetJenisTugas = [{
			name: 'JenisTugasId',
			type: 'float'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'Deskripsi',
			type: 'string'
		}];
		
		tugasku.RecordSetMataPelajaran = [{
			name: 'MataPelajaranId',
			type: 'float'
		}, {
			name: 'KompetensiKeahlian',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}];

		tugasku.RecordSetSiswa = [{
			name: 'Nis',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'JenisKelamin',
			type: 'string'
		}, {
			name: 'TempatLahir',
			type: 'string'
		}, {
			name: 'TanggalLahir',
			type: 'date',
			dateFormat: 'Y/m/d'
		}, {
			name: 'Telepon',
			type: 'string'
		}];

		tugasku.RecordSetPengguna = [{
			name: 'PenggunaId',
			type: 'float'
		}, {
			name: 'Username',
			type: 'string'
		}, {
			name: 'Password',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'Telepon',
			type: 'string'
		}, {
			name: 'Foto',
			type: 'string'
		}, {
			name: 'JabatanId',
			type: 'float'
		}];
		
		tugasku.GroupMp = new Ext.data.GroupingStore({
			reader: new Ext.data.JsonReader({
				idProperty: 'TugasId',
				root: 'rows',
				remoteGroup: true,
				remoteSort: true,
				fields: tugasku.RecordSetTugasGroup
			}),
			autoLoad: false,
			url: './TugasAll/Tugas.json',
			sortInfo: {field: 'TugasId', direction: 'ASC'},
			groupField: 'MataPelajaran'
		});
		
		tugasku.RecordObjTugas = Ext.data.Record.create(tugasku.RecordSetTugas);
		tugasku.StoreTugas = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'TugasId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetTugas
			}),
			autoLoad: false,
			url: './Tugas/Tugas.json',
			sortInfo: {
				field: 'MataPelajaranId',
				direction: "ASC"
			}
		});
		
		tugasku.RecordObjJenisTugas = Ext.data.Record.create(tugasku.RecordSetJenisTugas);
		tugasku.LookupStoreJenisTugas = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenisTugasId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetJenisTugas
			}),
			autoLoad: true,
			url: './data/JenisTugas.json',
			sortInfo: {
				field: 'JenisTugasId',
				direction: "ASC"
			}
		});
		
		tugasku.RecordObjMataPelajaran = Ext.data.Record.create(tugasku.RecordSetMataPelajaran);
		tugasku.LookupStoreMataPelajaran = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'MataPelajaranId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetMataPelajaran
			}),
			autoLoad: true,
			url: './data/MataPelajaran.json',
			sortInfo: {
				field: 'MataPelajaranId',
				direction: "ASC"
			}
		});
		
		var grpStoreMp = new Ext.data.GroupingStore({
			reader: new Ext.data.JsonReader({
				id: 'MataPelajaranId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetMataPelajaran
			}),
			autoLoad: false,
			url: './Tugas/MataPelajaran.json',
			sortInfo: {field: 'MataPelajaranId', direction: 'ASC'},
			groupField: 'KompetensiKeahlian'
		});
		
		tugasku.RecordObjSiswa = Ext.data.Record.create(tugasku.RecordSetSiswa);
		tugasku.LookupStoreSiswa = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'Nis',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetSiswa
			}),
			autoLoad: true,
			url: './data/Siswa.json',
			sortInfo: {
				field: 'Nis',
				direction: "ASC"
			}
		});

		tugasku.RecordObjPengguna = Ext.data.Record.create(tugasku.RecordSetPengguna);
		tugasku.LookupStorePengguna = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetPengguna
			}),
			autoLoad: true,
			url: './Tugas/guru.json',
			sortInfo: {
				field: 'PenggunaId',
				direction: "ASC"
			}
		});
		
		var gridTugas = new Ext.grid.EditorGridPanel({
			id: 'grid-Tugas',
			// store: tugasku.StoreTugasAll,
			ds: tugasku.GroupMp,
			sm: new Ext.grid.RowSelectionModel(),
			region: 'center',
			tbar: [{
			text: 'Tambah Tugas',
				tooltip: 'Klik untuk menambah tugas',
				iconCls: 'add',
				handler: function(btn){
					tugasWin.show();
					pnlTugas.form.reset();
					pnlAbstrak.form.reset();
					pnlTugas.form.setValues({
						tid: 0,
						jenis_tugas_id: null,
						// mata_pelajaran_id: null,
						nis: '0'+ <?=$this->getAuth()->getUser()->getUsername()?>,
						tanggal_pengumpulan : null,
						nama: '',
						file: null,
						// pengguna_id: <?=$this->getAuth()->getUser()->getPenggunaId()?>
						pengguna_id: null
					});
				}
			},'->',{
				// xtype: 'combo',
				// hiddenName: 'jenis_tugas',
				// store: tugasku.LookupStoreJenisTugas,
				// listeners: {
					// select: function(thisfield, record, index){
						// tugasku.StoreTugas.reload({params: { jenis_tugas_id : thisfield.getValue()}});
					// }
				// },
				// displayField: 'Nama',
				// emptyText: 'Pilih Jenis Tugas...',
				// valueField: 'JenisTugasId',
				// editable: false,
				// typeAhead: true,
				// mode: 'local',
				// selectOnFocus: true,
				// triggerAction: 'all',
				// anchor: '100%'
			// },'-',{
				xtype: 'textfield',
				hiddenName: 'nama',
				enableKeyEvents: true,
				listeners: {
					keypress: function(field, e){
						if(e.getKey() == Ext.EventObject.ENTER){
							tugasku.GroupMp.reload({params: { nama : field.getValue(), nis : <?=$this->getAuth()->getUser()->getUsername()?>}});
						}
					}
				},
				emptyText: 'Ketikkan nama tugas..'
			},'-',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh grid',
				iconCls: 'refresh',
				handler: function(btn){
					tugasku.GroupMp.reload({params: { nis : <?=$this->getAuth()->getUser()->getUsername()?>}});
				}
			}],
			columns: [
				{
					header: 'Jenis Tugas',
					width: 100,
					sortable: true,
					dataIndex: 'JenisTugas'
				},{
					header: 'Mata Pelajaran',
					width: 100,
					sortable: true,
					dataIndex: 'MataPelajaran'
				},{
					header: 'Tanggal Pengumpulan',
					width: 50,
					sortable: true,
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					dataIndex: 'TanggalPengumpulan'
				},{
					header: 'Nama Tugas',
					width: 100,
					sortable: true,
					dataIndex: 'Nama'
				},{
					name: 'Guru',
					header: 'Guru',
					width: 100,
					sortable: true,
					dataIndex: 'Pengguna'
				}],
			view: new Ext.grid.GroupingView({
				forceFit: true,
				showGroupName: false,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Tugas" : "Tugas"]})',
				hideGroupedColumn: true
			}),
			border: false,
			frame: false,
			loadMask: true,
			animCollapse: false,
			bbar: new Ext.PagingToolbar({
				pageSize: 20,
				store: tugasku.GroupTugas,
				displayInfo: true,
				displayMsg: 'Menampilkan [0] - [1] dari [2]',
				emptyMsg: 'Data tidak ditemukan'
			})
		});
		
		var grpCboTugas = new Ext.ux.form.GroupComboBox({
			xtype: 'uxgroupcombo',			
			fieldLabel: 'MataPelajaran',
			hiddenName: 'mata_pelajaran_id',
			displayField: 'Nama',
			valueField: 'MataPelajaranId',
			store: grpStoreMp,
			mode: 'local',
			triggerAction: 'all',
			editable: false,
			typeAhead: false,
			width: 150,
			groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Komponen" : "Komponen"]})',
			showGroupName: false,
			startCollapsed: true,
			emptyText:'Pilih Mata Pelajaran..'
		});
		
		var pnlTugas = new Ext.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 120, // label settings here cascade unless overridden
        //url:'/Tugas/save.json',
        bodyStyle:'padding:5px 5px 0',
        defaultType: 'textfield',
		fileUpload: true,
        items: [{
			name: 'tid',
			xtype: 'hidden'
		},{
			xtype: 'combo',
			fieldLabel: 'Jenis Tugas',
			hiddenName: 'jenis_tugas_id',
			store: tugasku.LookupStoreJenisTugas,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'JenisTugasId',
			typeAhead: true,
			// listeners:{
				 // 'select': function(combo){
					// var isi = pnlTugas.getForm().getValues();
					// pnlTugas.getForm().setValues({
						// nama: isi['jenis_tugas_id']
					// });
					// pnlTugas.add({
						// xtype: 'fileuploadfield',
						// fieldLabel: 'Abstrak',
						// name: 'abstrak',
						// emptyText: 'Pilih File berupa .pdf'
					// });
				 // }
			// },
			mode: 'local',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		},
		{
			// xtype: 'uxgroupcombo',			
			// fieldLabel: 'MataPelajaran',
			// hiddenName: 'mata_pelajaran_id',
			// displayField: 'Nama',
			// valueField: 'MataPelajaranId',
			// store: grpStoreMp,
			// mode: 'local',
			// triggerAction: 'all',
			// editable: false,
			// typeAhead: false,
			// width: 150,
			// groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Komponen" : "Komponen"]})',
			// showGroupName: false,
			// startCollapsed: true,
			// emptyText:'Pilih Mata Pelajaran..',
			// allowBlank: false
			xtype: 'combo',
			fieldLabel: 'Mata Pelajaran',
			hiddenName: 'mata_pelajaran_id',
			store: tugasku.LookupStoreMataPelajaran,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'MataPelajaranId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		},{
			name: 'nis',
			xtype: 'hidden'
		},{
			xtype: 'datefield',
			fieldLabel: 'Tanggal Pengumpulan',
			name: 'tanggal_pengumpulan',
			emptyText: 'Pilih Tanggal...',
		},{
			fieldLabel: 'Nama Tugas',
			name: 'nama',
			allowBlank:false,
			anchor: '%100'
		},{
			xtype: 'fileuploadfield',
			fieldLabel: 'File',
			name: 'file',
			emptyText: 'Pilih File Berupa .zip',
			allowBlank:false
		},{
			xtype: 'combo',
			fieldLabel: 'Guru',
			hiddenName: 'pengguna_id',
			store: tugasku.LookupStorePengguna,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'PenggunaId',
			typeAhead: true,
			mode: 'remote',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		}]
    });

    var tugasWin = new Ext.Window({
		title: 'Tambah Tugas',
		id: 'tugaswin',
		width: 380,
		height: 270,
		layout: 'fit',
		plain: 'true',
		bodyStyle: 'padding:5px;',
		buttonAlign: 'center',
		items: pnlTugas,
		closable: false,
		buttons: [{
			text: 'Simpan',
			iconCls: 'save',
			handler: function(){
				if(pnlTugas.getForm().isValid()){
						pnlTugas.getForm().submit({
							url: 'Tugas/upload.php',
							waitMsg: 'Upload file...',
							success: function(pnlTugas, o){
								if(o.result.success == 'true'){
									Ext.Msg.alert('Berhasil '+ o.result.message + '.');
								}else{
									//Ext.Msg.alert('Berhasil cuy' + o.result.message + '.');
								}
							},
							failure: function(response, o){
								Ext.Msg.alert('Gagal ' + o.result.message + '.');
							}
						});
					}else{
						Ext.Msg.alert('Error', 'Data tidak lengkap..');
						return;
					}
				
				var isi = pnlTugas.getForm().getValues();
				if(isi['jenis_tugas_id'] == 4){
					abstrakWin.show();
					return;
				}else{
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',
						url: 'Tugas/save.json',
						method: 'POST',
						params: pnlTugas.getForm().getValues(),
						failure: function(response, options){
							Ext.Msg.alert('Warning', 'Response : ' + response );
						},
						success: function(response, options){
							var json = Ext.util.JSON.decode(response.responseText);
							
							if(json.success){
								Ext.Msg.alert('OK', ' Data Pengguna tersimpan.');
								setTimeout(function(){
									tugasku.GroupMp.reload();
								}, 1000);
								tugasWin.hide();
							}else if(json.success == false){
								Ext.Msg.alert('Error', json.message);	
							}
						}
					});
				}			
			}		
		},{
			text: 'Batal',
			iconCls: 'logout',
			handler: function(){
				tugasWin.hide();
				abstrakWin.hide();
			}
		}]
	});
	
		var pnlAbstrak = new Ext.FormPanel({
			baseCls: 'x-plain',
			labelWidth: 50,
			bodyStyle: 'padding:5px 5px 0',
			fileUpload: true,
			items: [{
				name: 'nama',
				xtype: 'hidden'
			},{
				xtype: 'fileuploadfield',
				fieldLabel: 'Abstrak',
				name: 'fileAbstrak',
				emptyText: 'Pilih File berupa .pdf'
			}]			
		});
	
		var abstrakWin = new Ext.Window({
			title: 'Upload Abstrak',
			id: 'abstrakwin',
			width: 290,
			height: 115,
			layout: 'fit',
			plain: 'true',
			bodyStyle: 'padding:5px;',
			buttonAlign: 'center',
			items: pnlAbstrak,
			closable: false,
			buttons: [{
				text: 'Upload',
				iconCls: 'save',
				handler: function(){
					var isi = pnlTugas.getForm().getValues();
					pnlAbstrak.getForm().setValues({ nama: isi['nama'] });
					if(pnlAbstrak.getForm().isValid()){
						pnlAbstrak.getForm().submit({
							url: 'Tugas/abstrak.php',
							waitMsg: 'Upload file...',
							success: function(pnlAbstrak, o){
								if(o.result.success == 'true'){
									Ext.Msg.alert('Berhasil '+ o.result.message + '.');
								}else{
									// Ext.Msg.alert('Berhasil cuy' + o.result.message + '.');
									// return;
								}
							},
							failure: function(response, o){
								// Ext.Msg.alert('Gagal ' + o.result.message + '.');
								// return;
							}
						});
					}else{
						Ext.Msg.alert('Error', 'Data tidak lengkap..');
						return;
					}
				
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',
						url: 'Tugas/save.json',
						method: 'POST',
						params: pnlTugas.getForm().getValues(),
						failure: function(response, options){
							Ext.Msg.alert('Warning', 'Response : ' + response );
						},
						success: function(response, options){
							var json = Ext.util.JSON.decode(response.responseText);
							
							if(json.success){
								Ext.Msg.alert('OK', ' Data Pengguna tersimpan.');
								setTimeout(function(){
									tugasku.StoreTugas.reload();
								}, 1000);
								tugasWin.hide();
								abstrakWin.hide();
							}else if(json.success == false){
								Ext.Msg.alert('Error', json.message);	
							}
						}
					});
				}
			}]
		});
		
		grpStoreMp.load();
		
		this.panel = new Ext.Panel({
			id: 'tugas-tab',
			border: 'false',
			layout: 'border',
			title: 'Daftar Tugas Saya',
			closable: true,
			items: gridTugas
		});
		
		setTimeout(function(){
			tugasku.GroupMp.load({params: { nis : <?=$this->getAuth()->getUser()->getUsername()?>}});
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});