<?php

class SiswaModule extends XondModule{
	var $component;
	
	function SiswaModule(){
		parent::__construct();
		
		$this->registerUi('siswa');
		//$this->setMinifyUi(false);
		$this->setTitle('Siswa');
	}
	
	function execFetch(){
		$c = new Criteria();
		$rowCount = SiswaPeer::doCount($c);
		$c->setOffset($_REQUEST['start']);
		$c->setLimit($_REQUEST['limit']);
		$siswas = SiswaPeer::doSelect($c);
		$fieldName = SiswaPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($siswas), $rowCount, $fieldName));
	}
	
		
	function execSave(){
	try{
			if($_POST['sid']){
				$c = new Criteria();
				$c->add(SiswaPeer::SISWA_ID,$_POST['sid']);
				$siswa = SiswaPeer::doSelect($c);
				foreach ($siswa as $siswaArr){
					$foto = $siswaArr->getFoto();
				}				
				$n = SiswaPeer::retrieveByPK($_POST['sid']);
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
				$n->setFoto($foto);
			}else{
				$n = new Siswa();
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
				$n->setFoto($n->getNis() .'.png');
				$pass = substr($n->getNis(), 5, 5);
				$s = new Pengguna();
				$s->setUsername($n->getNis());
				$s->setPassword($pass);
				$s->setNama($n->getNama());
				$s->setTelepon($n->getTelepon());
				$s->setFoto($n->getFoto());
				$s->setJabatanId(3);
				$s->save();
			}
			
			if($n->save()){
				$this->write("{ success : true }");
			}else{
				$this->write("{ success : false }");
			}
		}catch(Exception $e){
			$this->write(sprintf("{ success : false }"));
		}
	}
	
	function execUpload(){
		$uploadDir = 'images/';
		$theFileName = '';
					
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){
			$n = new Siswa();
			$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			$uploadFile = $uploadDir . $n->getNis() . '.png';
			if(move_uploaded_file($_FILES['foto']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'false';
			}
		}else{
			$success = 'false';
		}
		$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
		$this->write($result);
	}
	
	function execDelete(){
		$siswa = SiswaPeer::retrieveByPK($_REQUEST['id']);
		try{
			$siswa->delete();
			$this->write("{ 'success' : true }");			
		}catch(Exception $e){
			$this->write("{ 'success' : false }");
		}
	}	
}

?>