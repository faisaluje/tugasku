tugasku.SiswaModule = Ext.extend(Xond.Module, {
	name: 'siswa',
	title: 'Siswa',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
		Ext.QuickTips.init();
		
		var grid = Ext.grid;
	
		tugasku.RecordSetSiswa = [
			{name: 'SiswaId'},
			{name: 'Nis', type: 'string'},
			{name: 'Nama', type: 'string'},
			{name: 'JenisKelamin', type: 'string'},
			{name: 'TempatLahir', type: 'string'},
			{name: 'TanggalLahir', type: 'string'},
			{name: 'Telepon', type: 'string'},
			{name: 'Kelas', type: 'string'}
		];
		
		tugasku.KelasGroup = new Ext.data.GroupingStore({
			reader: new Ext.data.JsonReader({
				idProperty: 'SiswaId',
				root: 'rows',
				remoteGroup: true,
				remoteSort: true,
				fields: tugasku.RecordSetSiswa
			}),
			autoLoad: false,
			url: './Siswa/fetch.json',
			sortInfo: {field: 'Nis', direction: "ASC"},
			groupField: 'Kelas'
		});
		
		var gridSiswa = new grid.EditorGridPanel({
			ds: tugasku.KelasGroup,
			sm: new Ext.grid.RowSelectionModel(),
			region: 'center',
			tbar: [{
				text: 'Tambah Siswa',
				tooltip: 'Klik untuk menambah siswa',
				iconCls: 'add',
				handler: function(btn){
					winSiswa.show();
					pnlSiswa.form.reset();
					pnlSiswa.form.setValues({
						sid: 0
					});
				}
			},'-',{
				text: 'Hapus Siswa',
				tooltip: 'Klik untuk menghapus siswa yang dipilih',
				iconCls: 'remove',
				handler: function(btn){
					checkedHapus = gridSiswa.selModel.getSelected();
					if(!(checkedHapus)){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data yang mau dihapus..');
						return false;
					}else{
						Ext.Ajax.request({
							waitMsg: 'Menghapus...',
							url: './Siswa/delete.json',
							params: { id: checkedHapus.data.SiswaId },
							failure: function(response, options){
								Ext.Msg.alert('Error', 'Something wrong...');
							},
							success: function(response, options){
								Ext.Msg.alert('Ok', 'Data berhasil dihapus');
								tugasku.KelasGroup.reload();
							}
						});
					}
				}
			},'-',{
				text: 'Ubah Siswa',
				tooltip: 'Klik untuk mengubah baris terpilih',
				iconCls: 'edit',
				handler: function(btn){
					if(!gridSiswa.getSelectionModel().getSelected()){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris...');
						return;
					}
					var data = gridSiswa.getSelectionModel().getSelected().data;
					winSiswa.show();
					
					pnlSiswa.form.setValues({
						sid: data.SiswaId,
						nis: data.Nis,
						nama: data.Nama,
						tempat_lahir: data.TempatLahir,
						tanggal_lahir : data.TanggalLahir,
						telepon: data.Telepon,
						kelas: data.Kelas,
						foto: data.Foto
					});
				}
			},'->',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh',
				iconCls: 'refresh',
				handler: function(btn){
					tugasku.KelasGroup.reload();
				}
			}],
			columns: [
				{
					header: 'NIS',
					width: 75,
					editor: false,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Nis'
				},{
					header: 'Nama',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Nama'
				},{
					header: 'Jenis Kelamin',
					width: 100,
					sortable: true,
					// editor: new Ext.form.ComboBox({
						// store: new Ext.data.JsonStore({
							// fields: ['jenis_kelamin'],
							// data: [
								// {jenis_kelamin: 'Laki-laki'},
								// {jenis_kelamin: 'Perempuan'}
							// ]
						// }),
						// displayField: 'jenis_kelamin',
						// valueField: 'jenis_kelamin',
						// typeAhead: true,
						// mode: 'local',
						// triggerAction: 'all',
						// selectOnFocus: true
					// }),
					dataIndex: 'JenisKelamin'
				},{
					header: 'Tempat Lahir',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'TempatLahir'
				},{
					header: 'Tanggal Lahir',
					width: 100,
					sortable: true,
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					// editor: new fm.DateField({
						// format: 'd/m/Y',
						// minValue: '01/01/1920'
					// }),
					dataIndex: 'TanggalLahir'
				},{
					header: 'Telepon',
					width: 100,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Telepon'
				},{
					header: 'Kelas',
					width: 20,
					sortable: true,
					// editor: new fm.TextField({
						// allowBlank: false
					// }),
					dataIndex: 'Kelas'
				}],
				view: new Ext.grid.GroupingView({
					forceFit: true,
					showGroupName: false,
					groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Siswa" : "Siswa"]})',
					hideGroupedColumn: true
				}),
				border: false,
				frame: false,
				loadMask: true,
				animCollapse: false
		});
		
		var pnlSiswa = new Ext.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 120, // label settings here cascade unless overridden
        bodyStyle:'padding:5px 5px 0',
        defaultType: 'textfield',
		fileUpload: true,
        items: [{
			name: 'sid',
			xtype: 'hidden'
		},{
			fieldLabel: 'Nis',
			name: 'nis',
			allowBlank:false,
			anchor: '%75'
		},{
			fieldLabel: 'Nama Siswa',
			name: 'nama',
			allowBlank:false,
			anchor: '%100'
		},{
			xtype: 'radiogroup',
			fieldLabel: 'Jenis Kelamin',
			name: 'jenis_kelamin',
			anchor: '100%',
			items: [{
				boxLabel: 'Laki-laki', name: 'jenis_kelamin', inputValue: 'Laki-laki' , checked: true 
			},{
				boxLabel: 'Perempuan', name: 'jenis_kelamin', inputValue: 'Perempuan'
			}]				
		},{
			fieldLabel: 'Tempat Lahir',
			name: 'tempat_lahir',
			allowBlank:false,
			anchor: '%75'
		},{
			xtype: 'datefield',
			fieldLabel: 'Tanggal Lahir',
			name: 'tanggal_lahir',
			emptyText: 'Pilih Tanggal...',
			format: 'd/m/Y'
		},{
			fieldLabel: 'Telepon',
			name: 'telepon',
			allowBlank:false,
			anchor: '%75'
		},{
			fieldLabel: 'Kelas',
			name: 'kelas',
			allowBlank:false,
			anchor: '%50'
		},{
			xtype: 'fileuploadfield',
			fieldLabel: 'Foto',
			name: 'foto',
			emptyText: 'Pilih foto... (.png)'
		}]
    });

    var winSiswa = new Ext.Window({
		title: 'Tambah Siswa',
		id: 'winSiswa',
		width: 380,
		height: 300,
		layout: 'fit',
		plain: 'true',
		bodyStyle: 'padding:5px;',
		buttonAlign: 'center',
		items: pnlSiswa,
		closable: false,
		buttons: [{
			text: 'Simpan',
			iconCls: 'save',
			handler: function(){
				if(pnlSiswa.getForm().isValid()){
					pnlSiswa.getForm().submit({
						url: 'Siswa/upload.php',
						waitMsg: 'Upload file...',
						success: function(pnlSiswa, o){
							if(o.result.success == 'true'){
								Ext.Msg.alert('Berhasil '+ o.result.message + '.');
							}else{
								//Ext.Msg.alert('Berhasil cuy' + o.result.message + '.');
							}
						},
						failure: function(response, o){
							//Ext.Msg.alert('Gagal ' + o.result.message + '.');
						}
					});
				}
			
				Ext.Ajax.request({
					waitMsg: 'Menyimpan...',
					url: 'Siswa/save.json',
					method: 'POST',
					params: pnlSiswa.getForm().getValues(),
					failure: function(response, options){
						Ext.Msg.alert('Warning', 'Response : ' + response );
					},
					success: function(response, options){
						var json = Ext.util.JSON.decode(response.responseText);
						
						if(json.success){
							Ext.Msg.alert('OK', ' Data Siswa tersimpan.');
							setTimeout(function(){
								tugasku.KelasGroup.load();
							}, 1000);
							winSiswa.hide();
						}else if(json.success == false){
							Ext.Msg.alert('Error', json.message);	
						}
					}
				});
			}		
		},{
			text: 'Batal',
			iconCls: 'logout',
			handler: function(){
				winSiswa.hide();
			}
		}]
	});
	
		this.panel = new Ext.Panel({
			id: 'siswa-tab',
			border: 'false',
			layout: 'border',
			title: 'Daftar Siswa',
			closable: true,
			items: gridSiswa
		});
	
		setTimeout(function(){
			tugasku.KelasGroup.load();
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});