<?php

class MataPelajaranModule extends XondCrudModule{
	var $component;
	
	function MataPelajaranModule(){
		parent::__construct();
		$this->registerUi('default', $this->LIBDIR);
//		$this->setMinifyUi(false);
		$this->setTitle('MataPelajaran');
		$this->setParams();
	}
	
	function setParams(){
		$this->params['colNames'] = array('ID','','Nama','NSS','NPSN','Jenis Sekolah','Alamat','RT','RW','Dusun','Desa/Kelurahan','Kecamatan','Kabupaten/Kota','Kode Pos','Kategori Wilayah','Lintang','Bujur','Telepon','Fax','Akses Internet','ISP','E-mail','Website','Status Sekolah','Status Kepemilikan','SK Pendirian','Tanggal SK Pendirian','SK Izin Operasional','Tanggal SK Izin Operasional','Akreditasi','No SK Akreditasi','Tanggal SK Akreditasi','Status Mutu','Sertifikat ISO','Waktu Penyelenggaraan','Gugus Sekolah','Kategori Sekolah','No. Rekening','Nama Bank','Rekening a/n','MBS','Sember Listrik','Sumber Air','Daya Listrik','Yayasan','Flag');
		$this->params['colwidths'] = array(40, 200);
	}
}

?>