tugasku.PenggunaModule = Ext.extend(Xond.Module, {
	name: 'pengguna',
	title: 'Pengguna',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
		tugasku.RecordSetPengguna = [
			{name: 'PenggunaId'},
			{name: 'Username', type: 'string'},
			{name: 'Password', type: 'string'},
			{name: 'Nama', type: 'string'},
			{name: 'Foto', type: 'string'},
			{name: 'Telepon', type: 'string'},
			{name: 'JabatanId', type: 'integer'}
		];
		
		tugasku.RecordSetJabatan = [
			{name: 'JabatanId', type: 'integer'},
			{name: 'Nama', type: 'string'},
			{name: 'Deskripsi', type: 'string'}
		];
		
		tugasku.RecordObjPengguna = Ext.data.Record.create(tugasku.RecordSetPengguna);
	
		tugasku.JabatanStore = new Ext.data.Store({
			url: './Pengguna/fetchJabatan.json',
			autoLoad: true,
			reader: new Ext.data.JsonReader({
				id: 'JabatanId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetJabatan
			})
		});
		
		tugasku.PenggunaStore = new Ext.data.Store({
			url: './Pengguna/fetch.json',
			autoLoad: true,
			reader: new Ext.data.JsonReader({
				id: 'penggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetPengguna
			})
		});
		
		var penggunaGrid = new Ext.grid.EditorGridPanel({
			id: 'grid-pengguna',
			store: tugasku.PenggunaStore,
			sm: new Ext.grid.RowSelectionModel(),
			region: 'center',
			viewConfig: {
				forceFit: true
			},
			loadMask: true,
			tbar: [{
				text: 'Tambah Pengguna',
				tooltip: 'Klik untuk menambah pengguna',
				iconCls: 'add',
				handler: function(btn){
					var r = new tugasku.RecordObjPengguna();
					penggunaGrid.stopEditing();
					penggunaGrid.store.insert(0, r);
					penggunaGrid.startEditing(0, 1);
				}
			},'-',{
				text: 'Hapus Pengguna',
				tooltip: 'Klik untuk menghapus pengguna yang dipilih',
				iconCls: 'remove',
				handler: function(btn){
					checkedHapus = penggunaGrid.selModel.getSelected();
					if(!(checkedHapus)){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data yang mau dihapus..');
						return false;
					}else{
						Ext.Ajax.request({
							waitMsg: 'Menghapus...',
							url: '/Pengguna/delete.json',
							params: { id: checkedHapus.data.PenggunaId },
							failure: function(response, options){
								Ext.Msg.alert('Error', 'Something wrong...');
							},
							success: function(response, options){
								Ext.Msg.alert('Ok', 'Data berhasil dihapus');
								tugasku.PenggunaStore.reload();
							}
						});
					}
				}
			},'-',{
				text: 'Simpan Perubahan',
				tooltip: 'Klik untuk menyimpan perubahan',
				iconCls: 'save',
				handler: function(btn){
					var count = 0;
					jsonData = "[";
					
					for(i=0;i<tugasku.PenggunaStore.getCount(); i++){
						record = tugasku.PenggunaStore.getAt(i);
						if(record.dirty){
							jsonData += Ext.util.JSON.encode(record.data) + ",";
							count++;
						}
					}
					jsonData = jsonData.substring(0,jsonData.length-1) + "]";
					
					if(!count > 0){
						Ext.Msg.alert('Warning', 'Tidak ada yang diperbaharui');
						return;
					}
					Ext.Ajax.request({
						waitMsg: 'Menyimpan...',
						url: 'Pengguna/save.json',
						method: 'POST',
						params: {
							data: jsonData
						},
						failure: function(response, options){
							Ext.Msg.alert('Warning', 'Response : ' + response );
						},
						success: function(response, options){
							var json = Ext.util.JSON.decode(response.responseText);
							
							if(json.success){
								Ext.Msg.alert('OK', json.affected + ' baris data tersimpan.');
								setTimeout(function(){
									tugasku.PenggunaStore.reload();
								}, 1000);
							}else if(json.success == false){
								Ext.Msg.alert('Error', json.message);	
							}
						}
					});
				}
			},'->',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh grid',
				iconCls: 'refresh',
				handler: function(btn){
					tugasku.PenggunaStore.reload();
				}
			}],
			columns: [
				{
					header: 'ID',
					width: 15,
					editor: false,
					sortable: true,
					dataIndex: 'PenggunaId'
				},{
					header: 'Username',
					width: 100,
					sortable: true,
					editor: new fm.TextField({
						allowBlank: false
					}),
					dataIndex: 'Username'
				},{
					header: 'Password',
					width: 100,
					sortable: true,
					editor: new fm.TextField({
						allowBlank: false
					}),
					dataIndex: 'Password'
				},{
					header: 'Nama',
					width: 100,
					editor: new fm.TextField({
						allowBlank: false
					}),
					sortable: true,
					dataIndex: 'Nama'
				},{
					header: 'Telepon',
					width: 100,
					sortable: true,
					editor: new fm.TextField({
						allowBlank: false
					}),
					dataIndex: 'Telepon'
				},{
					header: 'Foto',
					width: 100,
					sortable: true,
					editor: new fm.TextField({
						allowBlank: false
					}),
					dataIndex: 'Foto'
				},{
					header: 'Jabatan',
					width: 100,
					sortable: true,
					renderer: function(v, params, record){
						record = tugasku.JabatanStore.getById(v);
						if(record){
							return record.get('Nama');
						}else{
							return v;
						}
					},
					editor: new Ext.form.ComboBox({
						store: tugasku.JabatanStore,
						displayField: 'Nama',
						valueField: 'JabatanId',
						typeAhead: true,
						mode: 'local',
						triggerAction: 'all',
						emptyText: 'Pilih...',
						selectOnFocus: true
					}),
					dataIndex: 'JabatanId'
				}],
			bbar: new Ext.PagingToolbar({
				pageSize: 20,
				store: tugasku.PenggunaStore,
				displayInfo: true,
				displayMsg: 'Menampilkan [0] - [1] dari [2]',
				emptyMsg: 'Data tidak ditemukan'
			})
		});
	
		this.panel = new Ext.Panel({
			id: 'pengguna-tab',
			border: 'false',
			layout: 'border',
			title: 'Daftar Pengguna',
			closable: true,
			items: penggunaGrid
		});
		
		setTimeout(function(){
//			tugasku.PenggunaStore.load({params:{start:0,limit:20}});
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});