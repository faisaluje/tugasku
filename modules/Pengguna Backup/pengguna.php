<?php

class PenggunaModule extends XondModule{
	var $component;
	
	function PenggunaModule(){
		parent::__construct();
		
		$this->registerUi('pengguna');
		//$this->setMinifyUi(false);
		$this->setTitle('Pengguna');
	}
	
	function execFetch(){
		$c = new Criteria();
		$rowCount = PenggunaPeer::doCount($c);
		$c->setOffset($_REQUEST['start']);
		$c->setLimit($_REQUEST['limit']);
		$penggunas = PenggunaPeer::doSelect($c);
		$fieldName = PenggunaPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($penggunas), $rowCount, $fieldName));
	}
	
	function execFetchJabatan(){
		$jabatanId = JabatanPeer::doSelect(new Criteria());
		$rowCount = JabatanPeer::docount(new Criteria());
		
		$fieldName = JabatanPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson(getArray($jabatanId), $rowCount, $fieldName));
	}
	
	function execSave(){
		$data = splitJsonArray($_REQUEST['data']);
		//print_r($data);die();
		$rowAffected = 0;
		
		try{
			foreach ($data as $d){
				$row = json_decode(stripcslashes($d));
				$pengguna = PenggunaPeer::retrieveByPK($row->PenggunaId);
				
				if(!is_object($pengguna)){
					$pengguna = new Pengguna();
				}
				
				$pengguna->fromArray(get_object_vars($row), BasePeer::TYPE_PHPNAME);
				if($pengguna->save()){
					$rowAffected++;
				}				
			}
		}catch (Exception $ex){
			$message = $ex->getMessage();
			$success = 'false';
			$result = sprintf("{ 'success' : %s, 'affected' : '%s', 'message' : '%s' }", $success, $rowAffected, $message);
			die($result);
		}
		
		$succes = ($rowAffected > 0) ? 'true' : 'false';
		$result = sprintf("{ 'success' : %s, 'affected' : '%s' }", $succes, $rowAffected);
		$this->write($result);
	}
	
	function execDelete(){
		$pengguna = PenggunaPeer::retrieveByPK($_REQUEST['id']);
		try{
			$pengguna->delete();
			$this->write("{ 'success' : true }");			
		}catch(Exception $e){
			$this->write("{ 'success' : false }");
		}
	}	
}

?>