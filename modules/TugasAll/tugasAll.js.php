tugasku.TugasAllModule = Ext.extend(Xond.Module, {
	name: 'tugasAll',
	title: 'Tugas',
	panel: 'null',
	subMenu: 'Aplikasi',
	create: function(){
		tugasku.RecordSetTugas = [{
			name: 'TugasId',
			type: 'float'
		}, {
			name: 'JenisTugas',
			type: 'string'
		}, {
			name: 'MataPelajaran',
			type: 'string'
		}, {
			name: 'Siswa',
			type: 'string'
		}, {
			name: 'TanggalPengumpulan',
			type: 'string',
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'File',
			type: 'string'
		}, {
			name: 'Abstrak',
			type: 'string'
		}, {
			name: 'Pengguna',
			type: 'string'
		}];
		
		tugasku.RecordSetJenisTugas = [{
			name: 'JenisTugasId',
			type: 'float'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'Deskripsi',
			type: 'string'
		}];
		
		tugasku.RecordSetMataPelajaran = [{
			name: 'MataPelajaranId',
			type: 'float'
		}, {
			name: 'KompetensiKeahlianId',
			type: 'float'
		}, {
			name: 'Nama',
			type: 'string'
		}];

		tugasku.RecordSetSiswa = [{
			name: 'Nis',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'JenisKelamin',
			type: 'string'
		}, {
			name: 'TempatLahir',
			type: 'string'
		}, {
			name: 'TanggalLahir',
			type: 'date',
			dateFormat: 'Y/m/d'
		}, {
			name: 'Telepon',
			type: 'string'
		}];

		tugasku.RecordSetPengguna = [{
			name: 'PenggunaId',
			type: 'float'
		}, {
			name: 'Username',
			type: 'string'
		}, {
			name: 'Password',
			type: 'string'
		}, {
			name: 'Nama',
			type: 'string'
		}, {
			name: 'Telepon',
			type: 'string'
		}, {
			name: 'Foto',
			type: 'string'
		}, {
			name: 'JabatanId',
			type: 'float'
		}];
		
		tugasku.RecordObjTugas = Ext.data.Record.create(tugasku.RecordSetTugas);
		// tugasku.StoreTugasAll = new Ext.data.Store({
			// reader: new Ext.data.JsonReader({
				// id: 'TugasId',
				// root: 'rows',
				// totalProperty: 'results',
				// fields: tugasku.RecordSetTugas
			// }),
			// autoLoad: false,
			// url: './TugasAll/Tugas.json',
			// sortInfo: {
				// field: 'TugasId',
				// direction: "ASC"
			// }
		// });
		
		tugasku.GroupTugas = new Ext.data.GroupingStore({
			reader: new Ext.data.JsonReader({
				idProperty: 'TugasId',
				root: 'rows',
				remoteGroup: true,
				remoteSort: true,
				fields: tugasku.RecordSetTugas
			}),
			autoLoad: false,
			url: './TugasAll/Tugas.json',
			sortInfo: {field: 'TugasId', direction: 'ASC'},
			groupField: 'MataPelajaran'
		});
		
		tugasku.RecordObjJenisTugas = Ext.data.Record.create(tugasku.RecordSetJenisTugas);
		tugasku.LookupStoreJenisTugas = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'JenisTugasId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetJenisTugas
			}),
			autoLoad: true,
			url: './data/JenisTugas.json',
			sortInfo: {
				field: 'JenisTugasId',
				direction: "ASC"
			}
		});
		
		tugasku.RecordObjMataPelajaran = Ext.data.Record.create(tugasku.RecordSetMataPelajaran);
		tugasku.LookupStoreMataPelajaran = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'MataPelajaranId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetMataPelajaran
			}),
			autoLoad: true,
			url: './data/MataPelajaran.json',
			sortInfo: {
				field: 'MataPelajaranId',
				direction: "ASC"
			}
		});
		
		tugasku.RecordObjSiswa = Ext.data.Record.create(tugasku.RecordSetSiswa);
		tugasku.LookupStoreSiswa = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'Nis',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetSiswa
			}),
			autoLoad: true,
			url: './data/Siswa.json',
			sortInfo: {
				field: 'Nis',
				direction: "ASC"
			}
		});

		tugasku.RecordObjPengguna = Ext.data.Record.create(tugasku.RecordSetPengguna);
		tugasku.LookupStorePengguna = new Ext.data.Store({
			reader: new Ext.data.JsonReader({
				id: 'PenggunaId',
				root: 'rows',
				totalProperty: 'results',
				fields: tugasku.RecordSetPengguna
			}),
			autoLoad: true,
			url: './data/Pengguna.json',
			sortInfo: {
				field: 'PenggunaId',
				direction: "ASC"
			}
		});
		
		var gridTugasAll = new Ext.grid.EditorGridPanel({
			id: 'grid-TugasAll',
			// store: tugasku.StoreTugasAll,
			ds: tugasku.GroupTugas,
			sm: new Ext.grid.RowSelectionModel(),
			region: 'center',
			tbar: [{
			<?php if($this->getAuth()->getUser()->getJabatanId() == 1){ ?>
				text: 'Tambah Tugas',
				tooltip: 'Klik untuk menambah Tugas',
				iconCls: 'add',
				handler: function(btn){
					tugasAllWin.show();
					pnlTugasAll.form.setValues({
						tid: null,
						jenis_tugas_id: null,
						mata_pelajaran_id: null,
						nis: null,
						tanggal_pengumpulan : null,
						nama: '',
						file: null,
						pengguna_id: <?=$this->getAuth()->getUser()->getPenggunaId()?>
					});
				}
			},'-',{
				text: 'Hapus Tugas',
				tooltip: 'Klik untuk menghapus Tugas yang dipilih',
				iconCls: 'remove',
				handler: function(btn){
					checkedHapus = gridTugasAll.selModel.getSelected();
					if(!(checkedHapus)){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris data yang mau dihapus..');
						return false;
					}else{
						Ext.Ajax.request({
							waitMsg: 'Menghapus...',
							url: '/TugasAll/delete.json',
							params: { id: checkedHapus.data.TugasId },
							failure: function(response, options){
								Ext.Msg.alert('Error', 'Something wrong...');
							},
							success: function(response, options){
								Ext.Msg.alert('Ok', 'Data berhasil dihapus');
								tugasku.GroupTugas.reload();
							}
						});
					}
				}
			},'-',{
				text: 'Ubah Tugas',
				tooltip: 'Klik untuk mengubah baris terpilih',
				iconCls: 'edit',
				handler: function(btn){
					if(!gridTugasAll.getSelectionModel().getSelected()){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris...');
						return;
					}
					var data = gridTugasAll.getSelectionModel().getSelected().data;
					tugasAllWin.show();
					
					pnlTugasAll.form.setValues({
						tid: data.TugasId,
						jenis_tugas_id: null,
						mata_pelajaran_id: null,
						nis: null,
						tanggal_pengumpulan : null,
						nama: data.Nama,
						file: null,
						pengguna_id: <?=$this->getAuth()->getUser()->getPenggunaId()?>
					});
				}
			},{
				// text: 'Download Tugas',
				// iconCls: 'down',
				// handler: function(){
					// if(!gridTugasAll.getSelectionModel().getSelected()){
						// Ext.Msg.alert('Error', 'Mohon pilih salah satu baris...');
						// return;
					// }
					// var data = gridTugasAll.getSelectionModel().getSelected().data;
					// var nmFile = data.Nama;
					// var siswa = data.Siswa;
					
					// window.location.assign("/files/"+ siswa.replace(/ /g, "_") + "_" + nmFile.replace(/ /g, "_") + ".zip");
				// }
			<?php } ?>
				text: 'Download Abstrak',
				iconCls: 'down',
				handler: function(){
					if(!gridTugasAll.getSelectionModel().getSelected()){
						Ext.Msg.alert('Error', 'Mohon pilih salah satu baris...');
						return;
					}
					var data = gridTugasAll.getSelectionModel().getSelected().data;
					var nmFile = data.Abstrak;
					
					if(nmFile == ""){
						Ext.Msg.alert('Error', 'Tidak terdapat abstrak..');
					}else{
						window.location.assign("/files/"+ nmFile);
					}
				}
			},'->',{
				// xtype: 'combo',
				// hiddenName: 'jenis_tugas',
				// store: tugasku.LookupStoreJenisTugas,
				// listeners: {
					// select: function(thisfield, record, index){
						// tugasku.StoreTugasAll.reload({params: { jenis_tugas_id : thisfield.getValue()}});
					// }
				// },
				// displayField: 'Nama',
				// emptyText: 'Pilih Jenis TugasAll...',
				// valueField: 'JenisTugasId',
				// editable: false,
				// typeAhead: true,
				// mode: 'local',
				// selectOnFocus: true,
				// triggerAction: 'all',
				// anchor: '100%'
			// },'-',{
				xtype: 'textfield',
				hiddenName: 'nama',
				enableKeyEvents: true,
				listeners: {
					keypress: function(field, e){
						if(e.getKey() == Ext.EventObject.ENTER){
							tugasku.GroupTugas.reload({params: { nama : field.getValue()}});
						}
					}
				},
				emptyText: 'Pencarian...'
			},'-',{
				text: 'Refresh',
				tooltip: 'Klik untuk merefresh grid',
				iconCls: 'refresh',
				handler: function(btn){
					tugasku.GroupTugas.reload();
				}
			}],
			columns: [
				{
					header: 'Jenis Tugas',
					width: 100,
					sortable: true,
					dataIndex: 'JenisTugas'
				},{
					header: 'Mata Pelajaran',
					width: 100,
					sortable: true,
					dataIndex: 'MataPelajaran'
				},{
					header: 'Nama Siswa',
					width: 100,
					sortable: true,
					dataIndex: 'Siswa'
				},{
					header: 'Tanggal Pengumpulan',
					width: 50,
					sortable: true,
					renderer : Ext.util.Format.dateRenderer('d/m/Y'),
					dataIndex: 'TanggalPengumpulan'
				},{
					header: 'Nama Tugas',
					width: 100,
					sortable: true,
					dataIndex: 'Nama'
				},{
					name: 'Guru',
					header: 'Guru',
					width: 100,
					sortable: true,
					dataIndex: 'Pengguna'
				}],
			view: new Ext.grid.GroupingView({
				forceFit: true,
				showGroupName: false,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Tugas" : "Tugas"]})',
				hideGroupedColumn: true
			}),
			border: false,
			frame: false,
			loadMask: true,
			animCollapse: false,
			bbar: new Ext.PagingToolbar({
				pageSize: 20,
				store: tugasku.GroupTugas,
				displayInfo: true,
				displayMsg: 'Menampilkan [0] - [1] dari [2]',
				emptyMsg: 'Data tidak ditemukan'
			})
		});
	
		this.panel = new Ext.Panel({
			id: 'TugasAll-tab',
			border: 'false',
			layout: 'border',
			title: 'Daftar Tugas',
			closable: true,
			items: gridTugasAll
		});
		
		var pnlTugasAll = new Ext.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 120, // label settings here cascade unless overridden
        //url:'/TugasAll/save.json',
        bodyStyle:'padding:5px 5px 0',
        defaultType: 'textfield',
		fileUpload: true,
        items: [{
			name: 'tid',
			xtype: 'hidden'
		},{
			xtype: 'combo',
			fieldLabel: 'Jenis Tugas',
			hiddenName: 'jenis_tugas_id',
			store: tugasku.LookupStoreJenisTugas,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'JenisTugasId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		},{
			xtype: 'combo',
			fieldLabel: 'Mata Pelajaran',
			hiddenName: 'mata_pelajaran_id',
			store: tugasku.LookupStoreMataPelajaran,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'MataPelajaranId',
			typeAhead: true,
			mode: 'local',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		},{
			xtype: 'combo',
			fieldLabel: 'Nama Siswa',
			hiddenName: 'nis',
			store: tugasku.LookupStoreSiswa,
			displayField: 'Nama',
			emptyText: 'Pilih...',
			valueField: 'Nis',
			typeAhead: true,
			mode: 'remote',
			triggerAction: 'all',
			selectOnFocus: true,
			allowBlank: false
		},{
			xtype: 'datefield',
			fieldLabel: 'Tanggal Pengumpulan',
			name: 'tanggal_pengumpulan',
			emptyText: 'Pilih Tanggal...',
		},{
			fieldLabel: 'Nama Tugas',
			name: 'nama',
			allowBlank:false,
			anchor: '%100'
		},{
			xtype: 'fileuploadfield',
			fieldLabel: 'File',
			name: 'file',
			emptyText: 'Pilih File...'
		},{
			name: 'pengguna_id',
			xtype: 'hidden'
		}]
    });

    var tugasAllWin = new Ext.Window({
		title: 'Tambah Tugas',
		id: 'tugasAllWin',
		width: 380,
		height: 280,
		layout: 'fit',
		plain: 'true',
		bodyStyle: 'padding:5px;',
		buttonAlign: 'center',
		items: pnlTugasAll,
		closable: false,
		buttons: [{
			text: 'Simpan',
			iconCls: 'save',
			handler: function(){
				if(pnlTugasAll.getForm().isValid()){
					pnlTugasAll.getForm().submit({
						url: 'TugasAll/upload.php',
						waitMsg: 'Upload file...',
						success: function(pnlTugasAll, o){
							if(o.result.success == 'true'){
								Ext.Msg.alert('Berhasil '+ o.result.message + '.');
							}else{
								//Ext.Msg.alert('Berhasil cuy' + o.result.message + '.');
							}
						},
						failure: function(response, o){
							//Ext.Msg.alert('Gagal ' + o.result.message + '.');
						}
					});
				}
			
				Ext.Ajax.request({
					waitMsg: 'Menyimpan...',
					url: 'TugasAll/save.json',
					method: 'POST',
					params: pnlTugasAll.getForm().getValues(),
					failure: function(response, options){
						Ext.Msg.alert('Warning', 'Response : ' + response );
					},
					success: function(response, options){
						var json = Ext.util.JSON.decode(response.responseText);
						
						if(json.success){
							Ext.Msg.alert('OK', ' Data Pengguna tersimpan.');
							setTimeout(function(){
								tugasku.GroupTugas.load();
							}, 1000);
							tugasAllWin.hide();
						}else if(json.success == false){
							Ext.Msg.alert('Error', json.message);	
						}
					}
				});
			}		
		},{
			text: 'Batal',
			iconCls: 'logout',
			handler: function(){
				tugasAllWin.hide();
			}
		}]
	});
	
		setTimeout(function(){
			tugasku.GroupTugas.load();
		}, 1000);
		
		this.fireEvent('create', this);
		
		return this.panel;
	}, 
	init: function(){
		this.addEvents({
			'create' : true
		});
	}
});