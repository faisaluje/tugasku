<?php

class TugasAllModule extends XondModule{
	var $component;
	
	function TugasAllModule(){
		parent::__construct();
		
		$this->registerUi('tugasall');
		//$this->setMinifyUi(false);
		$this->setTitle('TugasAll');
	}
	
	function execTugas(){
		$c = new Criteria();
		if($_REQUEST["jenis_tugas_id"] > 0){
			$c->add(TugasPeer::JENIS_TUGAS_ID, $_REQUEST["jenis_tugas_id"]);
		}
		if($_REQUEST["nama"] != ""){
			$c->add(TugasPeer::NAMA, "%".$_REQUEST[nama]."%", Criteria::LIKE);
		}
		$rowCount = TugasPeer::doCount($c);
		$c->setOffset($_REQUEST['start']);
		$c->setLimit($_REQUEST['limit']);
		$tugas = TugasPeer::doSelectJoinAll($c);
		foreach ($tugas as $tugasArr){
			$arr = $tugasArr->toArray();
			$arr["JenisTugas"] = $tugasArr->getJenisTugas()->getNama();
			$arr["MataPelajaran"] = $tugasArr->getMataPelajaran()->getNama();
			$arr["Siswa"] = $tugasArr->getSiswa()->getNama();
			$arr["Pengguna"] = $tugasArr->getPengguna()->getNama();
			$outArr[] = $arr;
		}
		$fieldName = TugasPeer::getFieldNames(BasePeer::TYPE_PHPNAME);
		$this->write(tableJson($outArr, $rowCount, $fieldName));
	}

	function execSave(){
		try{
			if($_POST['tid']){
				$c = new Criteria();
				$c->add(TugasPeer::TUGAS_ID, $_POST['tid']);
				$tugas = TugasPeer::doSelect($c);
				foreach ($tugas as $tugasArr){
					$file = $tugasArr->getFile();
				}				
				$n = TugasPeer::retrieveByPK($_POST['tid']);
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
				$n->setFile($file);
			}else{
				$n = new Tugas();
				$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
				$n->setFile($n->getNis() . '_' . $n->getNama() . '.zip');
			}			
			
			if($n->save()){
				$this->write("{ success : true }");
			}else{
				$this->write("{ success : false }");
			}
		}catch(Exception $e){
			$this->write(sprintf("{ success : false, message : '.$e.getMessage().' }"));
		}
	}
	
	function execUpload(){
		$uploadDir = 'files/';
		$theFileName = '';
					
		if(is_uploaded_file($_FILES['file']['tmp_name'])){
			$uploadFile = $uploadDir . basename($_FILES['file']['name']);
			$n = new Tugas();
			$n->fromArray($_REQUEST, BasePeer::TYPE_FIELDNAME);
			$siswa = str_replace(" ", "_", $n->getSiswa()->getNama());
			$nmFile = str_replace(" ", "_", $n->getNama());
			$uploadFile = $uploadDir . $siswa . '_' . $nmFile . '.zip';
			if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFile)){
				$success = 'true';
			}else{
				$success = 'false';
			}
		}else{
			$success = 'false';
		}
		$result = sprintf("{ success : %s, message : '%s' }", $success, $success);
		$this->write($result);
	}
	
	function execDelete(){
		$tugas = TugasPeer::retrieveByPK($_REQUEST['id']);
		try{
			$tugas->delete();
			$this->write("{ 'success' : true }");			
		}catch(Exception $e){
			$this->write("{ 'success' : false }");
		}
	}	
}

?>