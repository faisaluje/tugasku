
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

#-----------------------------------------------------------------------------
#-- pengguna
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `pengguna`;


CREATE TABLE `pengguna`
(
	`pengguna_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(255)  NOT NULL,
	`password` VARCHAR(255)  NOT NULL,
	`nama` VARCHAR(255)  NOT NULL,
	`telepon` VARCHAR(15)  NOT NULL,
	`foto` VARCHAR(255)  NOT NULL,
	`jabatan_id` INTEGER  NOT NULL,
	PRIMARY KEY (`pengguna_id`),
	INDEX `pengguna_FI_1` (`jabatan_id`),
	CONSTRAINT `pengguna_FK_1`
		FOREIGN KEY (`jabatan_id`)
		REFERENCES `jabatan` (`jabatan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- jabatan
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `jabatan`;


CREATE TABLE `jabatan`
(
	`jabatan_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(10)  NOT NULL,
	`deskripsi` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`jabatan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- coba
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `coba`;


CREATE TABLE `coba`
(
	`coba_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(10)  NOT NULL,
	`deskripsi` VARCHAR(255)  NOT NULL,
	`jabatan_id` INTEGER  NOT NULL,
	PRIMARY KEY (`coba_id`),
	INDEX `coba_FI_1` (`jabatan_id`),
	CONSTRAINT `coba_FK_1`
		FOREIGN KEY (`jabatan_id`)
		REFERENCES `jabatan` (`jabatan_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- kompetensi_keahlian
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `kompetensi_keahlian`;


CREATE TABLE `kompetensi_keahlian`
(
	`kompetensi_keahlian_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(10)  NOT NULL,
	`deskripsi` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`kompetensi_keahlian_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- jenis_tugas
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_tugas`;


CREATE TABLE `jenis_tugas`
(
	`jenis_tugas_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`deskripsi` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`jenis_tugas_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- mata_pelajaran
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `mata_pelajaran`;


CREATE TABLE `mata_pelajaran`
(
	`mata_pelajaran_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nama` VARCHAR(50)  NOT NULL,
	`kompetensi_keahlian_id` INTEGER  NOT NULL,
	PRIMARY KEY (`mata_pelajaran_id`),
	INDEX `mata_pelajaran_FI_1` (`kompetensi_keahlian_id`),
	CONSTRAINT `mata_pelajaran_FK_1`
		FOREIGN KEY (`kompetensi_keahlian_id`)
		REFERENCES `kompetensi_keahlian` (`kompetensi_keahlian_id`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- siswa
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `siswa`;


CREATE TABLE `siswa`
(
	`siswa_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`nis` VARCHAR(20)  NOT NULL,
	`nama` VARCHAR(255)  NOT NULL,
	`jenis_kelamin` VARCHAR(50)  NOT NULL,
	`tempat_lahir` VARCHAR(50)  NOT NULL,
	`tanggal_lahir` VARCHAR(50)  NOT NULL,
	`telepon` VARCHAR(20)  NOT NULL,
	`kelas` VARCHAR(20)  NOT NULL,
	`foto` VARCHAR(255)  NOT NULL,
	PRIMARY KEY (`siswa_id`),
	INDEX `I_referenced_tugas_FK_3_1` (`nis`)
) ENGINE=InnoDB;

#-----------------------------------------------------------------------------
#-- tugas
#-----------------------------------------------------------------------------

DROP TABLE IF EXISTS `tugas`;


CREATE TABLE `tugas`
(
	`tugas_id` INTEGER  NOT NULL AUTO_INCREMENT,
	`jenis_tugas_id` INTEGER  NOT NULL,
	`mata_pelajaran_id` INTEGER  NOT NULL,
	`nis` VARCHAR(20)  NOT NULL,
	`tanggal_pengumpulan` VARCHAR(50)  NOT NULL,
	`nama` VARCHAR(255)  NOT NULL,
	`file` VARCHAR(255)  NOT NULL,
	`abstrak` VARCHAR(255)  NOT NULL,
	`pengguna_id` INTEGER  NOT NULL,
	PRIMARY KEY (`tugas_id`),
	INDEX `tugas_FI_1` (`jenis_tugas_id`),
	CONSTRAINT `tugas_FK_1`
		FOREIGN KEY (`jenis_tugas_id`)
		REFERENCES `jenis_tugas` (`jenis_tugas_id`),
	INDEX `tugas_FI_2` (`mata_pelajaran_id`),
	CONSTRAINT `tugas_FK_2`
		FOREIGN KEY (`mata_pelajaran_id`)
		REFERENCES `mata_pelajaran` (`mata_pelajaran_id`),
	INDEX `tugas_FI_3` (`nis`),
	CONSTRAINT `tugas_FK_3`
		FOREIGN KEY (`nis`)
		REFERENCES `siswa` (`nis`),
	INDEX `tugas_FI_4` (`pengguna_id`),
	CONSTRAINT `tugas_FK_4`
		FOREIGN KEY (`pengguna_id`)
		REFERENCES `pengguna` (`pengguna_id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
