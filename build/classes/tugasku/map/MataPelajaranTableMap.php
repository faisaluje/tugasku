<?php



/**
 * This class defines the structure of the 'mata_pelajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.tugasku.map
 */
class MataPelajaranTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'tugasku.map.MataPelajaranTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('mata_pelajaran');
		$this->setPhpName('MataPelajaran');
		$this->setClassname('MataPelajaran');
		$this->setPackage('tugasku');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('MATA_PELAJARAN_ID', 'MataPelajaranId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 50, null);
		$this->addForeignKey('KOMPETENSI_KEAHLIAN_ID', 'KompetensiKeahlianId', 'INTEGER', 'kompetensi_keahlian', 'KOMPETENSI_KEAHLIAN_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('KompetensiKeahlian', 'KompetensiKeahlian', RelationMap::MANY_TO_ONE, array('kompetensi_keahlian_id' => 'kompetensi_keahlian_id', ), null, null);
    $this->addRelation('Tugas', 'Tugas', RelationMap::ONE_TO_MANY, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
	} // buildRelations()

} // MataPelajaranTableMap
