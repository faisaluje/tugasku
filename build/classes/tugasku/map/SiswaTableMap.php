<?php



/**
 * This class defines the structure of the 'siswa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.tugasku.map
 */
class SiswaTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'tugasku.map.SiswaTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('siswa');
		$this->setPhpName('Siswa');
		$this->setClassname('Siswa');
		$this->setPackage('tugasku');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('SISWA_ID', 'SiswaId', 'INTEGER', true, null, null);
		$this->addColumn('NIS', 'Nis', 'VARCHAR', true, 20, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 255, null);
		$this->addColumn('JENIS_KELAMIN', 'JenisKelamin', 'VARCHAR', true, 50, null);
		$this->addColumn('TEMPAT_LAHIR', 'TempatLahir', 'VARCHAR', true, 50, null);
		$this->addColumn('TANGGAL_LAHIR', 'TanggalLahir', 'VARCHAR', true, 50, null);
		$this->addColumn('TELEPON', 'Telepon', 'VARCHAR', true, 20, null);
		$this->addColumn('KELAS', 'Kelas', 'VARCHAR', true, 20, null);
		$this->addColumn('FOTO', 'Foto', 'VARCHAR', true, 255, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Tugas', 'Tugas', RelationMap::ONE_TO_MANY, array('nis' => 'nis', ), null, null);
	} // buildRelations()

} // SiswaTableMap
