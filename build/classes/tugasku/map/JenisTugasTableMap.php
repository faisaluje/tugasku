<?php



/**
 * This class defines the structure of the 'jenis_tugas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.tugasku.map
 */
class JenisTugasTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'tugasku.map.JenisTugasTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('jenis_tugas');
		$this->setPhpName('JenisTugas');
		$this->setClassname('JenisTugas');
		$this->setPackage('tugasku');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('JENIS_TUGAS_ID', 'JenisTugasId', 'INTEGER', true, null, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 50, null);
		$this->addColumn('DESKRIPSI', 'Deskripsi', 'VARCHAR', true, 255, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('Tugas', 'Tugas', RelationMap::ONE_TO_MANY, array('jenis_tugas_id' => 'jenis_tugas_id', ), null, null);
	} // buildRelations()

} // JenisTugasTableMap
