<?php



/**
 * This class defines the structure of the 'tugas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.tugasku.map
 */
class TugasTableMap extends TableMap {

	/**
	 * The (dot-path) name of this class
	 */
	const CLASS_NAME = 'tugasku.map.TugasTableMap';

	/**
	 * Initialize the table attributes, columns and validators
	 * Relations are not initialized by this method since they are lazy loaded
	 *
	 * @return     void
	 * @throws     PropelException
	 */
	public function initialize()
	{
	  // attributes
		$this->setName('tugas');
		$this->setPhpName('Tugas');
		$this->setClassname('Tugas');
		$this->setPackage('tugasku');
		$this->setUseIdGenerator(true);
		// columns
		$this->addPrimaryKey('TUGAS_ID', 'TugasId', 'INTEGER', true, null, null);
		$this->addForeignKey('JENIS_TUGAS_ID', 'JenisTugasId', 'INTEGER', 'jenis_tugas', 'JENIS_TUGAS_ID', true, null, null);
		$this->addForeignKey('MATA_PELAJARAN_ID', 'MataPelajaranId', 'INTEGER', 'mata_pelajaran', 'MATA_PELAJARAN_ID', true, null, null);
		$this->addForeignKey('NIS', 'Nis', 'VARCHAR', 'siswa', 'NIS', true, 20, null);
		$this->addColumn('TANGGAL_PENGUMPULAN', 'TanggalPengumpulan', 'VARCHAR', true, 50, null);
		$this->addColumn('NAMA', 'Nama', 'VARCHAR', true, 255, null);
		$this->addColumn('FILE', 'File', 'VARCHAR', true, 255, null);
		$this->addColumn('ABSTRAK', 'Abstrak', 'VARCHAR', true, 255, null);
		$this->addForeignKey('PENGGUNA_ID', 'PenggunaId', 'INTEGER', 'pengguna', 'PENGGUNA_ID', true, null, null);
		// validators
	} // initialize()

	/**
	 * Build the RelationMap objects for this table relationships
	 */
	public function buildRelations()
	{
    $this->addRelation('JenisTugas', 'JenisTugas', RelationMap::MANY_TO_ONE, array('jenis_tugas_id' => 'jenis_tugas_id', ), null, null);
    $this->addRelation('MataPelajaran', 'MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
    $this->addRelation('Siswa', 'Siswa', RelationMap::MANY_TO_ONE, array('nis' => 'nis', ), null, null);
    $this->addRelation('Pengguna', 'Pengguna', RelationMap::MANY_TO_ONE, array('pengguna_id' => 'pengguna_id', ), null, null);
	} // buildRelations()

} // TugasTableMap
