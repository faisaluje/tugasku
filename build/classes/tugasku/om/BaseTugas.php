<?php


/**
 * Base class that represents a row from the 'tugas' table.
 *
 * 
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseTugas extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'TugasPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        TugasPeer
	 */
	protected static $peer;

	/**
	 * The value for the tugas_id field.
	 * @var        int
	 */
	protected $tugas_id;

	/**
	 * The value for the jenis_tugas_id field.
	 * @var        int
	 */
	protected $jenis_tugas_id;

	/**
	 * The value for the mata_pelajaran_id field.
	 * @var        int
	 */
	protected $mata_pelajaran_id;

	/**
	 * The value for the nis field.
	 * @var        string
	 */
	protected $nis;

	/**
	 * The value for the tanggal_pengumpulan field.
	 * @var        string
	 */
	protected $tanggal_pengumpulan;

	/**
	 * The value for the nama field.
	 * @var        string
	 */
	protected $nama;

	/**
	 * The value for the file field.
	 * @var        string
	 */
	protected $file;

	/**
	 * The value for the abstrak field.
	 * @var        string
	 */
	protected $abstrak;

	/**
	 * The value for the pengguna_id field.
	 * @var        int
	 */
	protected $pengguna_id;

	/**
	 * @var        JenisTugas
	 */
	protected $aJenisTugas;

	/**
	 * @var        MataPelajaran
	 */
	protected $aMataPelajaran;

	/**
	 * @var        Siswa
	 */
	protected $aSiswa;

	/**
	 * @var        Pengguna
	 */
	protected $aPengguna;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [tugas_id] column value.
	 * 
	 * @return     int
	 */
	public function getTugasId()
	{
		return $this->tugas_id;
	}

	/**
	 * Get the [jenis_tugas_id] column value.
	 * 
	 * @return     int
	 */
	public function getJenisTugasId()
	{
		return $this->jenis_tugas_id;
	}

	/**
	 * Get the [mata_pelajaran_id] column value.
	 * 
	 * @return     int
	 */
	public function getMataPelajaranId()
	{
		return $this->mata_pelajaran_id;
	}

	/**
	 * Get the [nis] column value.
	 * 
	 * @return     string
	 */
	public function getNis()
	{
		return $this->nis;
	}

	/**
	 * Get the [tanggal_pengumpulan] column value.
	 * 
	 * @return     string
	 */
	public function getTanggalPengumpulan()
	{
		return $this->tanggal_pengumpulan;
	}

	/**
	 * Get the [nama] column value.
	 * 
	 * @return     string
	 */
	public function getNama()
	{
		return $this->nama;
	}

	/**
	 * Get the [file] column value.
	 * 
	 * @return     string
	 */
	public function getFile()
	{
		return $this->file;
	}

	/**
	 * Get the [abstrak] column value.
	 * 
	 * @return     string
	 */
	public function getAbstrak()
	{
		return $this->abstrak;
	}

	/**
	 * Get the [pengguna_id] column value.
	 * 
	 * @return     int
	 */
	public function getPenggunaId()
	{
		return $this->pengguna_id;
	}

	/**
	 * Set the value of [tugas_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setTugasId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->tugas_id !== $v) {
			$this->tugas_id = $v;
			$this->modifiedColumns[] = TugasPeer::TUGAS_ID;
		}

		return $this;
	} // setTugasId()

	/**
	 * Set the value of [jenis_tugas_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setJenisTugasId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->jenis_tugas_id !== $v) {
			$this->jenis_tugas_id = $v;
			$this->modifiedColumns[] = TugasPeer::JENIS_TUGAS_ID;
		}

		if ($this->aJenisTugas !== null && $this->aJenisTugas->getJenisTugasId() !== $v) {
			$this->aJenisTugas = null;
		}

		return $this;
	} // setJenisTugasId()

	/**
	 * Set the value of [mata_pelajaran_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setMataPelajaranId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->mata_pelajaran_id !== $v) {
			$this->mata_pelajaran_id = $v;
			$this->modifiedColumns[] = TugasPeer::MATA_PELAJARAN_ID;
		}

		if ($this->aMataPelajaran !== null && $this->aMataPelajaran->getMataPelajaranId() !== $v) {
			$this->aMataPelajaran = null;
		}

		return $this;
	} // setMataPelajaranId()

	/**
	 * Set the value of [nis] column.
	 * 
	 * @param      string $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setNis($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nis !== $v) {
			$this->nis = $v;
			$this->modifiedColumns[] = TugasPeer::NIS;
		}

		if ($this->aSiswa !== null && $this->aSiswa->getNis() !== $v) {
			$this->aSiswa = null;
		}

		return $this;
	} // setNis()

	/**
	 * Set the value of [tanggal_pengumpulan] column.
	 * 
	 * @param      string $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setTanggalPengumpulan($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tanggal_pengumpulan !== $v) {
			$this->tanggal_pengumpulan = $v;
			$this->modifiedColumns[] = TugasPeer::TANGGAL_PENGUMPULAN;
		}

		return $this;
	} // setTanggalPengumpulan()

	/**
	 * Set the value of [nama] column.
	 * 
	 * @param      string $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setNama($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = TugasPeer::NAMA;
		}

		return $this;
	} // setNama()

	/**
	 * Set the value of [file] column.
	 * 
	 * @param      string $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setFile($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->file !== $v) {
			$this->file = $v;
			$this->modifiedColumns[] = TugasPeer::FILE;
		}

		return $this;
	} // setFile()

	/**
	 * Set the value of [abstrak] column.
	 * 
	 * @param      string $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setAbstrak($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->abstrak !== $v) {
			$this->abstrak = $v;
			$this->modifiedColumns[] = TugasPeer::ABSTRAK;
		}

		return $this;
	} // setAbstrak()

	/**
	 * Set the value of [pengguna_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Tugas The current object (for fluent API support)
	 */
	public function setPenggunaId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->pengguna_id !== $v) {
			$this->pengguna_id = $v;
			$this->modifiedColumns[] = TugasPeer::PENGGUNA_ID;
		}

		if ($this->aPengguna !== null && $this->aPengguna->getPenggunaId() !== $v) {
			$this->aPengguna = null;
		}

		return $this;
	} // setPenggunaId()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->tugas_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->jenis_tugas_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
			$this->mata_pelajaran_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
			$this->nis = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->tanggal_pengumpulan = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->nama = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->file = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->abstrak = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->pengguna_id = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 9; // 9 = TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Tugas object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aJenisTugas !== null && $this->jenis_tugas_id !== $this->aJenisTugas->getJenisTugasId()) {
			$this->aJenisTugas = null;
		}
		if ($this->aMataPelajaran !== null && $this->mata_pelajaran_id !== $this->aMataPelajaran->getMataPelajaranId()) {
			$this->aMataPelajaran = null;
		}
		if ($this->aSiswa !== null && $this->nis !== $this->aSiswa->getNis()) {
			$this->aSiswa = null;
		}
		if ($this->aPengguna !== null && $this->pengguna_id !== $this->aPengguna->getPenggunaId()) {
			$this->aPengguna = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = TugasPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aJenisTugas = null;
			$this->aMataPelajaran = null;
			$this->aSiswa = null;
			$this->aPengguna = null;
		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				TugasQuery::create()
					->filterByPrimaryKey($this->getPrimaryKey())
					->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				TugasPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aJenisTugas !== null) {
				if ($this->aJenisTugas->isModified() || $this->aJenisTugas->isNew()) {
					$affectedRows += $this->aJenisTugas->save($con);
				}
				$this->setJenisTugas($this->aJenisTugas);
			}

			if ($this->aMataPelajaran !== null) {
				if ($this->aMataPelajaran->isModified() || $this->aMataPelajaran->isNew()) {
					$affectedRows += $this->aMataPelajaran->save($con);
				}
				$this->setMataPelajaran($this->aMataPelajaran);
			}

			if ($this->aSiswa !== null) {
				if ($this->aSiswa->isModified() || $this->aSiswa->isNew()) {
					$affectedRows += $this->aSiswa->save($con);
				}
				$this->setSiswa($this->aSiswa);
			}

			if ($this->aPengguna !== null) {
				if ($this->aPengguna->isModified() || $this->aPengguna->isNew()) {
					$affectedRows += $this->aPengguna->save($con);
				}
				$this->setPengguna($this->aPengguna);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = TugasPeer::TUGAS_ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$criteria = $this->buildCriteria();
					if ($criteria->keyContainsValue(TugasPeer::TUGAS_ID) ) {
						throw new PropelException('Cannot insert a value for auto-increment primary key ('.TugasPeer::TUGAS_ID.')');
					}

					$pk = BasePeer::doInsert($criteria, $con);
					$affectedRows += 1;
					$this->setTugasId($pk);  //[IMV] update autoincrement primary key
					$this->setNew(false);
				} else {
					$affectedRows += TugasPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aJenisTugas !== null) {
				if (!$this->aJenisTugas->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aJenisTugas->getValidationFailures());
				}
			}

			if ($this->aMataPelajaran !== null) {
				if (!$this->aMataPelajaran->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aMataPelajaran->getValidationFailures());
				}
			}

			if ($this->aSiswa !== null) {
				if (!$this->aSiswa->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aSiswa->getValidationFailures());
				}
			}

			if ($this->aPengguna !== null) {
				if (!$this->aPengguna->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aPengguna->getValidationFailures());
				}
			}


			if (($retval = TugasPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}



			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TugasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getTugasId();
				break;
			case 1:
				return $this->getJenisTugasId();
				break;
			case 2:
				return $this->getMataPelajaranId();
				break;
			case 3:
				return $this->getNis();
				break;
			case 4:
				return $this->getTanggalPengumpulan();
				break;
			case 5:
				return $this->getNama();
				break;
			case 6:
				return $this->getFile();
				break;
			case 7:
				return $this->getAbstrak();
				break;
			case 8:
				return $this->getPenggunaId();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $includeForeignObjects = false)
	{
		$keys = TugasPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getTugasId(),
			$keys[1] => $this->getJenisTugasId(),
			$keys[2] => $this->getMataPelajaranId(),
			$keys[3] => $this->getNis(),
			$keys[4] => $this->getTanggalPengumpulan(),
			$keys[5] => $this->getNama(),
			$keys[6] => $this->getFile(),
			$keys[7] => $this->getAbstrak(),
			$keys[8] => $this->getPenggunaId(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aJenisTugas) {
				$result['JenisTugas'] = $this->aJenisTugas->toArray($keyType, $includeLazyLoadColumns, true);
			}
			if (null !== $this->aMataPelajaran) {
				$result['MataPelajaran'] = $this->aMataPelajaran->toArray($keyType, $includeLazyLoadColumns, true);
			}
			if (null !== $this->aSiswa) {
				$result['Siswa'] = $this->aSiswa->toArray($keyType, $includeLazyLoadColumns, true);
			}
			if (null !== $this->aPengguna) {
				$result['Pengguna'] = $this->aPengguna->toArray($keyType, $includeLazyLoadColumns, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = TugasPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setTugasId($value);
				break;
			case 1:
				$this->setJenisTugasId($value);
				break;
			case 2:
				$this->setMataPelajaranId($value);
				break;
			case 3:
				$this->setNis($value);
				break;
			case 4:
				$this->setTanggalPengumpulan($value);
				break;
			case 5:
				$this->setNama($value);
				break;
			case 6:
				$this->setFile($value);
				break;
			case 7:
				$this->setAbstrak($value);
				break;
			case 8:
				$this->setPenggunaId($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = TugasPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setTugasId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setJenisTugasId($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setMataPelajaranId($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNis($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTanggalPengumpulan($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setNama($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setFile($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setAbstrak($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setPenggunaId($arr[$keys[8]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(TugasPeer::DATABASE_NAME);

		if ($this->isColumnModified(TugasPeer::TUGAS_ID)) $criteria->add(TugasPeer::TUGAS_ID, $this->tugas_id);
		if ($this->isColumnModified(TugasPeer::JENIS_TUGAS_ID)) $criteria->add(TugasPeer::JENIS_TUGAS_ID, $this->jenis_tugas_id);
		if ($this->isColumnModified(TugasPeer::MATA_PELAJARAN_ID)) $criteria->add(TugasPeer::MATA_PELAJARAN_ID, $this->mata_pelajaran_id);
		if ($this->isColumnModified(TugasPeer::NIS)) $criteria->add(TugasPeer::NIS, $this->nis);
		if ($this->isColumnModified(TugasPeer::TANGGAL_PENGUMPULAN)) $criteria->add(TugasPeer::TANGGAL_PENGUMPULAN, $this->tanggal_pengumpulan);
		if ($this->isColumnModified(TugasPeer::NAMA)) $criteria->add(TugasPeer::NAMA, $this->nama);
		if ($this->isColumnModified(TugasPeer::FILE)) $criteria->add(TugasPeer::FILE, $this->file);
		if ($this->isColumnModified(TugasPeer::ABSTRAK)) $criteria->add(TugasPeer::ABSTRAK, $this->abstrak);
		if ($this->isColumnModified(TugasPeer::PENGGUNA_ID)) $criteria->add(TugasPeer::PENGGUNA_ID, $this->pengguna_id);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(TugasPeer::DATABASE_NAME);
		$criteria->add(TugasPeer::TUGAS_ID, $this->tugas_id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getTugasId();
	}

	/**
	 * Generic method to set the primary key (tugas_id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setTugasId($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getTugasId();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Tugas (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{
		$copyObj->setJenisTugasId($this->jenis_tugas_id);
		$copyObj->setMataPelajaranId($this->mata_pelajaran_id);
		$copyObj->setNis($this->nis);
		$copyObj->setTanggalPengumpulan($this->tanggal_pengumpulan);
		$copyObj->setNama($this->nama);
		$copyObj->setFile($this->file);
		$copyObj->setAbstrak($this->abstrak);
		$copyObj->setPenggunaId($this->pengguna_id);

		$copyObj->setNew(true);
		$copyObj->setTugasId(NULL); // this is a auto-increment column, so set to default value
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Tugas Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     TugasPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new TugasPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a JenisTugas object.
	 *
	 * @param      JenisTugas $v
	 * @return     Tugas The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setJenisTugas(JenisTugas $v = null)
	{
		if ($v === null) {
			$this->setJenisTugasId(NULL);
		} else {
			$this->setJenisTugasId($v->getJenisTugasId());
		}

		$this->aJenisTugas = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the JenisTugas object, it will not be re-added.
		if ($v !== null) {
			$v->addTugas($this);
		}

		return $this;
	}


	/**
	 * Get the associated JenisTugas object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     JenisTugas The associated JenisTugas object.
	 * @throws     PropelException
	 */
	public function getJenisTugas(PropelPDO $con = null)
	{
		if ($this->aJenisTugas === null && ($this->jenis_tugas_id !== null)) {
			$this->aJenisTugas = JenisTugasQuery::create()->findPk($this->jenis_tugas_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aJenisTugas->addTugass($this);
			 */
		}
		return $this->aJenisTugas;
	}

	/**
	 * Declares an association between this object and a MataPelajaran object.
	 *
	 * @param      MataPelajaran $v
	 * @return     Tugas The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setMataPelajaran(MataPelajaran $v = null)
	{
		if ($v === null) {
			$this->setMataPelajaranId(NULL);
		} else {
			$this->setMataPelajaranId($v->getMataPelajaranId());
		}

		$this->aMataPelajaran = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the MataPelajaran object, it will not be re-added.
		if ($v !== null) {
			$v->addTugas($this);
		}

		return $this;
	}


	/**
	 * Get the associated MataPelajaran object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     MataPelajaran The associated MataPelajaran object.
	 * @throws     PropelException
	 */
	public function getMataPelajaran(PropelPDO $con = null)
	{
		if ($this->aMataPelajaran === null && ($this->mata_pelajaran_id !== null)) {
			$this->aMataPelajaran = MataPelajaranQuery::create()->findPk($this->mata_pelajaran_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aMataPelajaran->addTugass($this);
			 */
		}
		return $this->aMataPelajaran;
	}

	/**
	 * Declares an association between this object and a Siswa object.
	 *
	 * @param      Siswa $v
	 * @return     Tugas The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setSiswa(Siswa $v = null)
	{
		if ($v === null) {
			$this->setNis(NULL);
		} else {
			$this->setNis($v->getNis());
		}

		$this->aSiswa = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Siswa object, it will not be re-added.
		if ($v !== null) {
			$v->addTugas($this);
		}

		return $this;
	}


	/**
	 * Get the associated Siswa object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Siswa The associated Siswa object.
	 * @throws     PropelException
	 */
	public function getSiswa(PropelPDO $con = null)
	{
		if ($this->aSiswa === null && (($this->nis !== "" && $this->nis !== null))) {
			$this->aSiswa = SiswaQuery::create()
				->filterByTugas($this) // here
				->findOne($con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aSiswa->addTugass($this);
			 */
		}
		return $this->aSiswa;
	}

	/**
	 * Declares an association between this object and a Pengguna object.
	 *
	 * @param      Pengguna $v
	 * @return     Tugas The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setPengguna(Pengguna $v = null)
	{
		if ($v === null) {
			$this->setPenggunaId(NULL);
		} else {
			$this->setPenggunaId($v->getPenggunaId());
		}

		$this->aPengguna = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Pengguna object, it will not be re-added.
		if ($v !== null) {
			$v->addTugas($this);
		}

		return $this;
	}


	/**
	 * Get the associated Pengguna object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Pengguna The associated Pengguna object.
	 * @throws     PropelException
	 */
	public function getPengguna(PropelPDO $con = null)
	{
		if ($this->aPengguna === null && ($this->pengguna_id !== null)) {
			$this->aPengguna = PenggunaQuery::create()->findPk($this->pengguna_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aPengguna->addTugass($this);
			 */
		}
		return $this->aPengguna;
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->tugas_id = null;
		$this->jenis_tugas_id = null;
		$this->mata_pelajaran_id = null;
		$this->nis = null;
		$this->tanggal_pengumpulan = null;
		$this->nama = null;
		$this->file = null;
		$this->abstrak = null;
		$this->pengguna_id = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
		} // if ($deep)

		$this->aJenisTugas = null;
		$this->aMataPelajaran = null;
		$this->aSiswa = null;
		$this->aPengguna = null;
	}

	/**
	 * Catches calls to virtual methods
	 */
	public function __call($name, $params)
	{
		if (preg_match('/get(\w+)/', $name, $matches)) {
			$virtualColumn = $matches[1];
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
			// no lcfirst in php<5.3...
			$virtualColumn[0] = strtolower($virtualColumn[0]);
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
		}
		return parent::__call($name, $params);
	}

} // BaseTugas
