<?php


/**
 * Base class that represents a query for the 'siswa' table.
 *
 * 
 *
 * @method     SiswaQuery orderBySiswaId($order = Criteria::ASC) Order by the siswa_id column
 * @method     SiswaQuery orderByNis($order = Criteria::ASC) Order by the nis column
 * @method     SiswaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     SiswaQuery orderByJenisKelamin($order = Criteria::ASC) Order by the jenis_kelamin column
 * @method     SiswaQuery orderByTempatLahir($order = Criteria::ASC) Order by the tempat_lahir column
 * @method     SiswaQuery orderByTanggalLahir($order = Criteria::ASC) Order by the tanggal_lahir column
 * @method     SiswaQuery orderByTelepon($order = Criteria::ASC) Order by the telepon column
 * @method     SiswaQuery orderByKelas($order = Criteria::ASC) Order by the kelas column
 * @method     SiswaQuery orderByFoto($order = Criteria::ASC) Order by the foto column
 *
 * @method     SiswaQuery groupBySiswaId() Group by the siswa_id column
 * @method     SiswaQuery groupByNis() Group by the nis column
 * @method     SiswaQuery groupByNama() Group by the nama column
 * @method     SiswaQuery groupByJenisKelamin() Group by the jenis_kelamin column
 * @method     SiswaQuery groupByTempatLahir() Group by the tempat_lahir column
 * @method     SiswaQuery groupByTanggalLahir() Group by the tanggal_lahir column
 * @method     SiswaQuery groupByTelepon() Group by the telepon column
 * @method     SiswaQuery groupByKelas() Group by the kelas column
 * @method     SiswaQuery groupByFoto() Group by the foto column
 *
 * @method     SiswaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     SiswaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     SiswaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     SiswaQuery leftJoinTugas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tugas relation
 * @method     SiswaQuery rightJoinTugas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tugas relation
 * @method     SiswaQuery innerJoinTugas($relationAlias = null) Adds a INNER JOIN clause to the query using the Tugas relation
 *
 * @method     Siswa findOne(PropelPDO $con = null) Return the first Siswa matching the query
 * @method     Siswa findOneOrCreate(PropelPDO $con = null) Return the first Siswa matching the query, or a new Siswa object populated from the query conditions when no match is found
 *
 * @method     Siswa findOneBySiswaId(int $siswa_id) Return the first Siswa filtered by the siswa_id column
 * @method     Siswa findOneByNis(string $nis) Return the first Siswa filtered by the nis column
 * @method     Siswa findOneByNama(string $nama) Return the first Siswa filtered by the nama column
 * @method     Siswa findOneByJenisKelamin(string $jenis_kelamin) Return the first Siswa filtered by the jenis_kelamin column
 * @method     Siswa findOneByTempatLahir(string $tempat_lahir) Return the first Siswa filtered by the tempat_lahir column
 * @method     Siswa findOneByTanggalLahir(string $tanggal_lahir) Return the first Siswa filtered by the tanggal_lahir column
 * @method     Siswa findOneByTelepon(string $telepon) Return the first Siswa filtered by the telepon column
 * @method     Siswa findOneByKelas(string $kelas) Return the first Siswa filtered by the kelas column
 * @method     Siswa findOneByFoto(string $foto) Return the first Siswa filtered by the foto column
 *
 * @method     array findBySiswaId(int $siswa_id) Return Siswa objects filtered by the siswa_id column
 * @method     array findByNis(string $nis) Return Siswa objects filtered by the nis column
 * @method     array findByNama(string $nama) Return Siswa objects filtered by the nama column
 * @method     array findByJenisKelamin(string $jenis_kelamin) Return Siswa objects filtered by the jenis_kelamin column
 * @method     array findByTempatLahir(string $tempat_lahir) Return Siswa objects filtered by the tempat_lahir column
 * @method     array findByTanggalLahir(string $tanggal_lahir) Return Siswa objects filtered by the tanggal_lahir column
 * @method     array findByTelepon(string $telepon) Return Siswa objects filtered by the telepon column
 * @method     array findByKelas(string $kelas) Return Siswa objects filtered by the kelas column
 * @method     array findByFoto(string $foto) Return Siswa objects filtered by the foto column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseSiswaQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseSiswaQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'Siswa', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new SiswaQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    SiswaQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof SiswaQuery) {
			return $criteria;
		}
		$query = new SiswaQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Siswa|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = SiswaPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(SiswaPeer::SISWA_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(SiswaPeer::SISWA_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the siswa_id column
	 * 
	 * @param     int|array $siswaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterBySiswaId($siswaId = null, $comparison = null)
	{
		if (is_array($siswaId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(SiswaPeer::SISWA_ID, $siswaId, $comparison);
	}

	/**
	 * Filter the query on the nis column
	 * 
	 * @param     string $nis The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByNis($nis = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nis)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nis)) {
				$nis = str_replace('*', '%', $nis);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::NIS, $nis, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the jenis_kelamin column
	 * 
	 * @param     string $jenisKelamin The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByJenisKelamin($jenisKelamin = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($jenisKelamin)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $jenisKelamin)) {
				$jenisKelamin = str_replace('*', '%', $jenisKelamin);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::JENIS_KELAMIN, $jenisKelamin, $comparison);
	}

	/**
	 * Filter the query on the tempat_lahir column
	 * 
	 * @param     string $tempatLahir The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByTempatLahir($tempatLahir = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tempatLahir)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tempatLahir)) {
				$tempatLahir = str_replace('*', '%', $tempatLahir);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::TEMPAT_LAHIR, $tempatLahir, $comparison);
	}

	/**
	 * Filter the query on the tanggal_lahir column
	 * 
	 * @param     string $tanggalLahir The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByTanggalLahir($tanggalLahir = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tanggalLahir)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tanggalLahir)) {
				$tanggalLahir = str_replace('*', '%', $tanggalLahir);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::TANGGAL_LAHIR, $tanggalLahir, $comparison);
	}

	/**
	 * Filter the query on the telepon column
	 * 
	 * @param     string $telepon The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByTelepon($telepon = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($telepon)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $telepon)) {
				$telepon = str_replace('*', '%', $telepon);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::TELEPON, $telepon, $comparison);
	}

	/**
	 * Filter the query on the kelas column
	 * 
	 * @param     string $kelas The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByKelas($kelas = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($kelas)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $kelas)) {
				$kelas = str_replace('*', '%', $kelas);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::KELAS, $kelas, $comparison);
	}

	/**
	 * Filter the query on the foto column
	 * 
	 * @param     string $foto The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByFoto($foto = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($foto)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $foto)) {
				$foto = str_replace('*', '%', $foto);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(SiswaPeer::FOTO, $foto, $comparison);
	}

	/**
	 * Filter the query by a related Tugas object
	 *
	 * @param     Tugas $tugas  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function filterByTugas($tugas, $comparison = null)
	{
		return $this
			->addUsingAlias(SiswaPeer::NIS, $tugas->getNis(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Tugas relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function joinTugas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tugas');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tugas');
		}
		
		return $this;
	}

	/**
	 * Use the Tugas relation Tugas object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery A secondary query class using the current class as primary query
	 */
	public function useTugasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTugas($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tugas', 'TugasQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Siswa $siswa Object to remove from the list of results
	 *
	 * @return    SiswaQuery The current query, for fluid interface
	 */
	public function prune($siswa = null)
	{
		if ($siswa) {
			$this->addUsingAlias(SiswaPeer::SISWA_ID, $siswa->getSiswaId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseSiswaQuery
