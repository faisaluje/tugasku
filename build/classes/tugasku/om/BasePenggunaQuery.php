<?php


/**
 * Base class that represents a query for the 'pengguna' table.
 *
 * 
 *
 * @method     PenggunaQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method     PenggunaQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method     PenggunaQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     PenggunaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     PenggunaQuery orderByTelepon($order = Criteria::ASC) Order by the telepon column
 * @method     PenggunaQuery orderByFoto($order = Criteria::ASC) Order by the foto column
 * @method     PenggunaQuery orderByJabatanId($order = Criteria::ASC) Order by the jabatan_id column
 *
 * @method     PenggunaQuery groupByPenggunaId() Group by the pengguna_id column
 * @method     PenggunaQuery groupByUsername() Group by the username column
 * @method     PenggunaQuery groupByPassword() Group by the password column
 * @method     PenggunaQuery groupByNama() Group by the nama column
 * @method     PenggunaQuery groupByTelepon() Group by the telepon column
 * @method     PenggunaQuery groupByFoto() Group by the foto column
 * @method     PenggunaQuery groupByJabatanId() Group by the jabatan_id column
 *
 * @method     PenggunaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     PenggunaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     PenggunaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     PenggunaQuery leftJoinJabatan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jabatan relation
 * @method     PenggunaQuery rightJoinJabatan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jabatan relation
 * @method     PenggunaQuery innerJoinJabatan($relationAlias = null) Adds a INNER JOIN clause to the query using the Jabatan relation
 *
 * @method     PenggunaQuery leftJoinTugas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tugas relation
 * @method     PenggunaQuery rightJoinTugas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tugas relation
 * @method     PenggunaQuery innerJoinTugas($relationAlias = null) Adds a INNER JOIN clause to the query using the Tugas relation
 *
 * @method     Pengguna findOne(PropelPDO $con = null) Return the first Pengguna matching the query
 * @method     Pengguna findOneOrCreate(PropelPDO $con = null) Return the first Pengguna matching the query, or a new Pengguna object populated from the query conditions when no match is found
 *
 * @method     Pengguna findOneByPenggunaId(int $pengguna_id) Return the first Pengguna filtered by the pengguna_id column
 * @method     Pengguna findOneByUsername(string $username) Return the first Pengguna filtered by the username column
 * @method     Pengguna findOneByPassword(string $password) Return the first Pengguna filtered by the password column
 * @method     Pengguna findOneByNama(string $nama) Return the first Pengguna filtered by the nama column
 * @method     Pengguna findOneByTelepon(string $telepon) Return the first Pengguna filtered by the telepon column
 * @method     Pengguna findOneByFoto(string $foto) Return the first Pengguna filtered by the foto column
 * @method     Pengguna findOneByJabatanId(int $jabatan_id) Return the first Pengguna filtered by the jabatan_id column
 *
 * @method     array findByPenggunaId(int $pengguna_id) Return Pengguna objects filtered by the pengguna_id column
 * @method     array findByUsername(string $username) Return Pengguna objects filtered by the username column
 * @method     array findByPassword(string $password) Return Pengguna objects filtered by the password column
 * @method     array findByNama(string $nama) Return Pengguna objects filtered by the nama column
 * @method     array findByTelepon(string $telepon) Return Pengguna objects filtered by the telepon column
 * @method     array findByFoto(string $foto) Return Pengguna objects filtered by the foto column
 * @method     array findByJabatanId(int $jabatan_id) Return Pengguna objects filtered by the jabatan_id column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BasePenggunaQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BasePenggunaQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'Pengguna', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new PenggunaQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    PenggunaQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof PenggunaQuery) {
			return $criteria;
		}
		$query = new PenggunaQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Pengguna|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = PenggunaPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the pengguna_id column
	 * 
	 * @param     int|array $penggunaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByPenggunaId($penggunaId = null, $comparison = null)
	{
		if (is_array($penggunaId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $penggunaId, $comparison);
	}

	/**
	 * Filter the query on the username column
	 * 
	 * @param     string $username The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByUsername($username = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($username)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $username)) {
				$username = str_replace('*', '%', $username);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::USERNAME, $username, $comparison);
	}

	/**
	 * Filter the query on the password column
	 * 
	 * @param     string $password The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByPassword($password = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($password)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $password)) {
				$password = str_replace('*', '%', $password);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::PASSWORD, $password, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the telepon column
	 * 
	 * @param     string $telepon The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByTelepon($telepon = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($telepon)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $telepon)) {
				$telepon = str_replace('*', '%', $telepon);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::TELEPON, $telepon, $comparison);
	}

	/**
	 * Filter the query on the foto column
	 * 
	 * @param     string $foto The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByFoto($foto = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($foto)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $foto)) {
				$foto = str_replace('*', '%', $foto);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::FOTO, $foto, $comparison);
	}

	/**
	 * Filter the query on the jabatan_id column
	 * 
	 * @param     int|array $jabatanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByJabatanId($jabatanId = null, $comparison = null)
	{
		if (is_array($jabatanId)) {
			$useMinMax = false;
			if (isset($jabatanId['min'])) {
				$this->addUsingAlias(PenggunaPeer::JABATAN_ID, $jabatanId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($jabatanId['max'])) {
				$this->addUsingAlias(PenggunaPeer::JABATAN_ID, $jabatanId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(PenggunaPeer::JABATAN_ID, $jabatanId, $comparison);
	}

	/**
	 * Filter the query by a related Jabatan object
	 *
	 * @param     Jabatan $jabatan  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByJabatan($jabatan, $comparison = null)
	{
		return $this
			->addUsingAlias(PenggunaPeer::JABATAN_ID, $jabatan->getJabatanId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Jabatan relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function joinJabatan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Jabatan');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Jabatan');
		}
		
		return $this;
	}

	/**
	 * Use the Jabatan relation Jabatan object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    JabatanQuery A secondary query class using the current class as primary query
	 */
	public function useJabatanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinJabatan($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Jabatan', 'JabatanQuery');
	}

	/**
	 * Filter the query by a related Tugas object
	 *
	 * @param     Tugas $tugas  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function filterByTugas($tugas, $comparison = null)
	{
		return $this
			->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $tugas->getPenggunaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Tugas relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function joinTugas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tugas');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tugas');
		}
		
		return $this;
	}

	/**
	 * Use the Tugas relation Tugas object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery A secondary query class using the current class as primary query
	 */
	public function useTugasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTugas($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tugas', 'TugasQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Pengguna $pengguna Object to remove from the list of results
	 *
	 * @return    PenggunaQuery The current query, for fluid interface
	 */
	public function prune($pengguna = null)
	{
		if ($pengguna) {
			$this->addUsingAlias(PenggunaPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BasePenggunaQuery
