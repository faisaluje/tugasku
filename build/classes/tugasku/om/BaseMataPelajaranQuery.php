<?php


/**
 * Base class that represents a query for the 'mata_pelajaran' table.
 *
 * 
 *
 * @method     MataPelajaranQuery orderByMataPelajaranId($order = Criteria::ASC) Order by the mata_pelajaran_id column
 * @method     MataPelajaranQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     MataPelajaranQuery orderByKompetensiKeahlianId($order = Criteria::ASC) Order by the kompetensi_keahlian_id column
 *
 * @method     MataPelajaranQuery groupByMataPelajaranId() Group by the mata_pelajaran_id column
 * @method     MataPelajaranQuery groupByNama() Group by the nama column
 * @method     MataPelajaranQuery groupByKompetensiKeahlianId() Group by the kompetensi_keahlian_id column
 *
 * @method     MataPelajaranQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     MataPelajaranQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     MataPelajaranQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     MataPelajaranQuery leftJoinKompetensiKeahlian($relationAlias = null) Adds a LEFT JOIN clause to the query using the KompetensiKeahlian relation
 * @method     MataPelajaranQuery rightJoinKompetensiKeahlian($relationAlias = null) Adds a RIGHT JOIN clause to the query using the KompetensiKeahlian relation
 * @method     MataPelajaranQuery innerJoinKompetensiKeahlian($relationAlias = null) Adds a INNER JOIN clause to the query using the KompetensiKeahlian relation
 *
 * @method     MataPelajaranQuery leftJoinTugas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tugas relation
 * @method     MataPelajaranQuery rightJoinTugas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tugas relation
 * @method     MataPelajaranQuery innerJoinTugas($relationAlias = null) Adds a INNER JOIN clause to the query using the Tugas relation
 *
 * @method     MataPelajaran findOne(PropelPDO $con = null) Return the first MataPelajaran matching the query
 * @method     MataPelajaran findOneOrCreate(PropelPDO $con = null) Return the first MataPelajaran matching the query, or a new MataPelajaran object populated from the query conditions when no match is found
 *
 * @method     MataPelajaran findOneByMataPelajaranId(int $mata_pelajaran_id) Return the first MataPelajaran filtered by the mata_pelajaran_id column
 * @method     MataPelajaran findOneByNama(string $nama) Return the first MataPelajaran filtered by the nama column
 * @method     MataPelajaran findOneByKompetensiKeahlianId(int $kompetensi_keahlian_id) Return the first MataPelajaran filtered by the kompetensi_keahlian_id column
 *
 * @method     array findByMataPelajaranId(int $mata_pelajaran_id) Return MataPelajaran objects filtered by the mata_pelajaran_id column
 * @method     array findByNama(string $nama) Return MataPelajaran objects filtered by the nama column
 * @method     array findByKompetensiKeahlianId(int $kompetensi_keahlian_id) Return MataPelajaran objects filtered by the kompetensi_keahlian_id column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseMataPelajaranQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseMataPelajaranQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'MataPelajaran', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new MataPelajaranQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    MataPelajaranQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof MataPelajaranQuery) {
			return $criteria;
		}
		$query = new MataPelajaranQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    MataPelajaran|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = MataPelajaranPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(MataPelajaranPeer::MATA_PELAJARAN_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(MataPelajaranPeer::MATA_PELAJARAN_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the mata_pelajaran_id column
	 * 
	 * @param     int|array $mataPelajaranId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByMataPelajaranId($mataPelajaranId = null, $comparison = null)
	{
		if (is_array($mataPelajaranId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(MataPelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaranId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(MataPelajaranPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the kompetensi_keahlian_id column
	 * 
	 * @param     int|array $kompetensiKeahlianId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByKompetensiKeahlianId($kompetensiKeahlianId = null, $comparison = null)
	{
		if (is_array($kompetensiKeahlianId)) {
			$useMinMax = false;
			if (isset($kompetensiKeahlianId['min'])) {
				$this->addUsingAlias(MataPelajaranPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlianId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($kompetensiKeahlianId['max'])) {
				$this->addUsingAlias(MataPelajaranPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlianId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(MataPelajaranPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlianId, $comparison);
	}

	/**
	 * Filter the query by a related KompetensiKeahlian object
	 *
	 * @param     KompetensiKeahlian $kompetensiKeahlian  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByKompetensiKeahlian($kompetensiKeahlian, $comparison = null)
	{
		return $this
			->addUsingAlias(MataPelajaranPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlian->getKompetensiKeahlianId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the KompetensiKeahlian relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function joinKompetensiKeahlian($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('KompetensiKeahlian');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'KompetensiKeahlian');
		}
		
		return $this;
	}

	/**
	 * Use the KompetensiKeahlian relation KompetensiKeahlian object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KompetensiKeahlianQuery A secondary query class using the current class as primary query
	 */
	public function useKompetensiKeahlianQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinKompetensiKeahlian($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'KompetensiKeahlian', 'KompetensiKeahlianQuery');
	}

	/**
	 * Filter the query by a related Tugas object
	 *
	 * @param     Tugas $tugas  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function filterByTugas($tugas, $comparison = null)
	{
		return $this
			->addUsingAlias(MataPelajaranPeer::MATA_PELAJARAN_ID, $tugas->getMataPelajaranId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Tugas relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function joinTugas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tugas');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tugas');
		}
		
		return $this;
	}

	/**
	 * Use the Tugas relation Tugas object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery A secondary query class using the current class as primary query
	 */
	public function useTugasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTugas($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tugas', 'TugasQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     MataPelajaran $mataPelajaran Object to remove from the list of results
	 *
	 * @return    MataPelajaranQuery The current query, for fluid interface
	 */
	public function prune($mataPelajaran = null)
	{
		if ($mataPelajaran) {
			$this->addUsingAlias(MataPelajaranPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseMataPelajaranQuery
