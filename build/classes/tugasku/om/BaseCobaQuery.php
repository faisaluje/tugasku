<?php


/**
 * Base class that represents a query for the 'coba' table.
 *
 * 
 *
 * @method     CobaQuery orderByCobaId($order = Criteria::ASC) Order by the coba_id column
 * @method     CobaQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     CobaQuery orderByDeskripsi($order = Criteria::ASC) Order by the deskripsi column
 * @method     CobaQuery orderByJabatanId($order = Criteria::ASC) Order by the jabatan_id column
 *
 * @method     CobaQuery groupByCobaId() Group by the coba_id column
 * @method     CobaQuery groupByNama() Group by the nama column
 * @method     CobaQuery groupByDeskripsi() Group by the deskripsi column
 * @method     CobaQuery groupByJabatanId() Group by the jabatan_id column
 *
 * @method     CobaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     CobaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     CobaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     CobaQuery leftJoinJabatan($relationAlias = null) Adds a LEFT JOIN clause to the query using the Jabatan relation
 * @method     CobaQuery rightJoinJabatan($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Jabatan relation
 * @method     CobaQuery innerJoinJabatan($relationAlias = null) Adds a INNER JOIN clause to the query using the Jabatan relation
 *
 * @method     Coba findOne(PropelPDO $con = null) Return the first Coba matching the query
 * @method     Coba findOneOrCreate(PropelPDO $con = null) Return the first Coba matching the query, or a new Coba object populated from the query conditions when no match is found
 *
 * @method     Coba findOneByCobaId(int $coba_id) Return the first Coba filtered by the coba_id column
 * @method     Coba findOneByNama(string $nama) Return the first Coba filtered by the nama column
 * @method     Coba findOneByDeskripsi(string $deskripsi) Return the first Coba filtered by the deskripsi column
 * @method     Coba findOneByJabatanId(int $jabatan_id) Return the first Coba filtered by the jabatan_id column
 *
 * @method     array findByCobaId(int $coba_id) Return Coba objects filtered by the coba_id column
 * @method     array findByNama(string $nama) Return Coba objects filtered by the nama column
 * @method     array findByDeskripsi(string $deskripsi) Return Coba objects filtered by the deskripsi column
 * @method     array findByJabatanId(int $jabatan_id) Return Coba objects filtered by the jabatan_id column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseCobaQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseCobaQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'Coba', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new CobaQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    CobaQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof CobaQuery) {
			return $criteria;
		}
		$query = new CobaQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Coba|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = CobaPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(CobaPeer::COBA_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(CobaPeer::COBA_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the coba_id column
	 * 
	 * @param     int|array $cobaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByCobaId($cobaId = null, $comparison = null)
	{
		if (is_array($cobaId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(CobaPeer::COBA_ID, $cobaId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CobaPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the deskripsi column
	 * 
	 * @param     string $deskripsi The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByDeskripsi($deskripsi = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($deskripsi)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $deskripsi)) {
				$deskripsi = str_replace('*', '%', $deskripsi);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(CobaPeer::DESKRIPSI, $deskripsi, $comparison);
	}

	/**
	 * Filter the query on the jabatan_id column
	 * 
	 * @param     int|array $jabatanId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByJabatanId($jabatanId = null, $comparison = null)
	{
		if (is_array($jabatanId)) {
			$useMinMax = false;
			if (isset($jabatanId['min'])) {
				$this->addUsingAlias(CobaPeer::JABATAN_ID, $jabatanId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($jabatanId['max'])) {
				$this->addUsingAlias(CobaPeer::JABATAN_ID, $jabatanId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(CobaPeer::JABATAN_ID, $jabatanId, $comparison);
	}

	/**
	 * Filter the query by a related Jabatan object
	 *
	 * @param     Jabatan $jabatan  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function filterByJabatan($jabatan, $comparison = null)
	{
		return $this
			->addUsingAlias(CobaPeer::JABATAN_ID, $jabatan->getJabatanId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Jabatan relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function joinJabatan($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Jabatan');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Jabatan');
		}
		
		return $this;
	}

	/**
	 * Use the Jabatan relation Jabatan object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    JabatanQuery A secondary query class using the current class as primary query
	 */
	public function useJabatanQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinJabatan($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Jabatan', 'JabatanQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Coba $coba Object to remove from the list of results
	 *
	 * @return    CobaQuery The current query, for fluid interface
	 */
	public function prune($coba = null)
	{
		if ($coba) {
			$this->addUsingAlias(CobaPeer::COBA_ID, $coba->getCobaId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseCobaQuery
