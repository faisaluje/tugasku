<?php


/**
 * Base class that represents a query for the 'tugas' table.
 *
 * 
 *
 * @method     TugasQuery orderByTugasId($order = Criteria::ASC) Order by the tugas_id column
 * @method     TugasQuery orderByJenisTugasId($order = Criteria::ASC) Order by the jenis_tugas_id column
 * @method     TugasQuery orderByMataPelajaranId($order = Criteria::ASC) Order by the mata_pelajaran_id column
 * @method     TugasQuery orderByNis($order = Criteria::ASC) Order by the nis column
 * @method     TugasQuery orderByTanggalPengumpulan($order = Criteria::ASC) Order by the tanggal_pengumpulan column
 * @method     TugasQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     TugasQuery orderByFile($order = Criteria::ASC) Order by the file column
 * @method     TugasQuery orderByAbstrak($order = Criteria::ASC) Order by the abstrak column
 * @method     TugasQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 *
 * @method     TugasQuery groupByTugasId() Group by the tugas_id column
 * @method     TugasQuery groupByJenisTugasId() Group by the jenis_tugas_id column
 * @method     TugasQuery groupByMataPelajaranId() Group by the mata_pelajaran_id column
 * @method     TugasQuery groupByNis() Group by the nis column
 * @method     TugasQuery groupByTanggalPengumpulan() Group by the tanggal_pengumpulan column
 * @method     TugasQuery groupByNama() Group by the nama column
 * @method     TugasQuery groupByFile() Group by the file column
 * @method     TugasQuery groupByAbstrak() Group by the abstrak column
 * @method     TugasQuery groupByPenggunaId() Group by the pengguna_id column
 *
 * @method     TugasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     TugasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     TugasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     TugasQuery leftJoinJenisTugas($relationAlias = null) Adds a LEFT JOIN clause to the query using the JenisTugas relation
 * @method     TugasQuery rightJoinJenisTugas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the JenisTugas relation
 * @method     TugasQuery innerJoinJenisTugas($relationAlias = null) Adds a INNER JOIN clause to the query using the JenisTugas relation
 *
 * @method     TugasQuery leftJoinMataPelajaran($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaran relation
 * @method     TugasQuery rightJoinMataPelajaran($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaran relation
 * @method     TugasQuery innerJoinMataPelajaran($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaran relation
 *
 * @method     TugasQuery leftJoinSiswa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Siswa relation
 * @method     TugasQuery rightJoinSiswa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Siswa relation
 * @method     TugasQuery innerJoinSiswa($relationAlias = null) Adds a INNER JOIN clause to the query using the Siswa relation
 *
 * @method     TugasQuery leftJoinPengguna($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pengguna relation
 * @method     TugasQuery rightJoinPengguna($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pengguna relation
 * @method     TugasQuery innerJoinPengguna($relationAlias = null) Adds a INNER JOIN clause to the query using the Pengguna relation
 *
 * @method     Tugas findOne(PropelPDO $con = null) Return the first Tugas matching the query
 * @method     Tugas findOneOrCreate(PropelPDO $con = null) Return the first Tugas matching the query, or a new Tugas object populated from the query conditions when no match is found
 *
 * @method     Tugas findOneByTugasId(int $tugas_id) Return the first Tugas filtered by the tugas_id column
 * @method     Tugas findOneByJenisTugasId(int $jenis_tugas_id) Return the first Tugas filtered by the jenis_tugas_id column
 * @method     Tugas findOneByMataPelajaranId(int $mata_pelajaran_id) Return the first Tugas filtered by the mata_pelajaran_id column
 * @method     Tugas findOneByNis(string $nis) Return the first Tugas filtered by the nis column
 * @method     Tugas findOneByTanggalPengumpulan(string $tanggal_pengumpulan) Return the first Tugas filtered by the tanggal_pengumpulan column
 * @method     Tugas findOneByNama(string $nama) Return the first Tugas filtered by the nama column
 * @method     Tugas findOneByFile(string $file) Return the first Tugas filtered by the file column
 * @method     Tugas findOneByAbstrak(string $abstrak) Return the first Tugas filtered by the abstrak column
 * @method     Tugas findOneByPenggunaId(int $pengguna_id) Return the first Tugas filtered by the pengguna_id column
 *
 * @method     array findByTugasId(int $tugas_id) Return Tugas objects filtered by the tugas_id column
 * @method     array findByJenisTugasId(int $jenis_tugas_id) Return Tugas objects filtered by the jenis_tugas_id column
 * @method     array findByMataPelajaranId(int $mata_pelajaran_id) Return Tugas objects filtered by the mata_pelajaran_id column
 * @method     array findByNis(string $nis) Return Tugas objects filtered by the nis column
 * @method     array findByTanggalPengumpulan(string $tanggal_pengumpulan) Return Tugas objects filtered by the tanggal_pengumpulan column
 * @method     array findByNama(string $nama) Return Tugas objects filtered by the nama column
 * @method     array findByFile(string $file) Return Tugas objects filtered by the file column
 * @method     array findByAbstrak(string $abstrak) Return Tugas objects filtered by the abstrak column
 * @method     array findByPenggunaId(int $pengguna_id) Return Tugas objects filtered by the pengguna_id column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseTugasQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseTugasQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'Tugas', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new TugasQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    TugasQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof TugasQuery) {
			return $criteria;
		}
		$query = new TugasQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    Tugas|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = TugasPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(TugasPeer::TUGAS_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(TugasPeer::TUGAS_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the tugas_id column
	 * 
	 * @param     int|array $tugasId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByTugasId($tugasId = null, $comparison = null)
	{
		if (is_array($tugasId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(TugasPeer::TUGAS_ID, $tugasId, $comparison);
	}

	/**
	 * Filter the query on the jenis_tugas_id column
	 * 
	 * @param     int|array $jenisTugasId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByJenisTugasId($jenisTugasId = null, $comparison = null)
	{
		if (is_array($jenisTugasId)) {
			$useMinMax = false;
			if (isset($jenisTugasId['min'])) {
				$this->addUsingAlias(TugasPeer::JENIS_TUGAS_ID, $jenisTugasId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($jenisTugasId['max'])) {
				$this->addUsingAlias(TugasPeer::JENIS_TUGAS_ID, $jenisTugasId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TugasPeer::JENIS_TUGAS_ID, $jenisTugasId, $comparison);
	}

	/**
	 * Filter the query on the mata_pelajaran_id column
	 * 
	 * @param     int|array $mataPelajaranId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByMataPelajaranId($mataPelajaranId = null, $comparison = null)
	{
		if (is_array($mataPelajaranId)) {
			$useMinMax = false;
			if (isset($mataPelajaranId['min'])) {
				$this->addUsingAlias(TugasPeer::MATA_PELAJARAN_ID, $mataPelajaranId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($mataPelajaranId['max'])) {
				$this->addUsingAlias(TugasPeer::MATA_PELAJARAN_ID, $mataPelajaranId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TugasPeer::MATA_PELAJARAN_ID, $mataPelajaranId, $comparison);
	}

	/**
	 * Filter the query on the nis column
	 * 
	 * @param     string $nis The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByNis($nis = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nis)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nis)) {
				$nis = str_replace('*', '%', $nis);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TugasPeer::NIS, $nis, $comparison);
	}

	/**
	 * Filter the query on the tanggal_pengumpulan column
	 * 
	 * @param     string $tanggalPengumpulan The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByTanggalPengumpulan($tanggalPengumpulan = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($tanggalPengumpulan)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $tanggalPengumpulan)) {
				$tanggalPengumpulan = str_replace('*', '%', $tanggalPengumpulan);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TugasPeer::TANGGAL_PENGUMPULAN, $tanggalPengumpulan, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TugasPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the file column
	 * 
	 * @param     string $file The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByFile($file = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($file)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $file)) {
				$file = str_replace('*', '%', $file);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TugasPeer::FILE, $file, $comparison);
	}

	/**
	 * Filter the query on the abstrak column
	 * 
	 * @param     string $abstrak The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByAbstrak($abstrak = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($abstrak)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $abstrak)) {
				$abstrak = str_replace('*', '%', $abstrak);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(TugasPeer::ABSTRAK, $abstrak, $comparison);
	}

	/**
	 * Filter the query on the pengguna_id column
	 * 
	 * @param     int|array $penggunaId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByPenggunaId($penggunaId = null, $comparison = null)
	{
		if (is_array($penggunaId)) {
			$useMinMax = false;
			if (isset($penggunaId['min'])) {
				$this->addUsingAlias(TugasPeer::PENGGUNA_ID, $penggunaId['min'], Criteria::GREATER_EQUAL);
				$useMinMax = true;
			}
			if (isset($penggunaId['max'])) {
				$this->addUsingAlias(TugasPeer::PENGGUNA_ID, $penggunaId['max'], Criteria::LESS_EQUAL);
				$useMinMax = true;
			}
			if ($useMinMax) {
				return $this;
			}
			if (null === $comparison) {
				$comparison = Criteria::IN;
			}
		}
		return $this->addUsingAlias(TugasPeer::PENGGUNA_ID, $penggunaId, $comparison);
	}

	/**
	 * Filter the query by a related JenisTugas object
	 *
	 * @param     JenisTugas $jenisTugas  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByJenisTugas($jenisTugas, $comparison = null)
	{
		return $this
			->addUsingAlias(TugasPeer::JENIS_TUGAS_ID, $jenisTugas->getJenisTugasId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the JenisTugas relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function joinJenisTugas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('JenisTugas');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'JenisTugas');
		}
		
		return $this;
	}

	/**
	 * Use the JenisTugas relation JenisTugas object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    JenisTugasQuery A secondary query class using the current class as primary query
	 */
	public function useJenisTugasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinJenisTugas($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'JenisTugas', 'JenisTugasQuery');
	}

	/**
	 * Filter the query by a related MataPelajaran object
	 *
	 * @param     MataPelajaran $mataPelajaran  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByMataPelajaran($mataPelajaran, $comparison = null)
	{
		return $this
			->addUsingAlias(TugasPeer::MATA_PELAJARAN_ID, $mataPelajaran->getMataPelajaranId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the MataPelajaran relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function joinMataPelajaran($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('MataPelajaran');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'MataPelajaran');
		}
		
		return $this;
	}

	/**
	 * Use the MataPelajaran relation MataPelajaran object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    MataPelajaranQuery A secondary query class using the current class as primary query
	 */
	public function useMataPelajaranQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinMataPelajaran($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'MataPelajaran', 'MataPelajaranQuery');
	}

	/**
	 * Filter the query by a related Siswa object
	 *
	 * @param     Siswa $siswa  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterBySiswa($siswa, $comparison = null)
	{
		return $this
			->addUsingAlias(TugasPeer::NIS, $siswa->getNis(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Siswa relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function joinSiswa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Siswa');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Siswa');
		}
		
		return $this;
	}

	/**
	 * Use the Siswa relation Siswa object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    SiswaQuery A secondary query class using the current class as primary query
	 */
	public function useSiswaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinSiswa($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Siswa', 'SiswaQuery');
	}

	/**
	 * Filter the query by a related Pengguna object
	 *
	 * @param     Pengguna $pengguna  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function filterByPengguna($pengguna, $comparison = null)
	{
		return $this
			->addUsingAlias(TugasPeer::PENGGUNA_ID, $pengguna->getPenggunaId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Pengguna relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function joinPengguna($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Pengguna');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Pengguna');
		}
		
		return $this;
	}

	/**
	 * Use the Pengguna relation Pengguna object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    PenggunaQuery A secondary query class using the current class as primary query
	 */
	public function usePenggunaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinPengguna($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Pengguna', 'PenggunaQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     Tugas $tugas Object to remove from the list of results
	 *
	 * @return    TugasQuery The current query, for fluid interface
	 */
	public function prune($tugas = null)
	{
		if ($tugas) {
			$this->addUsingAlias(TugasPeer::TUGAS_ID, $tugas->getTugasId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseTugasQuery
