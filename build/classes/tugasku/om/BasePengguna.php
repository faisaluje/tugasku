<?php


/**
 * Base class that represents a row from the 'pengguna' table.
 *
 * 
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BasePengguna extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'PenggunaPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        PenggunaPeer
	 */
	protected static $peer;

	/**
	 * The value for the pengguna_id field.
	 * @var        int
	 */
	protected $pengguna_id;

	/**
	 * The value for the username field.
	 * @var        string
	 */
	protected $username;

	/**
	 * The value for the password field.
	 * @var        string
	 */
	protected $password;

	/**
	 * The value for the nama field.
	 * @var        string
	 */
	protected $nama;

	/**
	 * The value for the telepon field.
	 * @var        string
	 */
	protected $telepon;

	/**
	 * The value for the foto field.
	 * @var        string
	 */
	protected $foto;

	/**
	 * The value for the jabatan_id field.
	 * @var        int
	 */
	protected $jabatan_id;

	/**
	 * @var        Jabatan
	 */
	protected $aJabatan;

	/**
	 * @var        array Tugas[] Collection to store aggregation of Tugas objects.
	 */
	protected $collTugass;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [pengguna_id] column value.
	 * 
	 * @return     int
	 */
	public function getPenggunaId()
	{
		return $this->pengguna_id;
	}

	/**
	 * Get the [username] column value.
	 * 
	 * @return     string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * Get the [password] column value.
	 * 
	 * @return     string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * Get the [nama] column value.
	 * 
	 * @return     string
	 */
	public function getNama()
	{
		return $this->nama;
	}

	/**
	 * Get the [telepon] column value.
	 * 
	 * @return     string
	 */
	public function getTelepon()
	{
		return $this->telepon;
	}

	/**
	 * Get the [foto] column value.
	 * 
	 * @return     string
	 */
	public function getFoto()
	{
		return $this->foto;
	}

	/**
	 * Get the [jabatan_id] column value.
	 * 
	 * @return     int
	 */
	public function getJabatanId()
	{
		return $this->jabatan_id;
	}

	/**
	 * Set the value of [pengguna_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setPenggunaId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->pengguna_id !== $v) {
			$this->pengguna_id = $v;
			$this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
		}

		return $this;
	} // setPenggunaId()

	/**
	 * Set the value of [username] column.
	 * 
	 * @param      string $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setUsername($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->username !== $v) {
			$this->username = $v;
			$this->modifiedColumns[] = PenggunaPeer::USERNAME;
		}

		return $this;
	} // setUsername()

	/**
	 * Set the value of [password] column.
	 * 
	 * @param      string $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setPassword($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->password !== $v) {
			$this->password = $v;
			$this->modifiedColumns[] = PenggunaPeer::PASSWORD;
		}

		return $this;
	} // setPassword()

	/**
	 * Set the value of [nama] column.
	 * 
	 * @param      string $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setNama($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = PenggunaPeer::NAMA;
		}

		return $this;
	} // setNama()

	/**
	 * Set the value of [telepon] column.
	 * 
	 * @param      string $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setTelepon($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->telepon !== $v) {
			$this->telepon = $v;
			$this->modifiedColumns[] = PenggunaPeer::TELEPON;
		}

		return $this;
	} // setTelepon()

	/**
	 * Set the value of [foto] column.
	 * 
	 * @param      string $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setFoto($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->foto !== $v) {
			$this->foto = $v;
			$this->modifiedColumns[] = PenggunaPeer::FOTO;
		}

		return $this;
	} // setFoto()

	/**
	 * Set the value of [jabatan_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Pengguna The current object (for fluent API support)
	 */
	public function setJabatanId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->jabatan_id !== $v) {
			$this->jabatan_id = $v;
			$this->modifiedColumns[] = PenggunaPeer::JABATAN_ID;
		}

		if ($this->aJabatan !== null && $this->aJabatan->getJabatanId() !== $v) {
			$this->aJabatan = null;
		}

		return $this;
	} // setJabatanId()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->pengguna_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->username = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->password = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->nama = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->telepon = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->foto = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->jabatan_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 7; // 7 = PenggunaPeer::NUM_COLUMNS - PenggunaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Pengguna object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

		if ($this->aJabatan !== null && $this->jabatan_id !== $this->aJabatan->getJabatanId()) {
			$this->aJabatan = null;
		}
	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = PenggunaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->aJabatan = null;
			$this->collTugass = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				PenggunaQuery::create()
					->filterByPrimaryKey($this->getPrimaryKey())
					->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(PenggunaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				PenggunaPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			// We call the save method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aJabatan !== null) {
				if ($this->aJabatan->isModified() || $this->aJabatan->isNew()) {
					$affectedRows += $this->aJabatan->save($con);
				}
				$this->setJabatan($this->aJabatan);
			}

			if ($this->isNew() ) {
				$this->modifiedColumns[] = PenggunaPeer::PENGGUNA_ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$criteria = $this->buildCriteria();
					if ($criteria->keyContainsValue(PenggunaPeer::PENGGUNA_ID) ) {
						throw new PropelException('Cannot insert a value for auto-increment primary key ('.PenggunaPeer::PENGGUNA_ID.')');
					}

					$pk = BasePeer::doInsert($criteria, $con);
					$affectedRows += 1;
					$this->setPenggunaId($pk);  //[IMV] update autoincrement primary key
					$this->setNew(false);
				} else {
					$affectedRows += PenggunaPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collTugass !== null) {
				foreach ($this->collTugass as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			// We call the validate method on the following object(s) if they
			// were passed to this object by their coresponding set
			// method.  This object relates to these object(s) by a
			// foreign key reference.

			if ($this->aJabatan !== null) {
				if (!$this->aJabatan->validate($columns)) {
					$failureMap = array_merge($failureMap, $this->aJabatan->getValidationFailures());
				}
			}


			if (($retval = PenggunaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTugass !== null) {
					foreach ($this->collTugass as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getPenggunaId();
				break;
			case 1:
				return $this->getUsername();
				break;
			case 2:
				return $this->getPassword();
				break;
			case 3:
				return $this->getNama();
				break;
			case 4:
				return $this->getTelepon();
				break;
			case 5:
				return $this->getFoto();
				break;
			case 6:
				return $this->getJabatanId();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $includeForeignObjects = false)
	{
		$keys = PenggunaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getPenggunaId(),
			$keys[1] => $this->getUsername(),
			$keys[2] => $this->getPassword(),
			$keys[3] => $this->getNama(),
			$keys[4] => $this->getTelepon(),
			$keys[5] => $this->getFoto(),
			$keys[6] => $this->getJabatanId(),
		);
		if ($includeForeignObjects) {
			if (null !== $this->aJabatan) {
				$result['Jabatan'] = $this->aJabatan->toArray($keyType, $includeLazyLoadColumns, true);
			}
		}
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = PenggunaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setPenggunaId($value);
				break;
			case 1:
				$this->setUsername($value);
				break;
			case 2:
				$this->setPassword($value);
				break;
			case 3:
				$this->setNama($value);
				break;
			case 4:
				$this->setTelepon($value);
				break;
			case 5:
				$this->setFoto($value);
				break;
			case 6:
				$this->setJabatanId($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = PenggunaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setPenggunaId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setNama($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTelepon($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setFoto($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setJabatanId($arr[$keys[6]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(PenggunaPeer::DATABASE_NAME);

		if ($this->isColumnModified(PenggunaPeer::PENGGUNA_ID)) $criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);
		if ($this->isColumnModified(PenggunaPeer::USERNAME)) $criteria->add(PenggunaPeer::USERNAME, $this->username);
		if ($this->isColumnModified(PenggunaPeer::PASSWORD)) $criteria->add(PenggunaPeer::PASSWORD, $this->password);
		if ($this->isColumnModified(PenggunaPeer::NAMA)) $criteria->add(PenggunaPeer::NAMA, $this->nama);
		if ($this->isColumnModified(PenggunaPeer::TELEPON)) $criteria->add(PenggunaPeer::TELEPON, $this->telepon);
		if ($this->isColumnModified(PenggunaPeer::FOTO)) $criteria->add(PenggunaPeer::FOTO, $this->foto);
		if ($this->isColumnModified(PenggunaPeer::JABATAN_ID)) $criteria->add(PenggunaPeer::JABATAN_ID, $this->jabatan_id);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(PenggunaPeer::DATABASE_NAME);
		$criteria->add(PenggunaPeer::PENGGUNA_ID, $this->pengguna_id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getPenggunaId();
	}

	/**
	 * Generic method to set the primary key (pengguna_id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setPenggunaId($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getPenggunaId();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Pengguna (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{
		$copyObj->setUsername($this->username);
		$copyObj->setPassword($this->password);
		$copyObj->setNama($this->nama);
		$copyObj->setTelepon($this->telepon);
		$copyObj->setFoto($this->foto);
		$copyObj->setJabatanId($this->jabatan_id);

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getTugass() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTugas($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);
		$copyObj->setPenggunaId(NULL); // this is a auto-increment column, so set to default value
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Pengguna Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     PenggunaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new PenggunaPeer();
		}
		return self::$peer;
	}

	/**
	 * Declares an association between this object and a Jabatan object.
	 *
	 * @param      Jabatan $v
	 * @return     Pengguna The current object (for fluent API support)
	 * @throws     PropelException
	 */
	public function setJabatan(Jabatan $v = null)
	{
		if ($v === null) {
			$this->setJabatanId(NULL);
		} else {
			$this->setJabatanId($v->getJabatanId());
		}

		$this->aJabatan = $v;

		// Add binding for other direction of this n:n relationship.
		// If this object has already been added to the Jabatan object, it will not be re-added.
		if ($v !== null) {
			$v->addPengguna($this);
		}

		return $this;
	}


	/**
	 * Get the associated Jabatan object
	 *
	 * @param      PropelPDO Optional Connection object.
	 * @return     Jabatan The associated Jabatan object.
	 * @throws     PropelException
	 */
	public function getJabatan(PropelPDO $con = null)
	{
		if ($this->aJabatan === null && ($this->jabatan_id !== null)) {
			$this->aJabatan = JabatanQuery::create()->findPk($this->jabatan_id, $con);
			/* The following can be used additionally to
				 guarantee the related object contains a reference
				 to this object.  This level of coupling may, however, be
				 undesirable since it could result in an only partially populated collection
				 in the referenced object.
				 $this->aJabatan->addPenggunas($this);
			 */
		}
		return $this->aJabatan;
	}

	/**
	 * Clears out the collTugass collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTugass()
	 */
	public function clearTugass()
	{
		$this->collTugass = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTugass collection.
	 *
	 * By default this just sets the collTugass collection to an empty array (like clearcollTugass());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initTugass()
	{
		$this->collTugass = new PropelObjectCollection();
		$this->collTugass->setModel('Tugas');
	}

	/**
	 * Gets an array of Tugas objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Pengguna is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 * @throws     PropelException
	 */
	public function getTugass($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTugass || null !== $criteria) {
			if ($this->isNew() && null === $this->collTugass) {
				// return empty collection
				$this->initTugass();
			} else {
				$collTugass = TugasQuery::create(null, $criteria)
					->filterByPengguna($this)
					->find($con);
				if (null !== $criteria) {
					return $collTugass;
				}
				$this->collTugass = $collTugass;
			}
		}
		return $this->collTugass;
	}

	/**
	 * Returns the number of related Tugas objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Tugas objects.
	 * @throws     PropelException
	 */
	public function countTugass(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTugass || null !== $criteria) {
			if ($this->isNew() && null === $this->collTugass) {
				return 0;
			} else {
				$query = TugasQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterByPengguna($this)
					->count($con);
			}
		} else {
			return count($this->collTugass);
		}
	}

	/**
	 * Method called to associate a Tugas object to this object
	 * through the Tugas foreign key attribute.
	 *
	 * @param      Tugas $l Tugas
	 * @return     void
	 * @throws     PropelException
	 */
	public function addTugas(Tugas $l)
	{
		if ($this->collTugass === null) {
			$this->initTugass();
		}
		if (!$this->collTugass->contains($l)) { // only add it if the **same** object is not already associated
			$this->collTugass[]= $l;
			$l->setPengguna($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Pengguna is new, it will return
	 * an empty collection; or if this Pengguna has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Pengguna.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinJenisTugas($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('JenisTugas', $join_behavior);

		return $this->getTugass($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Pengguna is new, it will return
	 * an empty collection; or if this Pengguna has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Pengguna.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinMataPelajaran($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('MataPelajaran', $join_behavior);

		return $this->getTugass($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Pengguna is new, it will return
	 * an empty collection; or if this Pengguna has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Pengguna.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinSiswa($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('Siswa', $join_behavior);

		return $this->getTugass($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->pengguna_id = null;
		$this->username = null;
		$this->password = null;
		$this->nama = null;
		$this->telepon = null;
		$this->foto = null;
		$this->jabatan_id = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collTugass) {
				foreach ((array) $this->collTugass as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collTugass = null;
		$this->aJabatan = null;
	}

	/**
	 * Catches calls to virtual methods
	 */
	public function __call($name, $params)
	{
		if (preg_match('/get(\w+)/', $name, $matches)) {
			$virtualColumn = $matches[1];
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
			// no lcfirst in php<5.3...
			$virtualColumn[0] = strtolower($virtualColumn[0]);
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
		}
		return parent::__call($name, $params);
	}

} // BasePengguna
