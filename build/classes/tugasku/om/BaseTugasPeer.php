<?php


/**
 * Base static class for performing query and update operations on the 'tugas' table.
 *
 * 
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseTugasPeer {

	/** the default database name for this class */
	const DATABASE_NAME = 'tugasku';

	/** the table name for this class */
	const TABLE_NAME = 'tugas';

	/** the related Propel class for this table */
	const OM_CLASS = 'Tugas';

	/** A class that can be returned by this peer. */
	const CLASS_DEFAULT = 'tugasku.Tugas';

	/** the related TableMap class for this table */
	const TM_CLASS = 'TugasTableMap';
	
	/** The total number of columns. */
	const NUM_COLUMNS = 9;

	/** The number of lazy-loaded columns. */
	const NUM_LAZY_LOAD_COLUMNS = 0;

	/** the column name for the TUGAS_ID field */
	const TUGAS_ID = 'tugas.TUGAS_ID';

	/** the column name for the JENIS_TUGAS_ID field */
	const JENIS_TUGAS_ID = 'tugas.JENIS_TUGAS_ID';

	/** the column name for the MATA_PELAJARAN_ID field */
	const MATA_PELAJARAN_ID = 'tugas.MATA_PELAJARAN_ID';

	/** the column name for the NIS field */
	const NIS = 'tugas.NIS';

	/** the column name for the TANGGAL_PENGUMPULAN field */
	const TANGGAL_PENGUMPULAN = 'tugas.TANGGAL_PENGUMPULAN';

	/** the column name for the NAMA field */
	const NAMA = 'tugas.NAMA';

	/** the column name for the FILE field */
	const FILE = 'tugas.FILE';

	/** the column name for the ABSTRAK field */
	const ABSTRAK = 'tugas.ABSTRAK';

	/** the column name for the PENGGUNA_ID field */
	const PENGGUNA_ID = 'tugas.PENGGUNA_ID';

	/**
	 * An identiy map to hold any loaded instances of Tugas objects.
	 * This must be public so that other peer classes can access this when hydrating from JOIN
	 * queries.
	 * @var        array Tugas[]
	 */
	public static $instances = array();


	/**
	 * holds an array of fieldnames
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
	 */
	private static $fieldNames = array (
		BasePeer::TYPE_PHPNAME => array ('TugasId', 'JenisTugasId', 'MataPelajaranId', 'Nis', 'TanggalPengumpulan', 'Nama', 'File', 'Abstrak', 'PenggunaId', ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('tugasId', 'jenisTugasId', 'mataPelajaranId', 'nis', 'tanggalPengumpulan', 'nama', 'file', 'abstrak', 'penggunaId', ),
		BasePeer::TYPE_COLNAME => array (self::TUGAS_ID, self::JENIS_TUGAS_ID, self::MATA_PELAJARAN_ID, self::NIS, self::TANGGAL_PENGUMPULAN, self::NAMA, self::FILE, self::ABSTRAK, self::PENGGUNA_ID, ),
		BasePeer::TYPE_RAW_COLNAME => array ('TUGAS_ID', 'JENIS_TUGAS_ID', 'MATA_PELAJARAN_ID', 'NIS', 'TANGGAL_PENGUMPULAN', 'NAMA', 'FILE', 'ABSTRAK', 'PENGGUNA_ID', ),
		BasePeer::TYPE_FIELDNAME => array ('tugas_id', 'jenis_tugas_id', 'mata_pelajaran_id', 'nis', 'tanggal_pengumpulan', 'nama', 'file', 'abstrak', 'pengguna_id', ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	/**
	 * holds an array of keys for quick access to the fieldnames array
	 *
	 * first dimension keys are the type constants
	 * e.g. self::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
	 */
	private static $fieldKeys = array (
		BasePeer::TYPE_PHPNAME => array ('TugasId' => 0, 'JenisTugasId' => 1, 'MataPelajaranId' => 2, 'Nis' => 3, 'TanggalPengumpulan' => 4, 'Nama' => 5, 'File' => 6, 'Abstrak' => 7, 'PenggunaId' => 8, ),
		BasePeer::TYPE_STUDLYPHPNAME => array ('tugasId' => 0, 'jenisTugasId' => 1, 'mataPelajaranId' => 2, 'nis' => 3, 'tanggalPengumpulan' => 4, 'nama' => 5, 'file' => 6, 'abstrak' => 7, 'penggunaId' => 8, ),
		BasePeer::TYPE_COLNAME => array (self::TUGAS_ID => 0, self::JENIS_TUGAS_ID => 1, self::MATA_PELAJARAN_ID => 2, self::NIS => 3, self::TANGGAL_PENGUMPULAN => 4, self::NAMA => 5, self::FILE => 6, self::ABSTRAK => 7, self::PENGGUNA_ID => 8, ),
		BasePeer::TYPE_RAW_COLNAME => array ('TUGAS_ID' => 0, 'JENIS_TUGAS_ID' => 1, 'MATA_PELAJARAN_ID' => 2, 'NIS' => 3, 'TANGGAL_PENGUMPULAN' => 4, 'NAMA' => 5, 'FILE' => 6, 'ABSTRAK' => 7, 'PENGGUNA_ID' => 8, ),
		BasePeer::TYPE_FIELDNAME => array ('tugas_id' => 0, 'jenis_tugas_id' => 1, 'mata_pelajaran_id' => 2, 'nis' => 3, 'tanggal_pengumpulan' => 4, 'nama' => 5, 'file' => 6, 'abstrak' => 7, 'pengguna_id' => 8, ),
		BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, )
	);

	/**
	 * Translates a fieldname to another type
	 *
	 * @param      string $name field name
	 * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @param      string $toType   One of the class type constants
	 * @return     string translated name of the field.
	 * @throws     PropelException - if the specified name could not be found in the fieldname mappings.
	 */
	static public function translateFieldName($name, $fromType, $toType)
	{
		$toNames = self::getFieldNames($toType);
		$key = isset(self::$fieldKeys[$fromType][$name]) ? self::$fieldKeys[$fromType][$name] : null;
		if ($key === null) {
			throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(self::$fieldKeys[$fromType], true));
		}
		return $toNames[$key];
	}

	/**
	 * Returns an array of field names.
	 *
	 * @param      string $type The type of fieldnames to return:
	 *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     array A list of field names
	 */

	static public function getFieldNames($type = BasePeer::TYPE_PHPNAME)
	{
		if (!array_key_exists($type, self::$fieldNames)) {
			throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
		}
		return self::$fieldNames[$type];
	}

	/**
	 * Convenience method which changes table.column to alias.column.
	 *
	 * Using this method you can maintain SQL abstraction while using column aliases.
	 * <code>
	 *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
	 *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
	 * </code>
	 * @param      string $alias The alias for the current table.
	 * @param      string $column The column name for current table. (i.e. TugasPeer::COLUMN_NAME).
	 * @return     string
	 */
	public static function alias($alias, $column)
	{
		return str_replace(TugasPeer::TABLE_NAME.'.', $alias.'.', $column);
	}

	/**
	 * Add all the columns needed to create a new object.
	 *
	 * Note: any columns that were marked with lazyLoad="true" in the
	 * XML schema will not be added to the select list and only loaded
	 * on demand.
	 *
	 * @param      Criteria $criteria object containing the columns to add.
	 * @param      string   $alias    optional table alias
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function addSelectColumns(Criteria $criteria, $alias = null)
	{
		if (null === $alias) {
			$criteria->addSelectColumn(TugasPeer::TUGAS_ID);
			$criteria->addSelectColumn(TugasPeer::JENIS_TUGAS_ID);
			$criteria->addSelectColumn(TugasPeer::MATA_PELAJARAN_ID);
			$criteria->addSelectColumn(TugasPeer::NIS);
			$criteria->addSelectColumn(TugasPeer::TANGGAL_PENGUMPULAN);
			$criteria->addSelectColumn(TugasPeer::NAMA);
			$criteria->addSelectColumn(TugasPeer::FILE);
			$criteria->addSelectColumn(TugasPeer::ABSTRAK);
			$criteria->addSelectColumn(TugasPeer::PENGGUNA_ID);
		} else {
			$criteria->addSelectColumn($alias . '.TUGAS_ID');
			$criteria->addSelectColumn($alias . '.JENIS_TUGAS_ID');
			$criteria->addSelectColumn($alias . '.MATA_PELAJARAN_ID');
			$criteria->addSelectColumn($alias . '.NIS');
			$criteria->addSelectColumn($alias . '.TANGGAL_PENGUMPULAN');
			$criteria->addSelectColumn($alias . '.NAMA');
			$criteria->addSelectColumn($alias . '.FILE');
			$criteria->addSelectColumn($alias . '.ABSTRAK');
			$criteria->addSelectColumn($alias . '.PENGGUNA_ID');
		}
	}

	/**
	 * Returns the number of rows matching criteria.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @return     int Number of matching rows.
	 */
	public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
	{
		// we may modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}

		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		$criteria->setDbName(self::DATABASE_NAME); // Set the correct dbName

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
		// BasePeer returns a PDOStatement
		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}
	/**
	 * Method to select one object from the DB.
	 *
	 * @param      Criteria $criteria object used to create the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     Tugas
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
	{
		$critcopy = clone $criteria;
		$critcopy->setLimit(1);
		$objects = TugasPeer::doSelect($critcopy, $con);
		if ($objects) {
			return $objects[0];
		}
		return null;
	}
	/**
	 * Method to do selects.
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con
	 * @return     array Array of selected Objects
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelect(Criteria $criteria, PropelPDO $con = null)
	{
		return TugasPeer::populateObjects(TugasPeer::doSelectStmt($criteria, $con));
	}
	/**
	 * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
	 *
	 * Use this method directly if you want to work with an executed statement durirectly (for example
	 * to perform your own object hydration).
	 *
	 * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
	 * @param      PropelPDO $con The connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @return     PDOStatement The executed PDOStatement object.
	 * @see        BasePeer::doSelect()
	 */
	public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		if (!$criteria->hasSelectClause()) {
			$criteria = clone $criteria;
			TugasPeer::addSelectColumns($criteria);
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		// BasePeer returns a PDOStatement
		return BasePeer::doSelect($criteria, $con);
	}
	/**
	 * Adds an object to the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doSelect*()
	 * methods in your stub classes -- you may need to explicitly add objects
	 * to the cache in order to ensure that the same objects are always returned by doSelect*()
	 * and retrieveByPK*() calls.
	 *
	 * @param      Tugas $value A Tugas object.
	 * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
	 */
	public static function addInstanceToPool(Tugas $obj, $key = null)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if ($key === null) {
				$key = (string) $obj->getTugasId();
			} // if key === null
			self::$instances[$key] = $obj;
		}
	}

	/**
	 * Removes an object from the instance pool.
	 *
	 * Propel keeps cached copies of objects in an instance pool when they are retrieved
	 * from the database.  In some cases -- especially when you override doDelete
	 * methods in your stub classes -- you may need to explicitly remove objects
	 * from the cache in order to prevent returning objects that no longer exist.
	 *
	 * @param      mixed $value A Tugas object or a primary key value.
	 */
	public static function removeInstanceFromPool($value)
	{
		if (Propel::isInstancePoolingEnabled() && $value !== null) {
			if (is_object($value) && $value instanceof Tugas) {
				$key = (string) $value->getTugasId();
			} elseif (is_scalar($value)) {
				// assume we've been passed a primary key
				$key = (string) $value;
			} else {
				$e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Tugas object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
				throw $e;
			}

			unset(self::$instances[$key]);
		}
	} // removeInstanceFromPool()

	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
	 * @return     Tugas Found object or NULL if 1) no instance exists for specified key or 2) instance pooling has been disabled.
	 * @see        getPrimaryKeyHash()
	 */
	public static function getInstanceFromPool($key)
	{
		if (Propel::isInstancePoolingEnabled()) {
			if (isset(self::$instances[$key])) {
				return self::$instances[$key];
			}
		}
		return null; // just to be explicit
	}
	
	/**
	 * Clear the instance pool.
	 *
	 * @return     void
	 */
	public static function clearInstancePool()
	{
		self::$instances = array();
	}
	
	/**
	 * Method to invalidate the instance pool of all tables related to tugas
	 * by a foreign key with ON DELETE CASCADE
	 */
	public static function clearRelatedInstancePool()
	{
	}

	/**
	 * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
	 *
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, a serialize()d version of the primary key will be returned.
	 *
	 * @param      array $row PropelPDO resultset row.
	 * @param      int $startcol The 0-based offset for reading from the resultset row.
	 * @return     string A string version of PK or NULL if the components of primary key in result array are all null.
	 */
	public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
	{
		// If the PK cannot be derived from the row, return NULL.
		if ($row[$startcol] === null) {
			return null;
		}
		return (string) $row[$startcol];
	}

	/**
	 * Retrieves the primary key from the DB resultset row 
	 * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
	 * a multi-column primary key, an array of the primary key columns will be returned.
	 *
	 * @param      array $row PropelPDO resultset row.
	 * @param      int $startcol The 0-based offset for reading from the resultset row.
	 * @return     mixed The primary key of the row
	 */
	public static function getPrimaryKeyFromRow($row, $startcol = 0)
	{
		return (int) $row[$startcol];
	}
	
	/**
	 * The returned array will contain objects of the default type or
	 * objects that inherit from the default.
	 *
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function populateObjects(PDOStatement $stmt)
	{
		$results = array();
	
		// set the class once to avoid overhead in the loop
		$cls = TugasPeer::getOMClass(false);
		// populate the object(s)
		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj = TugasPeer::getInstanceFromPool($key))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj->hydrate($row, 0, true); // rehydrate
				$results[] = $obj;
			} else {
				$obj = new $cls();
				$obj->hydrate($row);
				$results[] = $obj;
				TugasPeer::addInstanceToPool($obj, $key);
			} // if key exists
		}
		$stmt->closeCursor();
		return $results;
	}
	/**
	 * Populates an object of the default type or an object that inherit from the default.
	 *
	 * @param      array $row PropelPDO resultset row.
	 * @param      int $startcol The 0-based offset for reading from the resultset row.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 * @return     array (Tugas object, last column rank)
	 */
	public static function populateObject($row, $startcol = 0)
	{
		$key = TugasPeer::getPrimaryKeyHashFromRow($row, $startcol);
		if (null !== ($obj = TugasPeer::getInstanceFromPool($key))) {
			// We no longer rehydrate the object, since this can cause data loss.
			// See http://www.propelorm.org/ticket/509
			// $obj->hydrate($row, $startcol, true); // rehydrate
			$col = $startcol + TugasPeer::NUM_COLUMNS;
		} else {
			$cls = TugasPeer::OM_CLASS;
			$obj = new $cls();
			$col = $obj->hydrate($row, $startcol);
			TugasPeer::addInstanceToPool($obj, $key);
		}
		return array($obj, $col);
	}

	/**
	 * Returns the number of rows matching criteria, joining the related JenisTugas table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinJenisTugas(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related MataPelajaran table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinMataPelajaran(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Siswa table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinSiswa(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Pengguna table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinPengguna(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with their JenisTugas objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinJenisTugas(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);
		JenisTugasPeer::addSelectColumns($criteria);

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = JenisTugasPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = JenisTugasPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = JenisTugasPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					JenisTugasPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (Tugas) to $obj2 (JenisTugas)
				$obj2->addTugas($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with their MataPelajaran objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinMataPelajaran(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);
		MataPelajaranPeer::addSelectColumns($criteria);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = MataPelajaranPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = MataPelajaranPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					MataPelajaranPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (Tugas) to $obj2 (MataPelajaran)
				$obj2->addTugas($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with their Siswa objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinSiswa(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);
		SiswaPeer::addSelectColumns($criteria);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = SiswaPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = SiswaPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = SiswaPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					SiswaPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (Tugas) to $obj2 (Siswa)
				$obj2->addTugas($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with their Pengguna objects.
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinPengguna(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);
		PenggunaPeer::addSelectColumns($criteria);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {

				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if $obj1 already loaded

			$key2 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol);
			if ($key2 !== null) {
				$obj2 = PenggunaPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = PenggunaPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol);
					PenggunaPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 already loaded

				// Add the $obj1 (Tugas) to $obj2 (Pengguna)
				$obj2->addTugas($obj1);

			} // if joined row was not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining all related tables
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);

		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}

	/**
	 * Selects a collection of Tugas objects pre-filled with all related objects.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol2 = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);

		JenisTugasPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (JenisTugasPeer::NUM_COLUMNS - JenisTugasPeer::NUM_LAZY_LOAD_COLUMNS);

		MataPelajaranPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (MataPelajaranPeer::NUM_COLUMNS - MataPelajaranPeer::NUM_LAZY_LOAD_COLUMNS);

		SiswaPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (SiswaPeer::NUM_COLUMNS - SiswaPeer::NUM_LAZY_LOAD_COLUMNS);

		PenggunaPeer::addSelectColumns($criteria);
		$startcol6 = $startcol5 + (PenggunaPeer::NUM_COLUMNS - PenggunaPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

			// Add objects for joined JenisTugas rows

			$key2 = JenisTugasPeer::getPrimaryKeyHashFromRow($row, $startcol2);
			if ($key2 !== null) {
				$obj2 = JenisTugasPeer::getInstanceFromPool($key2);
				if (!$obj2) {

					$cls = JenisTugasPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					JenisTugasPeer::addInstanceToPool($obj2, $key2);
				} // if obj2 loaded

				// Add the $obj1 (Tugas) to the collection in $obj2 (JenisTugas)
				$obj2->addTugas($obj1);
			} // if joined row not null

			// Add objects for joined MataPelajaran rows

			$key3 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol3);
			if ($key3 !== null) {
				$obj3 = MataPelajaranPeer::getInstanceFromPool($key3);
				if (!$obj3) {

					$cls = MataPelajaranPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					MataPelajaranPeer::addInstanceToPool($obj3, $key3);
				} // if obj3 loaded

				// Add the $obj1 (Tugas) to the collection in $obj3 (MataPelajaran)
				$obj3->addTugas($obj1);
			} // if joined row not null

			// Add objects for joined Siswa rows

			$key4 = SiswaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
			if ($key4 !== null) {
				$obj4 = SiswaPeer::getInstanceFromPool($key4);
				if (!$obj4) {

					$cls = SiswaPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					SiswaPeer::addInstanceToPool($obj4, $key4);
				} // if obj4 loaded

				// Add the $obj1 (Tugas) to the collection in $obj4 (Siswa)
				$obj4->addTugas($obj1);
			} // if joined row not null

			// Add objects for joined Pengguna rows

			$key5 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol5);
			if ($key5 !== null) {
				$obj5 = PenggunaPeer::getInstanceFromPool($key5);
				if (!$obj5) {

					$cls = PenggunaPeer::getOMClass(false);

					$obj5 = new $cls();
					$obj5->hydrate($row, $startcol5);
					PenggunaPeer::addInstanceToPool($obj5, $key5);
				} // if obj5 loaded

				// Add the $obj1 (Tugas) to the collection in $obj5 (Pengguna)
				$obj5->addTugas($obj1);
			} // if joined row not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related JenisTugas table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptJenisTugas(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related MataPelajaran table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptMataPelajaran(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Siswa table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptSiswa(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Returns the number of rows matching criteria, joining the related Pengguna table
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     int Number of matching rows.
	 */
	public static function doCountJoinAllExceptPengguna(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		// we're going to modify criteria, so copy it first
		$criteria = clone $criteria;

		// We need to set the primary table name, since in the case that there are no WHERE columns
		// it will be impossible for the BasePeer::createSelectSql() method to determine which
		// tables go into the FROM clause.
		$criteria->setPrimaryTableName(TugasPeer::TABLE_NAME);
		
		if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
			$criteria->setDistinct();
		}

		if (!$criteria->hasSelectClause()) {
			TugasPeer::addSelectColumns($criteria);
		}
		
		$criteria->clearOrderByColumns(); // ORDER BY should not affect count
		
		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}
	
		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$stmt = BasePeer::doCount($criteria, $con);

		if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$count = (int) $row[0];
		} else {
			$count = 0; // no rows returned; we infer that means 0 matches.
		}
		$stmt->closeCursor();
		return $count;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with all related objects except JenisTugas.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptJenisTugas(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol2 = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);

		MataPelajaranPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (MataPelajaranPeer::NUM_COLUMNS - MataPelajaranPeer::NUM_LAZY_LOAD_COLUMNS);

		SiswaPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (SiswaPeer::NUM_COLUMNS - SiswaPeer::NUM_LAZY_LOAD_COLUMNS);

		PenggunaPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (PenggunaPeer::NUM_COLUMNS - PenggunaPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined MataPelajaran rows

				$key2 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = MataPelajaranPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = MataPelajaranPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					MataPelajaranPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj2 (MataPelajaran)
				$obj2->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Siswa rows

				$key3 = SiswaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = SiswaPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = SiswaPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					SiswaPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj3 (Siswa)
				$obj3->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Pengguna rows

				$key4 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
				if ($key4 !== null) {
					$obj4 = PenggunaPeer::getInstanceFromPool($key4);
					if (!$obj4) {
	
						$cls = PenggunaPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					PenggunaPeer::addInstanceToPool($obj4, $key4);
				} // if $obj4 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj4 (Pengguna)
				$obj4->addTugas($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with all related objects except MataPelajaran.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptMataPelajaran(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol2 = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);

		JenisTugasPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (JenisTugasPeer::NUM_COLUMNS - JenisTugasPeer::NUM_LAZY_LOAD_COLUMNS);

		SiswaPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (SiswaPeer::NUM_COLUMNS - SiswaPeer::NUM_LAZY_LOAD_COLUMNS);

		PenggunaPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (PenggunaPeer::NUM_COLUMNS - PenggunaPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined JenisTugas rows

				$key2 = JenisTugasPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = JenisTugasPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = JenisTugasPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					JenisTugasPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj2 (JenisTugas)
				$obj2->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Siswa rows

				$key3 = SiswaPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = SiswaPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = SiswaPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					SiswaPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj3 (Siswa)
				$obj3->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Pengguna rows

				$key4 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
				if ($key4 !== null) {
					$obj4 = PenggunaPeer::getInstanceFromPool($key4);
					if (!$obj4) {
	
						$cls = PenggunaPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					PenggunaPeer::addInstanceToPool($obj4, $key4);
				} // if $obj4 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj4 (Pengguna)
				$obj4->addTugas($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with all related objects except Siswa.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptSiswa(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol2 = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);

		JenisTugasPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (JenisTugasPeer::NUM_COLUMNS - JenisTugasPeer::NUM_LAZY_LOAD_COLUMNS);

		MataPelajaranPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (MataPelajaranPeer::NUM_COLUMNS - MataPelajaranPeer::NUM_LAZY_LOAD_COLUMNS);

		PenggunaPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (PenggunaPeer::NUM_COLUMNS - PenggunaPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::PENGGUNA_ID, PenggunaPeer::PENGGUNA_ID, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined JenisTugas rows

				$key2 = JenisTugasPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = JenisTugasPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = JenisTugasPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					JenisTugasPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj2 (JenisTugas)
				$obj2->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined MataPelajaran rows

				$key3 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = MataPelajaranPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = MataPelajaranPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					MataPelajaranPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj3 (MataPelajaran)
				$obj3->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Pengguna rows

				$key4 = PenggunaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
				if ($key4 !== null) {
					$obj4 = PenggunaPeer::getInstanceFromPool($key4);
					if (!$obj4) {
	
						$cls = PenggunaPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					PenggunaPeer::addInstanceToPool($obj4, $key4);
				} // if $obj4 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj4 (Pengguna)
				$obj4->addTugas($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}


	/**
	 * Selects a collection of Tugas objects pre-filled with all related objects except Pengguna.
	 *
	 * @param      Criteria  $criteria
	 * @param      PropelPDO $con
	 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
	 * @return     array Array of Tugas objects.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doSelectJoinAllExceptPengguna(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$criteria = clone $criteria;

		// Set the correct dbName if it has not been overridden
		// $criteria->getDbName() will return the same object if not set to another value
		// so == check is okay and faster
		if ($criteria->getDbName() == Propel::getDefaultDB()) {
			$criteria->setDbName(self::DATABASE_NAME);
		}

		TugasPeer::addSelectColumns($criteria);
		$startcol2 = (TugasPeer::NUM_COLUMNS - TugasPeer::NUM_LAZY_LOAD_COLUMNS);

		JenisTugasPeer::addSelectColumns($criteria);
		$startcol3 = $startcol2 + (JenisTugasPeer::NUM_COLUMNS - JenisTugasPeer::NUM_LAZY_LOAD_COLUMNS);

		MataPelajaranPeer::addSelectColumns($criteria);
		$startcol4 = $startcol3 + (MataPelajaranPeer::NUM_COLUMNS - MataPelajaranPeer::NUM_LAZY_LOAD_COLUMNS);

		SiswaPeer::addSelectColumns($criteria);
		$startcol5 = $startcol4 + (SiswaPeer::NUM_COLUMNS - SiswaPeer::NUM_LAZY_LOAD_COLUMNS);

		$criteria->addJoin(TugasPeer::JENIS_TUGAS_ID, JenisTugasPeer::JENIS_TUGAS_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::MATA_PELAJARAN_ID, MataPelajaranPeer::MATA_PELAJARAN_ID, $join_behavior);

		$criteria->addJoin(TugasPeer::NIS, SiswaPeer::NIS, $join_behavior);


		$stmt = BasePeer::doSelect($criteria, $con);
		$results = array();

		while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
			$key1 = TugasPeer::getPrimaryKeyHashFromRow($row, 0);
			if (null !== ($obj1 = TugasPeer::getInstanceFromPool($key1))) {
				// We no longer rehydrate the object, since this can cause data loss.
				// See http://www.propelorm.org/ticket/509
				// $obj1->hydrate($row, 0, true); // rehydrate
			} else {
				$cls = TugasPeer::getOMClass(false);

				$obj1 = new $cls();
				$obj1->hydrate($row);
				TugasPeer::addInstanceToPool($obj1, $key1);
			} // if obj1 already loaded

				// Add objects for joined JenisTugas rows

				$key2 = JenisTugasPeer::getPrimaryKeyHashFromRow($row, $startcol2);
				if ($key2 !== null) {
					$obj2 = JenisTugasPeer::getInstanceFromPool($key2);
					if (!$obj2) {
	
						$cls = JenisTugasPeer::getOMClass(false);

					$obj2 = new $cls();
					$obj2->hydrate($row, $startcol2);
					JenisTugasPeer::addInstanceToPool($obj2, $key2);
				} // if $obj2 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj2 (JenisTugas)
				$obj2->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined MataPelajaran rows

				$key3 = MataPelajaranPeer::getPrimaryKeyHashFromRow($row, $startcol3);
				if ($key3 !== null) {
					$obj3 = MataPelajaranPeer::getInstanceFromPool($key3);
					if (!$obj3) {
	
						$cls = MataPelajaranPeer::getOMClass(false);

					$obj3 = new $cls();
					$obj3->hydrate($row, $startcol3);
					MataPelajaranPeer::addInstanceToPool($obj3, $key3);
				} // if $obj3 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj3 (MataPelajaran)
				$obj3->addTugas($obj1);

			} // if joined row is not null

				// Add objects for joined Siswa rows

				$key4 = SiswaPeer::getPrimaryKeyHashFromRow($row, $startcol4);
				if ($key4 !== null) {
					$obj4 = SiswaPeer::getInstanceFromPool($key4);
					if (!$obj4) {
	
						$cls = SiswaPeer::getOMClass(false);

					$obj4 = new $cls();
					$obj4->hydrate($row, $startcol4);
					SiswaPeer::addInstanceToPool($obj4, $key4);
				} // if $obj4 already loaded

				// Add the $obj1 (Tugas) to the collection in $obj4 (Siswa)
				$obj4->addTugas($obj1);

			} // if joined row is not null

			$results[] = $obj1;
		}
		$stmt->closeCursor();
		return $results;
	}

	/**
	 * Returns the TableMap related to this peer.
	 * This method is not needed for general use but a specific application could have a need.
	 * @return     TableMap
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function getTableMap()
	{
		return Propel::getDatabaseMap(self::DATABASE_NAME)->getTable(self::TABLE_NAME);
	}

	/**
	 * Add a TableMap instance to the database for this peer class.
	 */
	public static function buildTableMap()
	{
	  $dbMap = Propel::getDatabaseMap(BaseTugasPeer::DATABASE_NAME);
	  if (!$dbMap->hasTable(BaseTugasPeer::TABLE_NAME))
	  {
	    $dbMap->addTableObject(new TugasTableMap());
	  }
	}

	/**
	 * The class that the Peer will make instances of.
	 *
	 * If $withPrefix is true, the returned path
	 * uses a dot-path notation which is tranalted into a path
	 * relative to a location on the PHP include_path.
	 * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
	 *
	 * @param      boolean $withPrefix Whether or not to return the path with the class name
	 * @return     string path.to.ClassName
	 */
	public static function getOMClass($withPrefix = true)
	{
		return $withPrefix ? TugasPeer::CLASS_DEFAULT : TugasPeer::OM_CLASS;
	}

	/**
	 * Method perform an INSERT on the database, given a Tugas or Criteria object.
	 *
	 * @param      mixed $values Criteria or Tugas object containing data that is used to create the INSERT statement.
	 * @param      PropelPDO $con the PropelPDO connection to use
	 * @return     mixed The new primary key.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doInsert($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity
		} else {
			$criteria = $values->buildCriteria(); // build Criteria from Tugas object
		}

		if ($criteria->containsKey(TugasPeer::TUGAS_ID) && $criteria->keyContainsValue(TugasPeer::TUGAS_ID) ) {
			throw new PropelException('Cannot insert a value for auto-increment primary key ('.TugasPeer::TUGAS_ID.')');
		}


		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		try {
			// use transaction because $criteria could contain info
			// for more than one table (I guess, conceivably)
			$con->beginTransaction();
			$pk = BasePeer::doInsert($criteria, $con);
			$con->commit();
		} catch(PropelException $e) {
			$con->rollBack();
			throw $e;
		}

		return $pk;
	}

	/**
	 * Method perform an UPDATE on the database, given a Tugas or Criteria object.
	 *
	 * @param      mixed $values Criteria or Tugas object containing data that is used to create the UPDATE statement.
	 * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function doUpdate($values, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$selectCriteria = new Criteria(self::DATABASE_NAME);

		if ($values instanceof Criteria) {
			$criteria = clone $values; // rename for clarity

			$comparison = $criteria->getComparison(TugasPeer::TUGAS_ID);
			$value = $criteria->remove(TugasPeer::TUGAS_ID);
			if ($value) {
				$selectCriteria->add(TugasPeer::TUGAS_ID, $value, $comparison);
			} else {
				$selectCriteria->setPrimaryTableName(TugasPeer::TABLE_NAME);
			}

		} else { // $values is Tugas object
			$criteria = $values->buildCriteria(); // gets full criteria
			$selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
		}

		// set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		return BasePeer::doUpdate($selectCriteria, $criteria, $con);
	}

	/**
	 * Method to DELETE all rows from the tugas table.
	 *
	 * @return     int The number of affected rows (if supported by underlying database driver).
	 */
	public static function doDeleteAll($con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}
		$affectedRows = 0; // initialize var to track total num of affected rows
		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			$affectedRows += BasePeer::doDeleteAll(TugasPeer::TABLE_NAME, $con, TugasPeer::DATABASE_NAME);
			// Because this db requires some delete cascade/set null emulation, we have to
			// clear the cached instance *after* the emulation has happened (since
			// instances get re-added by the select statement contained therein).
			TugasPeer::clearInstancePool();
			TugasPeer::clearRelatedInstancePool();
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Method perform a DELETE on the database, given a Tugas or Criteria object OR a primary key value.
	 *
	 * @param      mixed $values Criteria or Tugas object or primary key or array of primary keys
	 *              which is used to create the DELETE statement
	 * @param      PropelPDO $con the connection to use
	 * @return     int 	The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
	 *				if supported by native driver or if emulated using Propel.
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	 public static function doDelete($values, PropelPDO $con = null)
	 {
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		if ($values instanceof Criteria) {
			// invalidate the cache for all objects of this type, since we have no
			// way of knowing (without running a query) what objects should be invalidated
			// from the cache based on this Criteria.
			TugasPeer::clearInstancePool();
			// rename for clarity
			$criteria = clone $values;
		} elseif ($values instanceof Tugas) { // it's a model object
			// invalidate the cache for this single object
			TugasPeer::removeInstanceFromPool($values);
			// create criteria based on pk values
			$criteria = $values->buildPkeyCriteria();
		} else { // it's a primary key, or an array of pks
			$criteria = new Criteria(self::DATABASE_NAME);
			$criteria->add(TugasPeer::TUGAS_ID, (array) $values, Criteria::IN);
			// invalidate the cache for this object(s)
			foreach ((array) $values as $singleval) {
				TugasPeer::removeInstanceFromPool($singleval);
			}
		}

		// Set the correct dbName
		$criteria->setDbName(self::DATABASE_NAME);

		$affectedRows = 0; // initialize var to track total num of affected rows

		try {
			// use transaction because $criteria could contain info
			// for more than one table or we could emulating ON DELETE CASCADE, etc.
			$con->beginTransaction();
			
			$affectedRows += BasePeer::doDelete($criteria, $con);
			TugasPeer::clearRelatedInstancePool();
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Validates all modified columns of given Tugas object.
	 * If parameter $columns is either a single column name or an array of column names
	 * than only those columns are validated.
	 *
	 * NOTICE: This does not apply to primary or foreign keys for now.
	 *
	 * @param      Tugas $obj The object to validate.
	 * @param      mixed $cols Column name or array of column names.
	 *
	 * @return     mixed TRUE if all columns are valid or the error message of the first invalid column.
	 */
	public static function doValidate(Tugas $obj, $cols = null)
	{
		$columns = array();

		if ($cols) {
			$dbMap = Propel::getDatabaseMap(TugasPeer::DATABASE_NAME);
			$tableMap = $dbMap->getTable(TugasPeer::TABLE_NAME);

			if (! is_array($cols)) {
				$cols = array($cols);
			}

			foreach ($cols as $colName) {
				if ($tableMap->containsColumn($colName)) {
					$get = 'get' . $tableMap->getColumn($colName)->getPhpName();
					$columns[$colName] = $obj->$get();
				}
			}
		} else {

		}

		return BasePeer::doValidate(TugasPeer::DATABASE_NAME, TugasPeer::TABLE_NAME, $columns);
	}

	/**
	 * Retrieve a single object by pkey.
	 *
	 * @param      int $pk the primary key.
	 * @param      PropelPDO $con the connection to use
	 * @return     Tugas
	 */
	public static function retrieveByPK($pk, PropelPDO $con = null)
	{

		if (null !== ($obj = TugasPeer::getInstanceFromPool((string) $pk))) {
			return $obj;
		}

		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$criteria = new Criteria(TugasPeer::DATABASE_NAME);
		$criteria->add(TugasPeer::TUGAS_ID, $pk);

		$v = TugasPeer::doSelect($criteria, $con);

		return !empty($v) > 0 ? $v[0] : null;
	}

	/**
	 * Retrieve multiple objects by pkey.
	 *
	 * @param      array $pks List of primary keys
	 * @param      PropelPDO $con the connection to use
	 * @throws     PropelException Any exceptions caught during processing will be
	 *		 rethrown wrapped into a PropelException.
	 */
	public static function retrieveByPKs($pks, PropelPDO $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(TugasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		$objs = null;
		if (empty($pks)) {
			$objs = array();
		} else {
			$criteria = new Criteria(TugasPeer::DATABASE_NAME);
			$criteria->add(TugasPeer::TUGAS_ID, $pks, Criteria::IN);
			$objs = TugasPeer::doSelect($criteria, $con);
		}
		return $objs;
	}

} // BaseTugasPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseTugasPeer::buildTableMap();

