<?php


/**
 * Base class that represents a query for the 'kompetensi_keahlian' table.
 *
 * 
 *
 * @method     KompetensiKeahlianQuery orderByKompetensiKeahlianId($order = Criteria::ASC) Order by the kompetensi_keahlian_id column
 * @method     KompetensiKeahlianQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     KompetensiKeahlianQuery orderByDeskripsi($order = Criteria::ASC) Order by the deskripsi column
 *
 * @method     KompetensiKeahlianQuery groupByKompetensiKeahlianId() Group by the kompetensi_keahlian_id column
 * @method     KompetensiKeahlianQuery groupByNama() Group by the nama column
 * @method     KompetensiKeahlianQuery groupByDeskripsi() Group by the deskripsi column
 *
 * @method     KompetensiKeahlianQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     KompetensiKeahlianQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     KompetensiKeahlianQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     KompetensiKeahlianQuery leftJoinMataPelajaran($relationAlias = null) Adds a LEFT JOIN clause to the query using the MataPelajaran relation
 * @method     KompetensiKeahlianQuery rightJoinMataPelajaran($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MataPelajaran relation
 * @method     KompetensiKeahlianQuery innerJoinMataPelajaran($relationAlias = null) Adds a INNER JOIN clause to the query using the MataPelajaran relation
 *
 * @method     KompetensiKeahlian findOne(PropelPDO $con = null) Return the first KompetensiKeahlian matching the query
 * @method     KompetensiKeahlian findOneOrCreate(PropelPDO $con = null) Return the first KompetensiKeahlian matching the query, or a new KompetensiKeahlian object populated from the query conditions when no match is found
 *
 * @method     KompetensiKeahlian findOneByKompetensiKeahlianId(int $kompetensi_keahlian_id) Return the first KompetensiKeahlian filtered by the kompetensi_keahlian_id column
 * @method     KompetensiKeahlian findOneByNama(string $nama) Return the first KompetensiKeahlian filtered by the nama column
 * @method     KompetensiKeahlian findOneByDeskripsi(string $deskripsi) Return the first KompetensiKeahlian filtered by the deskripsi column
 *
 * @method     array findByKompetensiKeahlianId(int $kompetensi_keahlian_id) Return KompetensiKeahlian objects filtered by the kompetensi_keahlian_id column
 * @method     array findByNama(string $nama) Return KompetensiKeahlian objects filtered by the nama column
 * @method     array findByDeskripsi(string $deskripsi) Return KompetensiKeahlian objects filtered by the deskripsi column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseKompetensiKeahlianQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseKompetensiKeahlianQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'KompetensiKeahlian', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new KompetensiKeahlianQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    KompetensiKeahlianQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof KompetensiKeahlianQuery) {
			return $criteria;
		}
		$query = new KompetensiKeahlianQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    KompetensiKeahlian|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = KompetensiKeahlianPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(KompetensiKeahlianPeer::KOMPETENSI_KEAHLIAN_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(KompetensiKeahlianPeer::KOMPETENSI_KEAHLIAN_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the kompetensi_keahlian_id column
	 * 
	 * @param     int|array $kompetensiKeahlianId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByKompetensiKeahlianId($kompetensiKeahlianId = null, $comparison = null)
	{
		if (is_array($kompetensiKeahlianId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(KompetensiKeahlianPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlianId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(KompetensiKeahlianPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the deskripsi column
	 * 
	 * @param     string $deskripsi The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByDeskripsi($deskripsi = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($deskripsi)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $deskripsi)) {
				$deskripsi = str_replace('*', '%', $deskripsi);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(KompetensiKeahlianPeer::DESKRIPSI, $deskripsi, $comparison);
	}

	/**
	 * Filter the query by a related MataPelajaran object
	 *
	 * @param     MataPelajaran $mataPelajaran  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function filterByMataPelajaran($mataPelajaran, $comparison = null)
	{
		return $this
			->addUsingAlias(KompetensiKeahlianPeer::KOMPETENSI_KEAHLIAN_ID, $mataPelajaran->getKompetensiKeahlianId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the MataPelajaran relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function joinMataPelajaran($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('MataPelajaran');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'MataPelajaran');
		}
		
		return $this;
	}

	/**
	 * Use the MataPelajaran relation MataPelajaran object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    MataPelajaranQuery A secondary query class using the current class as primary query
	 */
	public function useMataPelajaranQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinMataPelajaran($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'MataPelajaran', 'MataPelajaranQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     KompetensiKeahlian $kompetensiKeahlian Object to remove from the list of results
	 *
	 * @return    KompetensiKeahlianQuery The current query, for fluid interface
	 */
	public function prune($kompetensiKeahlian = null)
	{
		if ($kompetensiKeahlian) {
			$this->addUsingAlias(KompetensiKeahlianPeer::KOMPETENSI_KEAHLIAN_ID, $kompetensiKeahlian->getKompetensiKeahlianId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseKompetensiKeahlianQuery
