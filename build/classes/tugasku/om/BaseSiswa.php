<?php


/**
 * Base class that represents a row from the 'siswa' table.
 *
 * 
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseSiswa extends BaseObject  implements Persistent
{

	/**
	 * Peer class name
	 */
	const PEER = 'SiswaPeer';

	/**
	 * The Peer class.
	 * Instance provides a convenient way of calling static methods on a class
	 * that calling code may not be able to identify.
	 * @var        SiswaPeer
	 */
	protected static $peer;

	/**
	 * The value for the siswa_id field.
	 * @var        int
	 */
	protected $siswa_id;

	/**
	 * The value for the nis field.
	 * @var        string
	 */
	protected $nis;

	/**
	 * The value for the nama field.
	 * @var        string
	 */
	protected $nama;

	/**
	 * The value for the jenis_kelamin field.
	 * @var        string
	 */
	protected $jenis_kelamin;

	/**
	 * The value for the tempat_lahir field.
	 * @var        string
	 */
	protected $tempat_lahir;

	/**
	 * The value for the tanggal_lahir field.
	 * @var        string
	 */
	protected $tanggal_lahir;

	/**
	 * The value for the telepon field.
	 * @var        string
	 */
	protected $telepon;

	/**
	 * The value for the kelas field.
	 * @var        string
	 */
	protected $kelas;

	/**
	 * The value for the foto field.
	 * @var        string
	 */
	protected $foto;

	/**
	 * @var        array Tugas[] Collection to store aggregation of Tugas objects.
	 */
	protected $collTugass;

	/**
	 * Flag to prevent endless save loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInSave = false;

	/**
	 * Flag to prevent endless validation loop, if this object is referenced
	 * by another object which falls in this transaction.
	 * @var        boolean
	 */
	protected $alreadyInValidation = false;

	/**
	 * Get the [siswa_id] column value.
	 * 
	 * @return     int
	 */
	public function getSiswaId()
	{
		return $this->siswa_id;
	}

	/**
	 * Get the [nis] column value.
	 * 
	 * @return     string
	 */
	public function getNis()
	{
		return $this->nis;
	}

	/**
	 * Get the [nama] column value.
	 * 
	 * @return     string
	 */
	public function getNama()
	{
		return $this->nama;
	}

	/**
	 * Get the [jenis_kelamin] column value.
	 * 
	 * @return     string
	 */
	public function getJenisKelamin()
	{
		return $this->jenis_kelamin;
	}

	/**
	 * Get the [tempat_lahir] column value.
	 * 
	 * @return     string
	 */
	public function getTempatLahir()
	{
		return $this->tempat_lahir;
	}

	/**
	 * Get the [tanggal_lahir] column value.
	 * 
	 * @return     string
	 */
	public function getTanggalLahir()
	{
		return $this->tanggal_lahir;
	}

	/**
	 * Get the [telepon] column value.
	 * 
	 * @return     string
	 */
	public function getTelepon()
	{
		return $this->telepon;
	}

	/**
	 * Get the [kelas] column value.
	 * 
	 * @return     string
	 */
	public function getKelas()
	{
		return $this->kelas;
	}

	/**
	 * Get the [foto] column value.
	 * 
	 * @return     string
	 */
	public function getFoto()
	{
		return $this->foto;
	}

	/**
	 * Set the value of [siswa_id] column.
	 * 
	 * @param      int $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setSiswaId($v)
	{
		if ($v !== null) {
			$v = (int) $v;
		}

		if ($this->siswa_id !== $v) {
			$this->siswa_id = $v;
			$this->modifiedColumns[] = SiswaPeer::SISWA_ID;
		}

		return $this;
	} // setSiswaId()

	/**
	 * Set the value of [nis] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setNis($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nis !== $v) {
			$this->nis = $v;
			$this->modifiedColumns[] = SiswaPeer::NIS;
		}

		return $this;
	} // setNis()

	/**
	 * Set the value of [nama] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setNama($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->nama !== $v) {
			$this->nama = $v;
			$this->modifiedColumns[] = SiswaPeer::NAMA;
		}

		return $this;
	} // setNama()

	/**
	 * Set the value of [jenis_kelamin] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setJenisKelamin($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->jenis_kelamin !== $v) {
			$this->jenis_kelamin = $v;
			$this->modifiedColumns[] = SiswaPeer::JENIS_KELAMIN;
		}

		return $this;
	} // setJenisKelamin()

	/**
	 * Set the value of [tempat_lahir] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setTempatLahir($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tempat_lahir !== $v) {
			$this->tempat_lahir = $v;
			$this->modifiedColumns[] = SiswaPeer::TEMPAT_LAHIR;
		}

		return $this;
	} // setTempatLahir()

	/**
	 * Set the value of [tanggal_lahir] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setTanggalLahir($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->tanggal_lahir !== $v) {
			$this->tanggal_lahir = $v;
			$this->modifiedColumns[] = SiswaPeer::TANGGAL_LAHIR;
		}

		return $this;
	} // setTanggalLahir()

	/**
	 * Set the value of [telepon] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setTelepon($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->telepon !== $v) {
			$this->telepon = $v;
			$this->modifiedColumns[] = SiswaPeer::TELEPON;
		}

		return $this;
	} // setTelepon()

	/**
	 * Set the value of [kelas] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setKelas($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->kelas !== $v) {
			$this->kelas = $v;
			$this->modifiedColumns[] = SiswaPeer::KELAS;
		}

		return $this;
	} // setKelas()

	/**
	 * Set the value of [foto] column.
	 * 
	 * @param      string $v new value
	 * @return     Siswa The current object (for fluent API support)
	 */
	public function setFoto($v)
	{
		if ($v !== null) {
			$v = (string) $v;
		}

		if ($this->foto !== $v) {
			$this->foto = $v;
			$this->modifiedColumns[] = SiswaPeer::FOTO;
		}

		return $this;
	} // setFoto()

	/**
	 * Indicates whether the columns in this object are only set to default values.
	 *
	 * This method can be used in conjunction with isModified() to indicate whether an object is both
	 * modified _and_ has some values set which are non-default.
	 *
	 * @return     boolean Whether the columns in this object are only been set with default values.
	 */
	public function hasOnlyDefaultValues()
	{
		// otherwise, everything was equal, so return TRUE
		return true;
	} // hasOnlyDefaultValues()

	/**
	 * Hydrates (populates) the object variables with values from the database resultset.
	 *
	 * An offset (0-based "start column") is specified so that objects can be hydrated
	 * with a subset of the columns in the resultset rows.  This is needed, for example,
	 * for results of JOIN queries where the resultset row includes columns from two or
	 * more tables.
	 *
	 * @param      array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
	 * @param      int $startcol 0-based offset column which indicates which restultset column to start with.
	 * @param      boolean $rehydrate Whether this object is being re-hydrated from the database.
	 * @return     int next starting column
	 * @throws     PropelException  - Any caught Exception will be rewrapped as a PropelException.
	 */
	public function hydrate($row, $startcol = 0, $rehydrate = false)
	{
		try {

			$this->siswa_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
			$this->nis = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
			$this->nama = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
			$this->jenis_kelamin = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
			$this->tempat_lahir = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
			$this->tanggal_lahir = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
			$this->telepon = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
			$this->kelas = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
			$this->foto = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
			$this->resetModified();

			$this->setNew(false);

			if ($rehydrate) {
				$this->ensureConsistency();
			}

			return $startcol + 9; // 9 = SiswaPeer::NUM_COLUMNS - SiswaPeer::NUM_LAZY_LOAD_COLUMNS).

		} catch (Exception $e) {
			throw new PropelException("Error populating Siswa object", $e);
		}
	}

	/**
	 * Checks and repairs the internal consistency of the object.
	 *
	 * This method is executed after an already-instantiated object is re-hydrated
	 * from the database.  It exists to check any foreign keys to make sure that
	 * the objects related to the current object are correct based on foreign key.
	 *
	 * You can override this method in the stub class, but you should always invoke
	 * the base method from the overridden method (i.e. parent::ensureConsistency()),
	 * in case your model changes.
	 *
	 * @throws     PropelException
	 */
	public function ensureConsistency()
	{

	} // ensureConsistency

	/**
	 * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
	 *
	 * This will only work if the object has been saved and has a valid primary key set.
	 *
	 * @param      boolean $deep (optional) Whether to also de-associated any related objects.
	 * @param      PropelPDO $con (optional) The PropelPDO connection to use.
	 * @return     void
	 * @throws     PropelException - if this object is deleted, unsaved or doesn't have pk match in db
	 */
	public function reload($deep = false, PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("Cannot reload a deleted object.");
		}

		if ($this->isNew()) {
			throw new PropelException("Cannot reload an unsaved object.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SiswaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
		}

		// We don't need to alter the object instance pool; we're just modifying this instance
		// already in the pool.

		$stmt = SiswaPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
		$row = $stmt->fetch(PDO::FETCH_NUM);
		$stmt->closeCursor();
		if (!$row) {
			throw new PropelException('Cannot find matching row in the database to reload object values.');
		}
		$this->hydrate($row, 0, true); // rehydrate

		if ($deep) {  // also de-associate any related objects?

			$this->collTugass = null;

		} // if (deep)
	}

	/**
	 * Removes this object from datastore and sets delete attribute.
	 *
	 * @param      PropelPDO $con
	 * @return     void
	 * @throws     PropelException
	 * @see        BaseObject::setDeleted()
	 * @see        BaseObject::isDeleted()
	 */
	public function delete(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("This object has already been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SiswaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		try {
			$ret = $this->preDelete($con);
			if ($ret) {
				SiswaQuery::create()
					->filterByPrimaryKey($this->getPrimaryKey())
					->delete($con);
				$this->postDelete($con);
				$con->commit();
				$this->setDeleted(true);
			} else {
				$con->commit();
			}
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Persists this object to the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All modified related objects will also be persisted in the doSave()
	 * method.  This method wraps all precipitate database operations in a
	 * single transaction.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        doSave()
	 */
	public function save(PropelPDO $con = null)
	{
		if ($this->isDeleted()) {
			throw new PropelException("You cannot save an object that has been deleted.");
		}

		if ($con === null) {
			$con = Propel::getConnection(SiswaPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
		}

		$con->beginTransaction();
		$isInsert = $this->isNew();
		try {
			$ret = $this->preSave($con);
			if ($isInsert) {
				$ret = $ret && $this->preInsert($con);
			} else {
				$ret = $ret && $this->preUpdate($con);
			}
			if ($ret) {
				$affectedRows = $this->doSave($con);
				if ($isInsert) {
					$this->postInsert($con);
				} else {
					$this->postUpdate($con);
				}
				$this->postSave($con);
				SiswaPeer::addInstanceToPool($this);
			} else {
				$affectedRows = 0;
			}
			$con->commit();
			return $affectedRows;
		} catch (PropelException $e) {
			$con->rollBack();
			throw $e;
		}
	}

	/**
	 * Performs the work of inserting or updating the row in the database.
	 *
	 * If the object is new, it inserts it; otherwise an update is performed.
	 * All related objects are also updated in this method.
	 *
	 * @param      PropelPDO $con
	 * @return     int The number of rows affected by this insert/update and any referring fk objects' save() operations.
	 * @throws     PropelException
	 * @see        save()
	 */
	protected function doSave(PropelPDO $con)
	{
		$affectedRows = 0; // initialize var to track total num of affected rows
		if (!$this->alreadyInSave) {
			$this->alreadyInSave = true;

			if ($this->isNew() ) {
				$this->modifiedColumns[] = SiswaPeer::SISWA_ID;
			}

			// If this object has been modified, then save it to the database.
			if ($this->isModified()) {
				if ($this->isNew()) {
					$criteria = $this->buildCriteria();
					if ($criteria->keyContainsValue(SiswaPeer::SISWA_ID) ) {
						throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiswaPeer::SISWA_ID.')');
					}

					$pk = BasePeer::doInsert($criteria, $con);
					$affectedRows = 1;
					$this->setSiswaId($pk);  //[IMV] update autoincrement primary key
					$this->setNew(false);
				} else {
					$affectedRows = SiswaPeer::doUpdate($this, $con);
				}

				$this->resetModified(); // [HL] After being saved an object is no longer 'modified'
			}

			if ($this->collTugass !== null) {
				foreach ($this->collTugass as $referrerFK) {
					if (!$referrerFK->isDeleted()) {
						$affectedRows += $referrerFK->save($con);
					}
				}
			}

			$this->alreadyInSave = false;

		}
		return $affectedRows;
	} // doSave()

	/**
	 * Array of ValidationFailed objects.
	 * @var        array ValidationFailed[]
	 */
	protected $validationFailures = array();

	/**
	 * Gets any ValidationFailed objects that resulted from last call to validate().
	 *
	 *
	 * @return     array ValidationFailed[]
	 * @see        validate()
	 */
	public function getValidationFailures()
	{
		return $this->validationFailures;
	}

	/**
	 * Validates the objects modified field values and all objects related to this table.
	 *
	 * If $columns is either a column name or an array of column names
	 * only those columns are validated.
	 *
	 * @param      mixed $columns Column name or an array of column names.
	 * @return     boolean Whether all columns pass validation.
	 * @see        doValidate()
	 * @see        getValidationFailures()
	 */
	public function validate($columns = null)
	{
		$res = $this->doValidate($columns);
		if ($res === true) {
			$this->validationFailures = array();
			return true;
		} else {
			$this->validationFailures = $res;
			return false;
		}
	}

	/**
	 * This function performs the validation work for complex object models.
	 *
	 * In addition to checking the current object, all related objects will
	 * also be validated.  If all pass then <code>true</code> is returned; otherwise
	 * an aggreagated array of ValidationFailed objects will be returned.
	 *
	 * @param      array $columns Array of column names to validate.
	 * @return     mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
	 */
	protected function doValidate($columns = null)
	{
		if (!$this->alreadyInValidation) {
			$this->alreadyInValidation = true;
			$retval = null;

			$failureMap = array();


			if (($retval = SiswaPeer::doValidate($this, $columns)) !== true) {
				$failureMap = array_merge($failureMap, $retval);
			}


				if ($this->collTugass !== null) {
					foreach ($this->collTugass as $referrerFK) {
						if (!$referrerFK->validate($columns)) {
							$failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
						}
					}
				}


			$this->alreadyInValidation = false;
		}

		return (!empty($failureMap) ? $failureMap : true);
	}

	/**
	 * Retrieves a field from the object by name passed in as a string.
	 *
	 * @param      string $name name
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     mixed Value of field.
	 */
	public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SiswaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		$field = $this->getByPosition($pos);
		return $field;
	}

	/**
	 * Retrieves a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @return     mixed Value of field at $pos
	 */
	public function getByPosition($pos)
	{
		switch($pos) {
			case 0:
				return $this->getSiswaId();
				break;
			case 1:
				return $this->getNis();
				break;
			case 2:
				return $this->getNama();
				break;
			case 3:
				return $this->getJenisKelamin();
				break;
			case 4:
				return $this->getTempatLahir();
				break;
			case 5:
				return $this->getTanggalLahir();
				break;
			case 6:
				return $this->getTelepon();
				break;
			case 7:
				return $this->getKelas();
				break;
			case 8:
				return $this->getFoto();
				break;
			default:
				return null;
				break;
		} // switch()
	}

	/**
	 * Exports the object as an array.
	 *
	 * You can specify the key type of the array by passing one of the class
	 * type constants.
	 *
	 * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 *                    Defaults to BasePeer::TYPE_PHPNAME.
	 * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
	 *
	 * @return    array an associative array containing the field names (as keys) and field values
	 */
	public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true)
	{
		$keys = SiswaPeer::getFieldNames($keyType);
		$result = array(
			$keys[0] => $this->getSiswaId(),
			$keys[1] => $this->getNis(),
			$keys[2] => $this->getNama(),
			$keys[3] => $this->getJenisKelamin(),
			$keys[4] => $this->getTempatLahir(),
			$keys[5] => $this->getTanggalLahir(),
			$keys[6] => $this->getTelepon(),
			$keys[7] => $this->getKelas(),
			$keys[8] => $this->getFoto(),
		);
		return $result;
	}

	/**
	 * Sets a field from the object by name passed in as a string.
	 *
	 * @param      string $name peer name
	 * @param      mixed $value field value
	 * @param      string $type The type of fieldname the $name is of:
	 *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
	 *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
	 * @return     void
	 */
	public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
	{
		$pos = SiswaPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
		return $this->setByPosition($pos, $value);
	}

	/**
	 * Sets a field from the object by Position as specified in the xml schema.
	 * Zero-based.
	 *
	 * @param      int $pos position in xml schema
	 * @param      mixed $value field value
	 * @return     void
	 */
	public function setByPosition($pos, $value)
	{
		switch($pos) {
			case 0:
				$this->setSiswaId($value);
				break;
			case 1:
				$this->setNis($value);
				break;
			case 2:
				$this->setNama($value);
				break;
			case 3:
				$this->setJenisKelamin($value);
				break;
			case 4:
				$this->setTempatLahir($value);
				break;
			case 5:
				$this->setTanggalLahir($value);
				break;
			case 6:
				$this->setTelepon($value);
				break;
			case 7:
				$this->setKelas($value);
				break;
			case 8:
				$this->setFoto($value);
				break;
		} // switch()
	}

	/**
	 * Populates the object using an array.
	 *
	 * This is particularly useful when populating an object from one of the
	 * request arrays (e.g. $_POST).  This method goes through the column
	 * names, checking to see whether a matching key exists in populated
	 * array. If so the setByName() method is called for that column.
	 *
	 * You can specify the key type of the array by additionally passing one
	 * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
	 * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
	 * The default key type is the column's phpname (e.g. 'AuthorId')
	 *
	 * @param      array  $arr     An array to populate the object from.
	 * @param      string $keyType The type of keys the array uses.
	 * @return     void
	 */
	public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
	{
		$keys = SiswaPeer::getFieldNames($keyType);

		if (array_key_exists($keys[0], $arr)) $this->setSiswaId($arr[$keys[0]]);
		if (array_key_exists($keys[1], $arr)) $this->setNis($arr[$keys[1]]);
		if (array_key_exists($keys[2], $arr)) $this->setNama($arr[$keys[2]]);
		if (array_key_exists($keys[3], $arr)) $this->setJenisKelamin($arr[$keys[3]]);
		if (array_key_exists($keys[4], $arr)) $this->setTempatLahir($arr[$keys[4]]);
		if (array_key_exists($keys[5], $arr)) $this->setTanggalLahir($arr[$keys[5]]);
		if (array_key_exists($keys[6], $arr)) $this->setTelepon($arr[$keys[6]]);
		if (array_key_exists($keys[7], $arr)) $this->setKelas($arr[$keys[7]]);
		if (array_key_exists($keys[8], $arr)) $this->setFoto($arr[$keys[8]]);
	}

	/**
	 * Build a Criteria object containing the values of all modified columns in this object.
	 *
	 * @return     Criteria The Criteria object containing all modified values.
	 */
	public function buildCriteria()
	{
		$criteria = new Criteria(SiswaPeer::DATABASE_NAME);

		if ($this->isColumnModified(SiswaPeer::SISWA_ID)) $criteria->add(SiswaPeer::SISWA_ID, $this->siswa_id);
		if ($this->isColumnModified(SiswaPeer::NIS)) $criteria->add(SiswaPeer::NIS, $this->nis);
		if ($this->isColumnModified(SiswaPeer::NAMA)) $criteria->add(SiswaPeer::NAMA, $this->nama);
		if ($this->isColumnModified(SiswaPeer::JENIS_KELAMIN)) $criteria->add(SiswaPeer::JENIS_KELAMIN, $this->jenis_kelamin);
		if ($this->isColumnModified(SiswaPeer::TEMPAT_LAHIR)) $criteria->add(SiswaPeer::TEMPAT_LAHIR, $this->tempat_lahir);
		if ($this->isColumnModified(SiswaPeer::TANGGAL_LAHIR)) $criteria->add(SiswaPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
		if ($this->isColumnModified(SiswaPeer::TELEPON)) $criteria->add(SiswaPeer::TELEPON, $this->telepon);
		if ($this->isColumnModified(SiswaPeer::KELAS)) $criteria->add(SiswaPeer::KELAS, $this->kelas);
		if ($this->isColumnModified(SiswaPeer::FOTO)) $criteria->add(SiswaPeer::FOTO, $this->foto);

		return $criteria;
	}

	/**
	 * Builds a Criteria object containing the primary key for this object.
	 *
	 * Unlike buildCriteria() this method includes the primary key values regardless
	 * of whether or not they have been modified.
	 *
	 * @return     Criteria The Criteria object containing value(s) for primary key(s).
	 */
	public function buildPkeyCriteria()
	{
		$criteria = new Criteria(SiswaPeer::DATABASE_NAME);
		$criteria->add(SiswaPeer::SISWA_ID, $this->siswa_id);

		return $criteria;
	}

	/**
	 * Returns the primary key for this object (row).
	 * @return     int
	 */
	public function getPrimaryKey()
	{
		return $this->getSiswaId();
	}

	/**
	 * Generic method to set the primary key (siswa_id column).
	 *
	 * @param      int $key Primary key.
	 * @return     void
	 */
	public function setPrimaryKey($key)
	{
		$this->setSiswaId($key);
	}

	/**
	 * Returns true if the primary key for this object is null.
	 * @return     boolean
	 */
	public function isPrimaryKeyNull()
	{
		return null === $this->getSiswaId();
	}

	/**
	 * Sets contents of passed object to values from current object.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      object $copyObj An object of Siswa (or compatible) type.
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @throws     PropelException
	 */
	public function copyInto($copyObj, $deepCopy = false)
	{
		$copyObj->setNis($this->nis);
		$copyObj->setNama($this->nama);
		$copyObj->setJenisKelamin($this->jenis_kelamin);
		$copyObj->setTempatLahir($this->tempat_lahir);
		$copyObj->setTanggalLahir($this->tanggal_lahir);
		$copyObj->setTelepon($this->telepon);
		$copyObj->setKelas($this->kelas);
		$copyObj->setFoto($this->foto);

		if ($deepCopy) {
			// important: temporarily setNew(false) because this affects the behavior of
			// the getter/setter methods for fkey referrer objects.
			$copyObj->setNew(false);

			foreach ($this->getTugass() as $relObj) {
				if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
					$copyObj->addTugas($relObj->copy($deepCopy));
				}
			}

		} // if ($deepCopy)


		$copyObj->setNew(true);
		$copyObj->setSiswaId(NULL); // this is a auto-increment column, so set to default value
	}

	/**
	 * Makes a copy of this object that will be inserted as a new row in table when saved.
	 * It creates a new object filling in the simple attributes, but skipping any primary
	 * keys that are defined for the table.
	 *
	 * If desired, this method can also make copies of all associated (fkey referrers)
	 * objects.
	 *
	 * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
	 * @return     Siswa Clone of current object.
	 * @throws     PropelException
	 */
	public function copy($deepCopy = false)
	{
		// we use get_class(), because this might be a subclass
		$clazz = get_class($this);
		$copyObj = new $clazz();
		$this->copyInto($copyObj, $deepCopy);
		return $copyObj;
	}

	/**
	 * Returns a peer instance associated with this om.
	 *
	 * Since Peer classes are not to have any instance attributes, this method returns the
	 * same instance for all member of this class. The method could therefore
	 * be static, but this would prevent one from overriding the behavior.
	 *
	 * @return     SiswaPeer
	 */
	public function getPeer()
	{
		if (self::$peer === null) {
			self::$peer = new SiswaPeer();
		}
		return self::$peer;
	}

	/**
	 * Clears out the collTugass collection
	 *
	 * This does not modify the database; however, it will remove any associated objects, causing
	 * them to be refetched by subsequent calls to accessor method.
	 *
	 * @return     void
	 * @see        addTugass()
	 */
	public function clearTugass()
	{
		$this->collTugass = null; // important to set this to NULL since that means it is uninitialized
	}

	/**
	 * Initializes the collTugass collection.
	 *
	 * By default this just sets the collTugass collection to an empty array (like clearcollTugass());
	 * however, you may wish to override this method in your stub class to provide setting appropriate
	 * to your application -- for example, setting the initial array to the values stored in database.
	 *
	 * @return     void
	 */
	public function initTugass()
	{
		$this->collTugass = new PropelObjectCollection();
		$this->collTugass->setModel('Tugas');
	}

	/**
	 * Gets an array of Tugas objects which contain a foreign key that references this object.
	 *
	 * If the $criteria is not null, it is used to always fetch the results from the database.
	 * Otherwise the results are fetched from the database the first time, then cached.
	 * Next time the same method is called without $criteria, the cached collection is returned.
	 * If this Siswa is new, it will return
	 * an empty collection or the current collection; the criteria is ignored on a new object.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 * @throws     PropelException
	 */
	public function getTugass($criteria = null, PropelPDO $con = null)
	{
		if(null === $this->collTugass || null !== $criteria) {
			if ($this->isNew() && null === $this->collTugass) {
				// return empty collection
				$this->initTugass();
			} else {
				$collTugass = TugasQuery::create(null, $criteria)
					->filterBySiswa($this)
					->find($con);
				if (null !== $criteria) {
					return $collTugass;
				}
				$this->collTugass = $collTugass;
			}
		}
		return $this->collTugass;
	}

	/**
	 * Returns the number of related Tugas objects.
	 *
	 * @param      Criteria $criteria
	 * @param      boolean $distinct
	 * @param      PropelPDO $con
	 * @return     int Count of related Tugas objects.
	 * @throws     PropelException
	 */
	public function countTugass(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
	{
		if(null === $this->collTugass || null !== $criteria) {
			if ($this->isNew() && null === $this->collTugass) {
				return 0;
			} else {
				$query = TugasQuery::create(null, $criteria);
				if($distinct) {
					$query->distinct();
				}
				return $query
					->filterBySiswa($this)
					->count($con);
			}
		} else {
			return count($this->collTugass);
		}
	}

	/**
	 * Method called to associate a Tugas object to this object
	 * through the Tugas foreign key attribute.
	 *
	 * @param      Tugas $l Tugas
	 * @return     void
	 * @throws     PropelException
	 */
	public function addTugas(Tugas $l)
	{
		if ($this->collTugass === null) {
			$this->initTugass();
		}
		if (!$this->collTugass->contains($l)) { // only add it if the **same** object is not already associated
			$this->collTugass[]= $l;
			$l->setSiswa($this);
		}
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Siswa is new, it will return
	 * an empty collection; or if this Siswa has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Siswa.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinJenisTugas($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('JenisTugas', $join_behavior);

		return $this->getTugass($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Siswa is new, it will return
	 * an empty collection; or if this Siswa has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Siswa.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinMataPelajaran($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('MataPelajaran', $join_behavior);

		return $this->getTugass($query, $con);
	}


	/**
	 * If this collection has already been initialized with
	 * an identical criteria, it returns the collection.
	 * Otherwise if this Siswa is new, it will return
	 * an empty collection; or if this Siswa has previously
	 * been saved, it will retrieve related Tugass from storage.
	 *
	 * This method is protected by default in order to keep the public
	 * api reasonable.  You can provide public methods for those you
	 * actually need in Siswa.
	 *
	 * @param      Criteria $criteria optional Criteria object to narrow the query
	 * @param      PropelPDO $con optional connection object
	 * @param      string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
	 * @return     PropelCollection|array Tugas[] List of Tugas objects
	 */
	public function getTugassJoinPengguna($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
	{
		$query = TugasQuery::create(null, $criteria);
		$query->joinWith('Pengguna', $join_behavior);

		return $this->getTugass($query, $con);
	}

	/**
	 * Clears the current object and sets all attributes to their default values
	 */
	public function clear()
	{
		$this->siswa_id = null;
		$this->nis = null;
		$this->nama = null;
		$this->jenis_kelamin = null;
		$this->tempat_lahir = null;
		$this->tanggal_lahir = null;
		$this->telepon = null;
		$this->kelas = null;
		$this->foto = null;
		$this->alreadyInSave = false;
		$this->alreadyInValidation = false;
		$this->clearAllReferences();
		$this->resetModified();
		$this->setNew(true);
		$this->setDeleted(false);
	}

	/**
	 * Resets all collections of referencing foreign keys.
	 *
	 * This method is a user-space workaround for PHP's inability to garbage collect objects
	 * with circular references.  This is currently necessary when using Propel in certain
	 * daemon or large-volumne/high-memory operations.
	 *
	 * @param      boolean $deep Whether to also clear the references on all associated objects.
	 */
	public function clearAllReferences($deep = false)
	{
		if ($deep) {
			if ($this->collTugass) {
				foreach ((array) $this->collTugass as $o) {
					$o->clearAllReferences($deep);
				}
			}
		} // if ($deep)

		$this->collTugass = null;
	}

	/**
	 * Catches calls to virtual methods
	 */
	public function __call($name, $params)
	{
		if (preg_match('/get(\w+)/', $name, $matches)) {
			$virtualColumn = $matches[1];
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
			// no lcfirst in php<5.3...
			$virtualColumn[0] = strtolower($virtualColumn[0]);
			if ($this->hasVirtualColumn($virtualColumn)) {
				return $this->getVirtualColumn($virtualColumn);
			}
		}
		return parent::__call($name, $params);
	}

} // BaseSiswa
