<?php


/**
 * Base class that represents a query for the 'jenis_tugas' table.
 *
 * 
 *
 * @method     JenisTugasQuery orderByJenisTugasId($order = Criteria::ASC) Order by the jenis_tugas_id column
 * @method     JenisTugasQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method     JenisTugasQuery orderByDeskripsi($order = Criteria::ASC) Order by the deskripsi column
 *
 * @method     JenisTugasQuery groupByJenisTugasId() Group by the jenis_tugas_id column
 * @method     JenisTugasQuery groupByNama() Group by the nama column
 * @method     JenisTugasQuery groupByDeskripsi() Group by the deskripsi column
 *
 * @method     JenisTugasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     JenisTugasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     JenisTugasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     JenisTugasQuery leftJoinTugas($relationAlias = null) Adds a LEFT JOIN clause to the query using the Tugas relation
 * @method     JenisTugasQuery rightJoinTugas($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Tugas relation
 * @method     JenisTugasQuery innerJoinTugas($relationAlias = null) Adds a INNER JOIN clause to the query using the Tugas relation
 *
 * @method     JenisTugas findOne(PropelPDO $con = null) Return the first JenisTugas matching the query
 * @method     JenisTugas findOneOrCreate(PropelPDO $con = null) Return the first JenisTugas matching the query, or a new JenisTugas object populated from the query conditions when no match is found
 *
 * @method     JenisTugas findOneByJenisTugasId(int $jenis_tugas_id) Return the first JenisTugas filtered by the jenis_tugas_id column
 * @method     JenisTugas findOneByNama(string $nama) Return the first JenisTugas filtered by the nama column
 * @method     JenisTugas findOneByDeskripsi(string $deskripsi) Return the first JenisTugas filtered by the deskripsi column
 *
 * @method     array findByJenisTugasId(int $jenis_tugas_id) Return JenisTugas objects filtered by the jenis_tugas_id column
 * @method     array findByNama(string $nama) Return JenisTugas objects filtered by the nama column
 * @method     array findByDeskripsi(string $deskripsi) Return JenisTugas objects filtered by the deskripsi column
 *
 * @package    propel.generator.tugasku.om
 */
abstract class BaseJenisTugasQuery extends ModelCriteria
{

	/**
	 * Initializes internal state of BaseJenisTugasQuery object.
	 *
	 * @param     string $dbName The dabase name
	 * @param     string $modelName The phpName of a model, e.g. 'Book'
	 * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
	 */
	public function __construct($dbName = 'tugasku', $modelName = 'JenisTugas', $modelAlias = null)
	{
		parent::__construct($dbName, $modelName, $modelAlias);
	}

	/**
	 * Returns a new JenisTugasQuery object.
	 *
	 * @param     string $modelAlias The alias of a model in the query
	 * @param     Criteria $criteria Optional Criteria to build the query from
	 *
	 * @return    JenisTugasQuery
	 */
	public static function create($modelAlias = null, $criteria = null)
	{
		if ($criteria instanceof JenisTugasQuery) {
			return $criteria;
		}
		$query = new JenisTugasQuery();
		if (null !== $modelAlias) {
			$query->setModelAlias($modelAlias);
		}
		if ($criteria instanceof Criteria) {
			$query->mergeWith($criteria);
		}
		return $query;
	}

	/**
	 * Find object by primary key
	 * Use instance pooling to avoid a database query if the object exists
	 * <code>
	 * $obj  = $c->findPk(12, $con);
	 * </code>
	 * @param     mixed $key Primary key to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    JenisTugas|array|mixed the result, formatted by the current formatter
	 */
	public function findPk($key, $con = null)
	{
		if ((null !== ($obj = JenisTugasPeer::getInstanceFromPool((string) $key))) && $this->getFormatter()->isObjectFormatter()) {
			// the object is alredy in the instance pool
			return $obj;
		} else {
			// the object has not been requested yet, or the formatter is not an object formatter
			$criteria = $this->isKeepQuery() ? clone $this : $this;
			$stmt = $criteria
				->filterByPrimaryKey($key)
				->getSelectStatement($con);
			return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
		}
	}

	/**
	 * Find objects by primary key
	 * <code>
	 * $objs = $c->findPks(array(12, 56, 832), $con);
	 * </code>
	 * @param     array $keys Primary keys to use for the query
	 * @param     PropelPDO $con an optional connection object
	 *
	 * @return    PropelObjectCollection|array|mixed the list of results, formatted by the current formatter
	 */
	public function findPks($keys, $con = null)
	{	
		$criteria = $this->isKeepQuery() ? clone $this : $this;
		return $this
			->filterByPrimaryKeys($keys)
			->find($con);
	}

	/**
	 * Filter the query by primary key
	 *
	 * @param     mixed $key Primary key to use for the query
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKey($key)
	{
		return $this->addUsingAlias(JenisTugasPeer::JENIS_TUGAS_ID, $key, Criteria::EQUAL);
	}

	/**
	 * Filter the query by a list of primary keys
	 *
	 * @param     array $keys The list of primary key to use for the query
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByPrimaryKeys($keys)
	{
		return $this->addUsingAlias(JenisTugasPeer::JENIS_TUGAS_ID, $keys, Criteria::IN);
	}

	/**
	 * Filter the query on the jenis_tugas_id column
	 * 
	 * @param     int|array $jenisTugasId The value to use as filter.
	 *            Accepts an associative array('min' => $minValue, 'max' => $maxValue)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByJenisTugasId($jenisTugasId = null, $comparison = null)
	{
		if (is_array($jenisTugasId) && null === $comparison) {
			$comparison = Criteria::IN;
		}
		return $this->addUsingAlias(JenisTugasPeer::JENIS_TUGAS_ID, $jenisTugasId, $comparison);
	}

	/**
	 * Filter the query on the nama column
	 * 
	 * @param     string $nama The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByNama($nama = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($nama)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $nama)) {
				$nama = str_replace('*', '%', $nama);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(JenisTugasPeer::NAMA, $nama, $comparison);
	}

	/**
	 * Filter the query on the deskripsi column
	 * 
	 * @param     string $deskripsi The value to use as filter.
	 *            Accepts wildcards (* and % trigger a LIKE)
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByDeskripsi($deskripsi = null, $comparison = null)
	{
		if (null === $comparison) {
			if (is_array($deskripsi)) {
				$comparison = Criteria::IN;
			} elseif (preg_match('/[\%\*]/', $deskripsi)) {
				$deskripsi = str_replace('*', '%', $deskripsi);
				$comparison = Criteria::LIKE;
			}
		}
		return $this->addUsingAlias(JenisTugasPeer::DESKRIPSI, $deskripsi, $comparison);
	}

	/**
	 * Filter the query by a related Tugas object
	 *
	 * @param     Tugas $tugas  the related object to use as filter
	 * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function filterByTugas($tugas, $comparison = null)
	{
		return $this
			->addUsingAlias(JenisTugasPeer::JENIS_TUGAS_ID, $tugas->getJenisTugasId(), $comparison);
	}

	/**
	 * Adds a JOIN clause to the query using the Tugas relation
	 * 
	 * @param     string $relationAlias optional alias for the relation
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function joinTugas($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		$tableMap = $this->getTableMap();
		$relationMap = $tableMap->getRelation('Tugas');
		
		// create a ModelJoin object for this join
		$join = new ModelJoin();
		$join->setJoinType($joinType);
		$join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
		if ($previousJoin = $this->getPreviousJoin()) {
			$join->setPreviousJoin($previousJoin);
		}
		
		// add the ModelJoin to the current object
		if($relationAlias) {
			$this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
			$this->addJoinObject($join, $relationAlias);
		} else {
			$this->addJoinObject($join, 'Tugas');
		}
		
		return $this;
	}

	/**
	 * Use the Tugas relation Tugas object
	 *
	 * @see       useQuery()
	 * 
	 * @param     string $relationAlias optional alias for the relation,
	 *                                   to be used as main alias in the secondary query
	 * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
	 *
	 * @return    TugasQuery A secondary query class using the current class as primary query
	 */
	public function useTugasQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
	{
		return $this
			->joinTugas($relationAlias, $joinType)
			->useQuery($relationAlias ? $relationAlias : 'Tugas', 'TugasQuery');
	}

	/**
	 * Exclude object from result
	 *
	 * @param     JenisTugas $jenisTugas Object to remove from the list of results
	 *
	 * @return    JenisTugasQuery The current query, for fluid interface
	 */
	public function prune($jenisTugas = null)
	{
		if ($jenisTugas) {
			$this->addUsingAlias(JenisTugasPeer::JENIS_TUGAS_ID, $jenisTugas->getJenisTugasId(), Criteria::NOT_EQUAL);
	  }
	  
		return $this;
	}

} // BaseJenisTugasQuery
