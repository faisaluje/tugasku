<?php
// phpinfo();
require_once 'startup.php';

//Auth//
$auth = new XondAuth();
$auth->setAuthObject('Pengguna');
$auth->setUserColumn('username');
$auth->setPasswordColumn('Password');
// $auth->setRedirectUrl('/app/default.html');
$auth->addGroupMembership('JabatanId');
$auth->setTheme('xtheme-green');

$app = new XondApplication();
if($app->setAuth($auth)){
	if($app->getAuth()->getSession()){
		$user = $app->getAuth()->getUser();
		switch ($user->getJabatanId()){
			case 1:
				$app->registerModule('Siswa');
				$app->registerModule('Pengguna');
				$app->registerModule('TugasAll');
				break;
			case 2:
				$app->registerModule('Siswa');
				$app->registerModule('TugasAll');
				break;
			case 3:
				$app->registerModule('Tugas');
				$app->registerModule('TugasAll');
//				$app->registerModule('MataPelajaran');
				break;
		}
		// print_r($_COOKIE);
		$app->setUserInfo(array(
				'Nama Akun' => $user->getUsername(),
				'Nama' => $user->getNama(),
				'Status' => $user->getJabatan()->getNama(),
		));
	}
}

$libDir = $app->LIBDIR;
$sysDir = $app->SYSDIR;

//App
$app->registerStyle('GridSummary',$libDir);
$app->registerStyle('file-upload',$libDir);
$app->registerStyle('ProgressColumn',$libDir);
$app->registerStyle('grid-examples',$libDir);
$app->registerStyle('summary',$libDir);
$app->registerStyle('sppsai',$sysDir);
$app->registerStyle('TreeGrid',$libDir);
$app->registerStyle('TreeGridLevels',$libDir);
$app->registerUi('GridSummary',$libDir);
$app->registerUi('FileUploadField',$libDir);
$app->registerUi('ProgressColumn',$libDir);
$app->registerUi('groupcombo',$libDir);
$app->registerUi('groupdataview',$libDir);
$app->registerUi('GroupSummary',$libDir);
$app->registerUi('example',$sysDir);
$app->registerUi('uxfusionpak',$libDir);
$app->registerUi('TreeGrid',$libDir);
$app->setTheme('xtheme-green');
$app->setDevelStatus(true);
$app->start();
$app->render();
?>