<html lang="en">
<head>
<title>SPIN Keuangan - Instalasi</title>
<style>
body {
     color:#666666;
     font-family:Georgia,serif;
     font-size:14.5px;
     padding:10px 0;
}
</style>
</head>
<body>
<?php

require_once('startup.php');

//error_reporting(E_ALL);
$thang = $_SESSION["strtahun"]; 
$kddept = $_SESSION["strdept"];
$kdunit = $_SESSION["strunit"];
$kdsatker = $_SESSION["strsatker"];
$kddekon = $_SESSION["strkddekon"];

if ($kdsatker) {
	$kddekon = 3;
	$_SESSION["strkddekon"] = 3;
} else {
	/* Untuk kasus semua satker dan dekon digabung (via ceklis) */
	if (isset($_REQUEST["gabung"])) {
		if ($_REQUEST["gabung"] == 1) {
			$_SESSION["strkddekon"] = 4; 
			$kddekon = 4;
		} else {
			$kddekon = 1;
			$_SESSION["strkddekon"] = 1;
		}
	} else {
		$kddekon = 1;
		$_SESSION["strkddekon"] = 1;		
	}
}
/*
$thang = '2012';
$kddept = '023';
$kdunit = '03';
//$kddekon = '1';   //1=pusat, 3=dekon
$kddekon = '1';   //1=pusat, 3=dekon, 4=gabung
//$kdsatker = '414726';
//$kdsatker = '040007'; diy
//$kdsatker = '310030'; g'talo
//$kdsatker = '682118'; 
*/

//Timer//
$mtime = microtime(); 
$mtime = explode(' ', $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$starttime = $mtime; 
$where = "where kdsatker = '$kdsatker' and kddept='$kddept' and kdunit= '$kdunit'";

function countTableRows($tablename) {
	return getValueBySql("select count(*) from $tablename");
}

function truncateTable($tablename) {
	try {
		executeSql("set foreign_key_checks = 0; truncate table $tablename");
		echo "Table emptied.";
	} catch (Exception $e) {
		echo "Error : ".$e->getMessage();
	}	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Importing Data RKAKL</title>
</head>
<body>
<?php

//error_reporting(E_ALL);
//error_reporting(E_ALL);
//session_start();

try {
	
	//$conn = odbc_connect('RKAKL12','','');
	
	if ($_REQUEST["action"] == "empty") {
		truncateTable($_REQUEST["data"]);
		?><br></br><a href="<?=$_SERVER["PHP_SELF"]?>">Kembali</a><?php
		die;
	}
	
	switch ($_REQUEST["data"]) {

		case "output" :
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : ""; 
			
			switch ($kddekon) {
				case '1':
					$sql = "select a.*, b.nmoutput, b.sat from rkakl_d_output a
							left join rkakl_t_output b on a.kdgiat = b.kdgiat and a.kdoutput = b.kdoutput
							where a.kddept = '$kddept' and kdunit= '$kdunit' and a.thang = '$thang' and a.kddekon = '$kddekon'
							$andkdsatker
							order by a.kdsatker, a.kdunit, a.kdprogram, a.kdgiat, a.kdoutput";					
					break;
				case '3':
					$sql = "select a.*, b.nmoutput, b.sat from rkakl_d_output a
							left join rkakl_t_output b on a.kdgiat = b.kdgiat and a.kdoutput = b.kdoutput
							where a.kddept = '$kddept' and a.thang = '$thang' and a.kddekon = '$kddekon'
							$andkdsatker
							order by a.kdsatker, a.kdunit, a.kdprogram, a.kdgiat, a.kdoutput";					
					break;
				case '4':
					$sql = "select a.*, b.nmoutput, b.sat from rkakl_d_output a 
						left join rkakl_t_output b on a.kdgiat = b.kdgiat and a.kdoutput = b.kdoutput					
						where a.kddept = '$kddept' and kdunit= '$kdunit' and a.thang = '$thang' 
						order by a.kdsatker, a.kdunit, a.kdprogram, a.kdgiat, a.kdoutput";					
					break;					
			}
			//echo $sql; die;
			
		
			$rs = getDataBySql($sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			foreach ($rs as $row) {
				//print_r($row); continue;
				
				$output = new Output();
				$output->setKode($row["kdoutput"]);
				$output->setKodeKp($row["kddekon"]);
				$output->setNama(trim(str_replace(".","",$row["nmoutput"])));
				$output->setVol($row["vol"]);
				$output->setVolmin1($row["volmin1"]);
				$output->setVolpls1($row["volpls1"]);
				$output->setVolpls2($row["volpls2"]);
				$output->setRphmin1($row["rphmin1"]);
				$output->setRphpls1($row["rphpls1"]);
				$output->setRphpls2($row["rphpls2"]);
				$output->setSatuan(trim(str_replace(".","", $row["sat"])));
				
				$c = new Criteria();
				$c->add(KabkotaPeer::KODE, $row["kdkabkota"]);
				$kabkota = KabkotaPeer::doSelectOne($c);				
				$output->setKabkota($kabkota);
				
				$c = new Criteria();
				$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
				$kegiatan = KegiatanPeer::doSelectOne($c);
				$output->setKegiatan($kegiatan);
				$output->setKodeOutput($kegiatan->getKode().".".$output->getKode());
				
				$c = new Criteria();
				$c->add(TahunPeer::NAMA, $row["thang"]);
				$tahun = TahunPeer::doSelectOne($c);
				$output->setTahun($tahun);
				
				$c = new Criteria();
				$c->add(UnitPeer::KODE, $row["kdunit"]);
				$unit = UnitPeer::doSelectOne($c);
				$output->setUnit($unit);
				
				$c = new Criteria();
				$c->add(SatkerPeer::KODE, $row["kdsatker"]);
				$satker = SatkerPeer::doSelectOne($c);
				$output->setSatker($satker);
				
				echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"]." - ".$row["nmoutput"]." | ". $row["kdunit"].".".$row["kdsatker"] ."<br>";
				
				$output->save($con);
			}
			$con->commit();
			break;
			
		case "sub_output":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			$sql = "select * from rkakl_d_soutput where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
					$andkdsatker
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput";
			//echo $sql; die;
			if ($kddekon == '4') {
				$sql = "select * from rkakl_d_soutput where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput";
			}
			
			$rs = getDataBySql($sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			foreach ($rs as $row) {
				//print_r($row);				
				$sub_output = new SubOutput();
				$sub_output->setKode($row["kdsoutput"]);				
				$sub_output->setNama(trim(str_replace(".","",$row["ursoutput"])));
				$sub_output->setVol($row["volsout"]);
				$sub_output->setSatuan($row["sbmksat"]);
				
				$c = new Criteria();
				$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
				$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
				$c->add(OutputPeer::KODE, $row["kdoutput"]);
				$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
				$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
				$output = OutputPeer::doSelectOne($c);								
				if (!is_object($output)) {
					throw new Exception("The parent (output) not found");					
				} else {
					$sub_output->setOutput($output);
					$sub_output->setKodeSubOutput($output->getKodeOutput().".".$sub_output->getKode());
					if (trim(strtolower($row["ursoutput"])) == "tanpa sub output") {
						$sub_output->setNama($output->getNama());
						$tanpa_sub_output_string = "( digantikan ".$output->getNama()." )";
					}					
				}
				echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"]." - ".$row["ursoutput"]." $tanpa_sub_output_string<br>";				
				$sub_output->save($con);
				$tanpa_sub_output_string = "";
			}
			$con->commit();
			break;			

		case "komponen":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			
			$sql = "select * from rkakl_d_kmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen";
			//echo $sql; die;
			if ($kddekon == '4') {
				$sql = "select * from rkakl_d_kmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen";			
			}
			
			$rs = getDataBySql($sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			foreach ($rs as $row) {
				try {
					//print_r($row);				
					$komponen = new Komponen();
					$komponen->setKode($row["kdkmpnen"]);
					$komponen->setNama(trim(str_replace(".","",$row["urkmpnen"])));
					
					$c = new Criteria();
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
					$sub_output = SubOutputPeer::doSelectOne($c);
									
					if (!is_object($sub_output)) {
						throw new Exception("<font color='red'>The parent (suboutput) not found. Komponen : <b>".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"]."</b></font><br>");
					} else {
						$komponen->setSubOutput($sub_output);
						$komponen->setKodeKomponen($sub_output->getKodeSubOutput().".".$komponen->getKode());	
					}
					echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"]." - ".$row["urkmpnen"]."<br>";
					$komponen->save($con);
				
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			$con->commit();
			break;

		case "sub_komponen":
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			
			$sql = "select * from rkakl_d_skmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen";
			//echo $sql; die;			
			if ($kddekon == '4') {
				$sql = "select * from rkakl_d_skmpnen where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
				order by kdsatker, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen";
			}
			//echo $kddekon."<br>";
			//echo $sql;
			
			$rs = getDataBySql($sql);
			
			
			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			foreach ($rs as $row) {
				//print_r($row);			
				try {	
					$sub_komponen = new SubKomponen();
					$sub_komponen->setKode($row["kdskmpnen"]);
					$sub_komponen->setNama(trim(str_replace(".","",$row["urskmpnen"])));
					
					$c = new Criteria();
					$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
					$komponen = KomponenPeer::doSelectOne($c);
									
					if (!is_object($komponen)) {
						//throw new Exception("The parent (komponen) not found at row #$i<br>");					
						throw new Exception("<font color='red'>The parent (komponen) not found. Komponen : <b>".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"]."</b></font><br>");
					} else {
						$sub_komponen->setKomponen($komponen);	
					}
					//echo $i++.") ".$row["urskmpnen"]."<br>";								
					echo $i++.") ".$row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"]." - ".$row["urskmpnen"]."<br>";
					
					$komponen->save($con);
				} catch (Exception $e) {
					echo $e->getMessage();
				}
			}
			$con->commit();
			break;

		case "item" :
			
			$andkdsatker = ($kddekon == '3') ? "and kdsatker = \"$kdsatker\"" : "";
			$sql = "select distinct * from rkakl_d_item where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' and kddekon = '$kddekon'
				$andkdsatker	
				order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem";
				//order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem  where kddekon = '1'";
				//echo $sql;die;
			
			if ($kddekon == '4') {
				$sql = "select distinct * from rkakl_d_item where kddept = '$kddept' and kdunit= '$kdunit' and thang = '$thang' 
					order by kdsatker, kddept, kdunit, kdprogram, kdgiat, kdoutput, kdsoutput, kdkmpnen, kdskmpnen, kdakun, noitem";
			}
			
			$rs = getDataBySql($sql);

			$con = Propel::getConnection(LokasiPeer::DATABASE_NAME);
			$con->beginTransaction();	
			$i = 1;
			
			foreach ($rs as $row) {
				//print_r($row);
				try {			
					$d = new Detil();
					//$d->setKodeDetil($row["noitem"]);
					$d->setNoHeader($row["kdheader"]);
					$d->setNoHeader1($row["header1"]);
					$d->setNoHeader2($row["header2"]);
					$d->setNoItem($row["noitem"]);
					$d->setNama(trim(str_replace(".","",$row["nmitem"])));
					$d->setKodeKppn($row["kdkppn"]);
					$d->setKodeBeban($row["kdbeban"]);
					$d->setKodeJenisbantuan($row["kdjnsban"]);
					$d->setKodeCaratarik($row["kdctarik"]);
					$d->setRegister($row["register"]);
					$d->setVolumeRka($row["volkeg"]);
					$d->setVol1($row["vol1"]);
					$d->setVol2($row["vol2"]);
					$d->setVol3($row["vol3"]);					
					$d->setVol4($row["vol4"]);
					$d->setSat1($row["sat1"]);
					$d->setSat2($row["sat2"]);
					$d->setSat3($row["sat3"]);
					$d->setSat4($row["sat4"]);
					$d->setVolumeSpp(0);
					$d->setSatuan($row["satkeg"]);
					$d->setHargaSatuan($row["hargasat"]);
					$d->setJumlahRka($row["jumlah"]);
					$d->setJumlahSpp(0);
					
					$c = new Criteria();
					$c->addJoin(SubKomponenPeer::KOMPONEN_ID, KomponenPeer::KOMPONEN_ID);
					$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
					$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
					$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
					$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
					$c->add(SubKomponenPeer::KODE, $row["kdskmpnen"]);
					$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
					$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
					$c->add(OutputPeer::KODE, $row["kdoutput"]);
					$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
					$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
	
					$kodeLengkap = $row["kdskmpnen"].".".$row["kdkmpnen"] .".". $row["kdsoutput"] .".". $row["kdoutput"] .".". $row["kdgiat"];
					$kodeLengkap = $row["kdgiat"].".".$row["kdoutput"].".".$row["kdsoutput"].".".$row["kdkmpnen"].".".$row["kdskmpnen"];
					
					$sub_komponen = SubKomponenPeer::doSelectOne($c);
					if (!is_object($sub_komponen)) {
						//throw new Exception("The parent (sub komponen) not found at row #$i ( $kodeLengkap )");
						echo "&nbsp;&nbsp;&nbsp;<font color='red'>Notice: The parent (sub komponen) not found at row #$i ( $kodeLengkap ). Trying to catch the komponen..</font><br>";
						
						$c = new Criteria();
						$c->addJoin(KomponenPeer::SUB_OUTPUT_ID, SubOutputPeer::SUB_OUTPUT_ID);
						$c->addJoin(SubOutputPeer::OUTPUT_ID, OutputPeer::OUTPUT_ID);
						$c->addJoin(OutputPeer::KEGIATAN_ID, KegiatanPeer::KEGIATAN_ID);
						$c->addJoin(OutputPeer::SATKER_ID, SatkerPeer::SATKER_ID);
						$c->add(KomponenPeer::KODE, $row["kdkmpnen"]);
						$c->add(SubOutputPeer::KODE, $row["kdsoutput"]);
						$c->add(OutputPeer::KODE, $row["kdoutput"]);
						$c->add(KegiatanPeer::KODE, $row["kdgiat"]);
						$c->add(SatkerPeer::KODE, $row["kdsatker"]);			//PENTING BHAAANGET
						$komponen = KomponenPeer::doSelectOne($c);						
						$d->setKomponen($komponen);
						
					} else {
						$d->setSubKomponen($sub_komponen);
						$d->setKomponen($sub_komponen->getKomponen());											
					}
					
					$c = new Criteria();
					$c->add(MakPeer::KODE, $row["kdakun"]);
					$mak = MakPeer::doSelectOne($c);
					if (!is_object($mak)) {
						throw new Exception("Kode akun not found<br>");					
					} else {
						$d->setMak($mak);
					}
					echo $i++.") ".$row["nmitem"]."<br>";								
					
					$d->save($con);
				} catch (Exception $e) {
					$e->getMessage();	
				}
			}
			$con->commit();			
			break;	
		
		default :
			?>
			<b>Parameter Import:</b><br>
			- Tahun : <?=$thang?><br>
			- Dept: <?=$kddept?><br>
			- Unit: <?=$kdunit?><br>
			- Satker: <?=$kdsatker?><br> 
			- Lingkup: <?=$kddekon == '3' ? "Dekon": ($kddekon == '1') ? "Pusat" : "Nasional"?><br><br>
			
			<b>Pilih data yg akan diimport</b> (klik pada link nama tabel): <br/>
			- <a href=<?=$_SERVER["PHP_SELF"]?>?data=output>output</a> (<?=countTableRows("output")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=output&action=empty>empty</a><br/>
			- <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output>sub_output</a> (<?=countTableRows("sub_output")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_output&action=empty>empty</a><br/>
			- <a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen>komponen</a> (<?=countTableRows("komponen")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=komponen&action=empty>empty</a><br/>									
			- <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen>sub_komponen</a> (<?=countTableRows("sub_komponen")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=sub_komponen&action=empty>empty</a><br/>
			- <a href=<?=$_SERVER["PHP_SELF"]?>?data=item>item</a> (<?=countTableRows("detil")?>) <a href=<?=$_SERVER["PHP_SELF"]?>?data=detil&action=empty>empty</a><br/><br/>
			<?php		 
			break;	
	}	
	//odbc_close($conn);

} catch (Exception $e) {
	echo $e->getMessage();
}

$mtime = microtime(); 
$mtime = explode(" ", $mtime); 
$mtime = $mtime[1] + $mtime[0]; 
$endtime = $mtime; 
$totaltime = ($endtime - $starttime); 
println('Diproses dalam ' .$totaltime. ' detik.');

?>
<br><a href="<?=$_SERVER["PHP_SELF"]?>">Kembali</a><br>
</body>
</html>