<?php
require_once 'startup.php';

if ($_REQUEST["getdata"] == "sub_satker") {
	
	$sub_satkers = SubSatkerPeer::doSelect(new Criteria());	
	$i = 1;	
	foreach($sub_satkers as $s){
		//$s = new SubSatker();
		$sarr = $s->toArray();
		//$sarr["NamaSatker"] = $i++.") ".$s->getSatker()->getNama();
		$sarr["NamaSatker"] = $s->getSatker()->getSatkerId().") ".$s->getSatker()->getNama();
		$out[] = $sarr;
	}
	//print_r($out); die;
	//$this->write(tableJson($out, sizeof($out), DataTransaksiPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));	
	echo(tableJson($out, sizeof($out), SubSatkerPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));

} else if ($_REQUEST["savedata"] == "sub_satker") {
	
	$data = splitJsonArray($_REQUEST['data']);
    //print_r($data); die;
    $rowsAffected = 0;
    
    foreach ($data as $d) {
    	$row = json_decode(stripcslashes($d));
    	
    	$ss = SubSatkerPeer::retrieveByPK($row->SubSatkerId);
    	
    	if (!is_object($ss)) {
    		$ss = new SubSatker();    		
    	}
    	$ss->setNama($row->Nama);
    	$ss->setNick($row->Nick);
    	$ss->setSingkatan($row->Singkatan);
    	$ss->setSatkerId($row->SatkerId);
    	try {
	    	if ($ss->save()) {
	    		$rowsAffected++;
	    	}
    	} catch (Exception $e) {
    		print_r($ss);
    		echo $e->getMessage();
    	}
    }    
    $success = ($rowsAffected > 0) ? 'true' : 'false';	        
	$result = sprintf("{ 'success' : %s, 'affected' : '%s' }", $success, $rowsAffected);   	
    
	echo($result);    	
	
} else if ($_REQUEST["getdata"] == "satker") {
	$c = new Criteria();
	$c->add(SatkerPeer::AKTIF, 1);
	
	$satkers = SatkerPeer::doSelect($c);
	//print_r($satker);	
	//$i = 1;	
	/*
	foreach($satkers as $s){
		//$s = new Satker();
		$sarr = $s->toArray();
		//$sarr["NamaSatker"] = $i++.") ".$s->getSatker()->getNama();
		$sarr["NamaSatker"] = $s->getSatker()->getSatkerId().") ".$s->getSatker()->getNama();
		$out[] = $sarr;
	}
	//print_r($out); die;
	//$this->write(tableJson($out, sizeof($out), DataTransaksiPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));	
	echo(tableJson($out, sizeof($out), SatkerPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	*/
	echo (tableJson(getArray($satkers), SatkerPeer::doCount($c), SatkerPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	
} else if ($_REQUEST["savedata"] == "satker") {
	
	$data = splitJsonArray($_REQUEST['data']);
    //print_r($data); die;
    $rowsAffected = 0;
    
    foreach ($data as $d) {
    	$row = json_decode(stripcslashes($d));
    	
    	$ss = SatkerPeer::retrieveByPK($row->SatkerId);
    	
    	if (!is_object($ss)) {
    		$ss = new Satker();    		
    	}
    	$ss->setNama($row->Nama);
    	$ss->setNick($row->Nick);
    	$ss->setAlamat($row->Alamat);
    	$ss->setSingkatan($row->Singkatan);
    	//$ss->setSatkerId($row->SatkerId);
    	try {
	    	if ($ss->save()) {
	    		$rowsAffected++;
	    	}
    	} catch (Exception $e) {
    		print_r($ss);
    		echo $e->getMessage();
    	}
    }    
    $success = ($rowsAffected > 0) ? 'true' : 'false';	        
	$result = sprintf("{ 'success' : %s, 'affected' : '%s' }", $success, $rowsAffected);   	
    
	echo($result);    	

	
} else if ($_REQUEST["getdata"] == "bendahara") {
	
	$c = new Criteria();
	$c->add(PenerimaPeer::BPP_ID, 1);	
	$bendaharas = PenerimaPeer::doSelect($c);
	//print_r($bendaharas);	
	//$i = 1;	
	/*
	foreach($satkers as $s){
		//$s = new Satker();
		$sarr = $s->toArray();
		//$sarr["NamaSatker"] = $i++.") ".$s->getSatker()->getNama();
		$sarr["NamaSatker"] = $s->getSatker()->getSatkerId().") ".$s->getSatker()->getNama();
		$out[] = $sarr;
	}
	//print_r($out); die;
	//$this->write(tableJson($out, sizeof($out), DataTransaksiPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));	
	echo(tableJson($out, sizeof($out), SatkerPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	*/
	echo (tableJson(getArray($bendaharas), PenerimaPeer::doCount($c), PenerimaPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	
} else if ($_REQUEST["savedata"] == "bendahara") {
	
	$data = splitJsonArray($_REQUEST['data']);
    //print_r($data); die;
    $rowsAffected = 0;
    //error_reporting(E_ALL);
    foreach ($data as $d) {
    	$row = json_decode(stripcslashes($d));
    	
    	$ss = PenerimaPeer::retrieveByPK($row->PenerimaId);
    	
    	if (!is_object($ss)) {
    		$ss = new Penerima();    		
    	}
    	//print_r($ss);
    	$rowArr = get_object_vars($row);	
    	unset($rowArr["PenerimaId"]);
    	
    	$ss->fromArray($rowArr, BasePeer::TYPE_PHPNAME);
    	$ss->setBppId(1);
    	
    	try {
	    	if ($ss->save()) {
	    		$rowsAffected++;
	    	}
    	} catch (Exception $e) {
    		//print_r($ss);
    		//echo $e->getMessage();
    	}
    }    
    $success = ($rowsAffected > 0) ? 'true' : 'false';	        
	$result = sprintf("{ 'success' : %s, 'affected' : '%s' }", $success, $rowsAffected);   	
    
	echo($result);    	

} else if ($_REQUEST["getdata"] == "pengguna") {
	//error_reporting(E_ALL);
	$c = new Criteria();
	//$c->add(PenerimaPeer::BPP_ID, 1);	
	$penggunas = PenggunaPeer::doSelect($c);
	
	//print_r($penggunas);	
	//$i = 1;	
	/*
	foreach($satkers as $s){
		//$s = new Satker();
		$sarr = $s->toArray();
		//$sarr["NamaSatker"] = $i++.") ".$s->getSatker()->getNama();
		$sarr["NamaSatker"] = $s->getSatker()->getSatkerId().") ".$s->getSatker()->getNama();
		$out[] = $sarr;
	}
	//print_r($out); die;
	//$this->write(tableJson($out, sizeof($out), DataTransaksiPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));	
	echo(tableJson($out, sizeof($out), SatkerPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));
	*/
	echo (tableJson(getArray($penggunas), PenggunaPeer::doCount($c), PenggunaPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));

} else if ($_REQUEST["savedata"] == "setting") {
	error_reporting(E_ALL);
	$s = SettingPeer::retrieveByPK($_REQUEST["SettingId"]);
	if (!is_object($s)) {
		$s = new Setting();
	}
	unset($_POST["SettingId"]);
	$s->fromArray($_POST, BasePeer::TYPE_PHPNAME);
	//print_r($s); die;	
	if ($s->save()) {
		echo("{ 'success' : 'true', 'message' : 'Berhasil menyimpan perubahan.' }");		
	} else {
		echo("{ 'success' : 'false', 'message' : 'Tidak ada perubahan.' }");		
	}
} else if ($_REQUEST["getdata"] == "setting") {
	//error_reporting(E_ALL);
	$c = new Criteria();
	//$c->add(PenerimaPeer::BPP_ID, 1);	
	$settings = SettingPeer::doSelect($c);
	echo (tableJson(getArray($settings), SettingPeer::doCount($c), SettingPeer::getFieldNames(BasePeer::TYPE_PHPNAME)));	
} else {
?>
<html>
<head>
    <title>Sistem Pengendalian Intern (SPIN) Keuangan Dikdas</title>
    <link rel="stylesheet" type="text/css" href="extjs/resources/css/ext-all.css"/>
	<link rel="stylesheet" type="text/css" href="app/GridSummary.css"/>
	<script type="text/javascript" src="extjs/adapter/ext/ext-base.js"></script>
	<script type="text/javascript" src="extjs/ext-all-debug.js"></script>
	<!-- script type="text/javascript" src="app/GroupSummary.js"></script -->
	<script type="text/javascript" src="setup/setting.js"></script>
	<style type="text/css">
	.icon-grid {
		background-image:url(/assets/icons/grid.png) !important;
	}
	.add {
	    background-image:url(../assets/icons/add.png) !important;
	}
	.remove {
	    background-image:url(../assets/icons/delete.png) !important;
	}
	.save {
	    background-image:url(../assets/icons/save.png) !important;
	
	}
	.refresh {
	    background-image:url(../assets/icons/arrow_refresh.png) !important;
	
	}
	</style>
</head>
<body>
<div id="header"></div>
<div id="grid_satker"></div><br>
<div id="grid_bendahara"></div><br>
<div id="grid_subsatker"></div>
<div id="grid"></div>       
</body>
</html>
<?php
}
?>