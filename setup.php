<html lang="en">
<head>
<title>SPIN Keuangan - Instalasi</title>
<style>
.top10 {
	/*background-color:#FFF7EF;*/
	background-color: #F1F9F9;
	border:7px solid #E9F4F6;
	color: #666666;
	margin: 10px 0 20px; /*may change */
	padding: 20px 10px 30px 20px;  /*may change */
	width: 800px;
	position: absolute;
}
/*
.top10 h2{
     color:#333333;
     font-family:Georgia,serif;
     font-size:25.5px;
     font-style:italic;
     margin:10px 0px 5px 0px;
}
*/
.top10 p {
     color:#666666;
     font-family:Georgia,serif;
     font-size:14.5px;
     padding:10px 0;
}
 
.top10 span a {
     bottom:25px;
     color:#DEDEDE;	 
     font-size:55px;
	 letter-spacing: -2.5px;
	 font-family: Times New Roman, times-roman, georgia, serif;
     position:absolute;
	 text-decoration: none;
     right:0;
}

.top10 span a:hover{
     color:#FF9900;	 
}
 
.top10 img{
     float:right;
     margin:10px;
}
 
h2 {
     /*font-family: Georgia, serif;* /
	 font-family: "Adobe Caslon Pro", "Hoefler Text", Georgia, Garamond, Times, serif;
     font-size: 30px;
     font-style: normal;
     font-weight: normal;
     /*text-transform: uppercase;* /
     letter-spacing: 2.5px;
	 */
	font-family: Gill Sans, Verdana;
	font-size: 11px;
	line-height: 14px;
	text-transform: uppercase;
	letter-spacing: 2px;
	font-weight: bold;
}
h1 {
    font-family: Palatino Linotype, Palatino MT, Times New Roman, times-roman, georgia, serif;
	color: #444;
	margin: 0;
	padding: 0px 0px 6px 0px;
	font-size: 30px;
	line-height: 44px;
	/*letter-spacing: -2px;*/
	font-weight: bold;	
}
body {
	margin-top: 30px;
	margin-left: 50px;
	width: 800px;
	/*font-family: Georgia,serif;*/
}
/* Form */
.myform{
	/*margin:0 auto;*/
	width:400px;
	/*padding:14px;*/
}
#stylized label{
	display:block;
	font-weight:bold;
	text-align:right;
	width:140px;
	float:left;
	font-family: Palatino Linotype, Palatino MT, Times New Roman, times-roman, georgia, serif;
}
#stylized .small{
	color:#666666;
	display:block;
	font-size:11px;
	font-weight:normal;
	text-align:right;
	width:140px;
}
#stylized input{
	float:left;
	font-size:12px;
	padding:4px 2px;
	border:solid 1px #aacfe4;
	width:200px;
	margin:2px 0 20px 10px;
}
#stylized button{
	clear:both;
	margin-left:150px;
	width:125px;
	height:31px;
	background:#666666 url(img/button.png) no-repeat;
	text-align:center;
	line-height:31px;
	color:#FFFFFF;
	font-size:11px;
	font-weight:bold;
}
/* tables */

table {
	font: 13px/24px Verdana, Arial, Helvetica, sans-serif;
	border-collapse: collapse;
	width: 700px;
	}

th {
	padding: 0 0.5em;
	text-align: left;
	}

tr.yellow td {
	border-top: 1px solid #FB7A31;
	border-bottom: 1px solid #FB7A31;
	background: #FFC;
	}

td {
	/*border-bottom: 1px solid #CCC;*/
	padding: 0 0.5em;
	}

td:first-child {
	width: 30px;
	}

td+td {
	border-left: 1px solid #CCC;
	text-align: left;
	}
</style>
</head>
<body>
<div/> 

<img src="setup/spin-new-logo-flat-small.png"/>
<div class="top10">

<?php 

include "startup.php";
include "setup/functions.php";

//define("D", DIRECTORY_SEPARATOR);
//define("ROOT", dirname(__FILE__));

$step = $_REQUEST["step"];
$step = ($step) ? $step : 1;

switch ($step) {
	case "1":
?>
		<h2>SPIN Keuangan</h2>
		<h1>Selamat!</h1>
		<p>Dengan tampilnya halaman ini anda telah berhasil menginstall server yang memungkinkan 
		Aplikasi SPIN mulai diinstal. Aplikasi Sistem Pengendalian Internal Keuangan atau bisa dinamai 
		SPIN kini memiliki antarmuka instalasi yang memungkinkan instalasi yang sedikit lebih mudah "step-by-step". 
		Tahap-tahap instalasi SPIN adalah :</p>
		<ul>
			<li>Instalasi XAMPP server & setting virtual host (done!)</li>
			<li>Requirements Check</li>
			<li>Setting Aplikasi</li>			
			<li>Instalasi Database</li>
			<li>Instalasi SPIN Network Client</li>
			<li>Upload Database RKAKL</li>
			<li>Import Referensi</li>
			<li>Upload Users</li>
			<li>Import database</li>
		</ul> 
		<p>Lanjut ke tahap berikut</p>
		<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$step+1?>>Next</a></span>
<?php
		break;
	case "2":
?>
		<h2>SPIN Keuangan</h2>
		<h1>Requirements Check</h1>
		<p>Berikut ini daftar kebutuhan aplikasi. Jika masih ada yang bertanda silang, maka
		instalasi tidak akan bisa dilanjutkan.</p>
		<?php 
			$checkListArr = array();
			
			/* Database online */			
			$title = "Database online";
			$link = @mysql_connect('localhost', 'root', '');
			if ($link) {
				$check = true;
			} else {
				$error = mysql_error();
				if (stripos($error, "Access denied for user") === 0) {
					$check = true;
				} else {
					$check = false;
				}
				//echo ($check) ? "true" : "false"; echo "<br>";
			}
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Password Database */
			$title = "Password database sudah di-set.";
			$link = @mysql_connect('localhost', 'root', '');
			if ($link) {
				$check = false;
			} else {
				$check = true;
			}
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Check servername */
			$servername = $_SERVER["SERVER_NAME"];
			
			/* Assets Shared Directory */
			$title = "Asset terinstal dan bisa diakses";
			$check = checkDir("assets");
			array_push($checkListArr, array("title"=>$title, "check"=>$check));

			/* Extjs Shared Directory */
			$title = "ExtJS terinstal dan bisa diakses";
			$check = checkDir("extjs");
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* html2ps Shared Directory */
			$title = "HTML2PS terinstal dan bisa diakses";
			$check = checkDir("html2ps");
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Xond Shared Directory */
			$title = "Xond terinstal dan bisa diakses";
			$check = checkDir("xond");
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Check Creole */
			$title = "Library Creole terinstal";
			@include('creole/Creole.php');
			$check = class_exists('Creole');
			array_push($checkListArr, array("title"=>$title, "check"=>$check));

			/* Check Propel */
			$title = "Library Propel terinstal";
			//@include('propel/Propel.php');
			$check = class_exists('Propel');
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Check Propel */
			$title = "Library PHPExcel terinstal";
			//@include('PHPExcel/PHPExcel.php');
			$check = class_exists('PHPExcel');
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
			/* Check Uploads */			
			$check = is_writable(dirname(__FILE__).D."uploads".D."rkakl".D."xml");
			$title = ($check) ? "SPIN Network upload directory ada dan dapat ditulisi" : "SPIN Network directory tidak ada atau tidak dapat ditulisi";
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
						
			$check = is_writable(dirname(__FILE__).D."uploads".D."pengguna");
			$title = ($check) ? "Upload directory untuk pengguna ada dan dapat ditulisi" : "Upload directory untuk pengguna tidak ada atau tidak dapat ditulisi";
			array_push($checkListArr, array("title"=>$title, "check"=>$check));
			
		?>
		
		<table border=0>
		<?php
		$ok2continue = true;
		foreach ($checkListArr as $c) {
			$img = ($c["check"]) ? "<img src='setup/tick.png'/>" : "<img src='setup/cross.png'/>";
			$terinstal = ($c["check"]) ? $c["title"] : str_replace("terinstal", "<b>belum</b> terinstal", $c["title"]);
			
		?>
		<tr><td><?=$img?></td><td><?=$terinstal?></td></tr>
		<?php
			$ok2continue = ($ok2continue && $c["check"] );
			
		} 
		$nextStep = $ok2continue ? $step+1 : $step;
		?>
		</table>
		<p>Lanjut ke tahap berikut</p>
		<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$nextStep?>>Next</a></span>
<?php
		break;
	case "3":
?>
		<div id="stylized" class="myform" width="500px">
		<form id="database_form" name="form" method="post" action="<?=$_SERVER["PHP_SELF"]?>">
		<h2>SPIN Keuangan</h2>
		<h1>Setting Database</h1>
		<p>Isilah form berikut ini sesuai setting yang berlaku pada server</p>
		
		<label>User
		<span class="small">User database</span>
		</label>
		<input type="text" name="user" id="user" value="root" />
		
		<label>Password
		<span class="small">Password database</span>
		</label>
		<input type="text" name="password" id="password" />
		
		<input type="hidden" name="step" id="step" value="4" />
		<label>Host
		<span class="small">Nama host database</span>
		</label>
		<input type="text" name="host" id="host" value="localhost"/>
		
		<label>DB Name
		<span class="small">Nama database</span>
		</label>
		<input type="text" name="dbname" id="dbname" value="spin12"/>
		
		
		</form>	
		<span>
		<a href="javascript:{}" onclick="document.getElementById('database_form').submit(); return false;">Next</a>
		</span>
<?php
		break;
	case "4":
?>
<h2>SPIN Keuangan</h2>
<h1>Membangun Struktur Basisdata</h1>
<p>Berikut ini hasil setup struktur basis data : 
<pre>
<?php
	//print_r($_REQUEST);
	$user = $_REQUEST["user"];
	$password = $_REQUEST["password"];
	$host = $_REQUEST["host"];
	$dbname = $_REQUEST["dbname"];
	$step = $_REQUEST["step"];
	$nextStep = $step+1;
	/* Commit Structure */	
	//D:\Projects\localhost\spin2011\build\sql\
	$conn = @getADODBconnection("mysql://$user:$password@$host/$dbname");
	if (!$conn) {
		die("Gagal terhubung dengan database <b>$dbname</b> di host <b>$host</b> dengan username <b>$user</b><br>Mohon periksa apakah database telah siap, lalu <a href='{$_SERVER["PHP_SELF"]}?step=".($step-1)."'>kembali</a> untuk memperbaiki informasi basis data. ");	
	}	
	
	$schema = dirname(__FILE__).D."build".D."sql".D."schema.sql"; 
	if (is_file($schema)) {
		echo "File skema ditemukan<br>";
	} else {
		die("Kesalahan. Mungkin source rusak atau skema perlu dikompilasi ulang<br>");
	}
	
	$res = executeSqlFile ($conn, $schema);
	
	if ($res == "true") {
		$datasqlfile = dirname(__FILE__).D."setup".D."data.sql";
		$datares = executeSqlFile ($conn, $datasqlfile);
		
		if ($datares == "true") {
			/* Change settings */
			$files = array(
				dirname(__FILE__).D."build".D."conf".D."spin-conf.php",
				dirname(__FILE__).D."runtime-conf.xml",
				dirname(__FILE__).D."build.properties",
				dirname(__FILE__).D."build.compile.properties",
				dirname(__FILE__).D."build.reverse.properties",
			);
			
			foreach ($files as $f) {
				$srcLoc = "setup".D.basename($f);			
				if (copy($srcLoc, $f) ) {
					$result = changeContent($f, array("{username}", "{password}", "{host}", "{dbname}"), array($user, $password, $host, $dbname));
					if ($result > 0) {
						echo "File ".basename($f)." berhasil disalin dan diupdate<br>";
					} else {
						echo "File ".basename($f)." tercopy, namun tidak ada perubahan<br>";
					}
				}
			}
			
			?><br></pre><p>Tahap berikutnya adalah mengunduh Aplikasi SPIN Client, menghubungkannya dengan Aplikasi RKAKL. Unduh aplikasi SPIN di sini, kemudian install dan kirim. Tahap selanjutnya akan mendeteksi hasil pengiriman dan memasukkan data ke dalam database.</p>
			<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$nextStep?>>Next</a></span><?php
		} else if (is_array($datares)) {
			echo "Query berikut menimbulkan error (<i>{$datares["errorMsg"]}</i>): <br><pre>'{$datares["culprit"]}'</pre><br>";
		}
	} else if (is_array($res)) {
		echo "Query berikut menimbulkan error (<i>{$res["errorMsg"]}</i>): <br><pre>'{$res["culprit"]}'</pre><br>"; 
	}	
	 
?>
</pre>
</p>
<?php
		break;
	case "5":	
?>		
<h2>SPIN Keuangan</h2>
<h1>Pengiriman Data Anggaran</h1>
<p>Berikut hasil proses pengiriman :</p>
<pre>
<?php	
		$xmlDir = dirname(__FILE__).D."uploads".D."rkakl".D."xml".D;
		$files = scandir($xmlDir);
		if (is_array($files)) {
			
			sort ($files, SORT_STRING);
			$latestFileSent = $files[sizeof($files)-1];
			echo "Ditemukan file: <b>".$latestFileSent."</b>. Terdeteksi: <br>";
			
			//error_reporting(E_ALL);
			/* Detect Parameters untuk import nanti */
			$tahun = substr($latestFileSent, 5, 4);			
			$dept = substr($latestFileSent, 9, 3);		
			
			$isUnit = (strlen($latestFileSent) == 25);			
			$unit = $isUnit ? substr($latestFileSent, 12, 2) : "";
			$satker = $isUnit ? "" : substr($latestFileSent, 9, 6);
			
			if (!$isUnit) {
				$c = new Criteria();
				$c->add(SatkerPeer::KODE, $satker);
				$satkerObj = SatkerPeer::doSelectOne($c);
				$unit = $satkerObj->getUnit()->getKode();								
			}
			/* Masukin ke session */		
			$_SESSION["strtahun"] = $tahun; 
			$_SESSION["strdept"] = $dept;
			$_SESSION["strunit"] = $unit;
			if (!isUnit) {
				$_SESSION["strsatker"] = $satker;
			} else {
				$_SESSION["strsatker"] = "";
			}			
			?>
- Tahun: <?=$tahun?>

- Dept: <?=$dept?>

- Unit: <?=$unit ? $unit : "-"?>

- Satker: <?=$satker ? $satker : "-"?>
<?
			$os = php_uname('s');			
			//echo $os; die;
			if ($os == 'Windows NT') {
				$exe7 = ROOT.D."bin".D."win32".D."7z.exe";
			} elseif (OS == 'Linux') {
				$exe7 = ROOT.D."bin".D."linux".D."7z";
			} else {
				echo("Error: OS belum didukung.<br>");
			}
			$outDir = $xmlDir.D.basename($xmlDir.D.$latestFileSent, ".dz");
			$command = $exe7." x -y -o".$outDir." ".$xmlDir.D.$latestFileSent."";
			//die;
			//echo("Menjalankan: ".$command."<br>"); //die;
			exec($command, $out);
			
			//print_r($out);
			//$cmdOut = implode("<br>",$out);
			//echo $cmdOut;
			
			$con = Propel::getConnection(ProgramPeer::DATABASE_NAME);
						
			if (!file_exists($outDir.D."d_item.xml")) {
				echo("File gagal diekstrak<br>");
				continue;
			} else {
				echo("File berhasil diekstrak.<br>");				
				echo("Mengimport data. Mohon tunggu....<br>");
				
				/* Program */
				$i = 0;
				try {
					$file = "t_program.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						//print_r($row);
						$row = get_object_vars($row);
						$prog = new Program();
						$prog->setKode($row["kddept"].".".$row["kdunit"].".".$row["kdprogram"]);
						$prog->setKodeFungsi($row["kdfungsi"].".".$row["kdsfung"]);
						$prog->setNama(trim(str_replace(".","",$row["nmprogram"])));				
						$prog->save($con);
						//echo $i++.") ".$row["nmprogram"]."<br>";
						$i++;
					}
					if (!$con->commit()) {
						echo('Cannot save !!!');
					} else {
						echo "Sejumlah $i program tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '. $file);
				}
				

				/* Kegiatan */
				$i = 0;
				try {
					$file = "t_giat.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						
						//print_r($row);
						$row = get_object_vars($row);
						$keg = new Kegiatan();
						$keg->setKode($row["kdgiat"]);
						$keg->setNama(trim(str_replace(".","",$row["nmgiat"])));
						
						$c = new Criteria();
						$c->add(ProgramPeer::KODE, $row["kddept"].".".$row["kdunit"].".".$row["kdprogram"]);
						$prog = ProgramPeer::doSelectOne($c);
						if (!is_object($prog)) {
							throw new Exception("The parent program (".$row["kddept"].".".$row["kdunit"].".".$row["kdprogram"].") not found");					
						}
						$keg->setProgram($prog);
						//echo $i++.") ".$row["nmgiat"]."<br>";
						$keg->save($con);
						$i++;						
					}
					
					if (!$con->commit()) {
						echo('Cannot save !!!');
					} else {
						echo "Sejumlah $i kegiatan tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}				

				/* T_Output */
				$i = 0;
				try {
					$file = "t_output.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklTOutput();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);
						$i++;
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i T_Output (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}
				
				/* D_Output */
				$i = 0;
				try {
					$file = "d_output.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklDOutput();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);	
						$i++;											
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i D_Output (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}				

				/* D_SOutput */
				$i = 0;
				try {
					$file = "d_soutput.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklDSoutput();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);	
						$i++;											
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i D_SOutput (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}				
				
				/* D_KMPNEN */
				$i = 0;
				try {
					$file = "d_kmpnen.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklDKmpnen();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);	
						$i++;											
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i D_Kmpnen (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}				
				
				/* D_SKMPNEN */
				$i = 0;
				try {
					$file = "d_skmpnen.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklDSkmpnen();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);	
						$i++;											
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i D_SKmpnen (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}
				
				/* D_ITEM */
				$i = 0;
				try {
					$file = "d_item.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						$r = new RkaklDItem();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);	
						$i++;											
					}
					if (!$con->commit()) {
						echo("Cannot save $file !!!");
					} else {
						echo "Sejumlah $i D_Item (rkakl) tersimpan<br>";   
					}
				} catch (Exception $e) {
					echo('Error saat menangani '.$file." ".$e->getMessage()."<br>");
				}
				
				/* UPDATE SATKER */
				try {
					$sql = "select b.satker_id as id, a.kdsatker as kode, b.nama from rkakl_d_item a left join satker b on a.kdsatker = b.kode group by a.kdsatker;";
					$satkerActivated = 0;
					$data = getDataBySql($sql);
					foreach ($data as $d) {					
						$satker = SatkerPeer::retrieveByPK($d["id"]);
						$satker->setAktif(1);
						if($satker->save()) {
							$satkerActivated++;
						}					
						
						$ss = new SubSatker();
						$ss->setNama("Sub Satker Nomor 1");
						$ss->setNick("Subsatker 1");
						$ss->setSingkatan("ss1");
						$ss->setSatker($satker);
						$ss->save();		
					}
					echo ("Sejumlah $satkerActivated satker terdeteksi dan diaktivasi.<br>");
				} catch (Exception $e) {
					echo('Error saat menginput satker : '. $e->getMessage()."<br>");
				}				
				/*
				try {
					$file = "t_glsai.xml";
					$xml = simplexml_load_file($outDir.D.$file);
					foreach ($xml->table as $row) {
						//$r = new Rk
						//$r = new Detil()
						$r = new RkaklDItem();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);						
						echo($i++."<br>");
						
						$r = new TGlsai();
						$r->fromArray(get_object_vars($row),BasePeer::TYPE_STUDLYPHPNAME);
						$r->save($con);
						echo($i++."<br>");
						//print_r($row);
						//if ($i == 10)
						//	break;
					}
					if (!$con->commit()) {
						echo('Cannot save !!!');
					}
				} catch (Exception $e) {
					echo('Error saat menangani '. "t_glsai.xml");
				}
				*/
			}
			
		} else {
			echo "<p>Gagal menemukan file <b>".$files[sizeof($files)-1]."</b></p>";
		}
?>
		</pre>
		<p>Lanjut ke tahap berikut</p>
		<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$step+1?>>Next</a></span>

<?php
		break;
	case "6":		
?>		
<h2>SPIN Keuangan</h2>
<h1>Pengiriman Data User</h1>
<p>Unduh file <a href="setup/pengguna.xlsx">ini</a>, isi kemudian unggah kembali melalui form di bawah ini.</p>
		<div id="stylized" class="myform" width="500px">
		<form id="database_form" name="form" method="post" action="<?=$_SERVER["PHP_SELF"]?>" enctype="multipart/form-data">
			<label>Users
			<span class="small">Daftar user</span>
			</label>
			<input name="userfile" type="file" name="users" id="users"/>
			<input type="hidden" name="step" id="step" value="7" />
		</form>
		<p>Sebagai referensi untuk pengisian data : </p><br>
		<table border=0>
		<tr><th>ID Satker</th><th>Kode</th><th>Nama</th></tr><?php  
		$sql = "select satker_id as id, kode, nama from satker where aktif = 1;";
		$data = getDataBySql($sql);
		foreach ($data as $d) {
			?><tr><td><?=$d["id"]?></td><td><?=$d["kode"]?></td><td><?=$d["nama"]?></td></tr><?
		}
		?></table><br>
		
		<table border=0>
		<tr><th width="100">ID Jabatan</th><th>Nama</th></tr><?php  
		$sql = "select * from jabatan";
		$data = getDataBySql($sql);
		foreach ($data as $d) {
			?><tr><td><?=$d["jabatan_id"]?></td><td><?=$d["nama"]?></td></tr><?
		}
		?></table>
		<span>
		<a href="javascript:{}" onclick="document.getElementById('database_form').submit(); return false;">Next</a>
		</span>		
		<?
		break;
	case 7:
		?>		
		<h2>SPIN Keuangan</h2>
		<h1>Pengiriman Data Pengguna</h1>
		<p>Berikut hasil pengiriman data pengguna</p>
		<pre><?php
		$uploaddir = "uploads/pengguna/"; // Relative Upload Location of data file
		//print_r($_FILES); die;
		
		if (is_uploaded_file($_FILES["userfile"]["tmp_name"])) {
			$uploadfile = $uploaddir . basename($_FILES["userfile"]["name"]);
			//echo ("File name: ". $_FILES["userfile"]["name"] ." ");
			
			if (move_uploaded_file($_FILES["userfile"]["tmp_name"], $uploadfile)) {
				echo("File ".$_FILES["userfile"]["name"]." berhasil diupload<br>");
			} else {
				echo("Upload gagal<br>");
				die;
			}	
						
			$reader = PHPExcel_IOFactory::createReaderForFile($uploadfile);
			$excelFile = new PHPExcel();					
			$excelFile = $reader->load($uploadfile);
			
			$sheet = $excelFile->getSheetByName("pengguna");
			$i = 2;
			$unique_arr = array();
			$fk_arr = array();
			
			while (1) {
				
				$pengguna_id = $sheet->getCellByColumnAndRow(0, $i)->getValue();
				
				if (!$pengguna_id) {
					break;
				}
				$username = $sheet->getCellByColumnAndRow(1, $i)->getValue();
				$password = $sheet->getCellByColumnAndRow(2, $i)->getValue();
				$nama = $sheet->getCellByColumnAndRow(3, $i)->getValue();
				$nip = $sheet->getCellByColumnAndRow(4, $i)->getValue();
				$jabatan_organisasi = $sheet->getCellByColumnAndRow(5, $i)->getValue();
				$email = $sheet->getCellByColumnAndRow(6, $i)->getValue();
				$ym = $sheet->getCellByColumnAndRow(7, $i)->getValue();
				$skype = $sheet->getCellByColumnAndRow(8, $i)->getValue();
				$no_telepon = $sheet->getCellByColumnAndRow(9, $i)->getValue();
				$no_hp = $sheet->getCellByColumnAndRow(10, $i)->getValue();
				$jabatan_id = $sheet->getCellByColumnAndRow(11, $i)->getValue();
				$satker_id = $sheet->getCellByColumnAndRow(12, $i)->getValue();

				$p = new Pengguna();
				//$p->setPenggunaId($pengguna_id);
				$p->setUsername($username);
				$p->setPassword($password);
				$p->setNama($nama);
				$p->setNip($nip);
				$p->setJabatanId($jabatan_id);
				$p->setSatkerId($satker_id);
				
				try {
					$p->save();
				} catch (Exception $e) {
					echo "Error : {$e->getMessage()}<br>";
				}
				$i++;			
				
			}
			
			if ($i > 0)
				echo "Sejumlah $i pengguna ditambahkan<br>";
			else 
				echo "Gagal menambahkan pengguna<br>";
		} else {
			echo "File upload gagal<br>";
		}
		?>
		</pre>
		<span>
		<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$step+1?>>Next</a></span>
		</span>
		<?php		
		break;
		
	case "8":
?>		
<h2>SPIN Keuangan</h2>
<h1>Import Data Anggaran</h1>
<p>Lakukan proses import data di bawah ini. (
<input type="checkbox" onclick="document.getElementById('import_frame').src = 'import_setup.php?gabung=' + ((this.checked) ? '1' : '-1')" />  Server gabungan *)<br />  
</p>
<table>
<iframe src="import_setup.php" id="import_frame" width="750" height="400" frameborder="1">Browser anda tidak mendukung iframe</iframe><br>
<p><i>*) Server disebut gabungan jika satker pusat dan daerah (KP / Dekon) aplikasinya disatukan dalam satu server.</i><br><br><br></p>
</table>
<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$step+1?>>Next</a></span>
<?php	
		break;
	case "9":
		?>
<h2>SPIN Keuangan</h2>
<h1>Setting Satker dan Bendahara</h1>
<p>Lakukan setting data di bawah ini sesuai dengan preferensi masing-masing satker. Jika form tidak tampil, periksa setting Xond anda</p>
<iframe src="setting.php" id="import_frame" width="750" height="700" frameborder="0">Browser anda tidak mendukung iframe</iframe><br><br><br><br>
<span><a href=<?=$_SERVER["PHP_SELF"]?>?step=<?=$step+1?>>Next</a></span>
		<?php
		break;
}	
?>
</div>
</body>
</html>
